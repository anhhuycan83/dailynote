//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DailyNote.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DAILYNOTE_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG2                     130
#define IDD_POPUP_RECORD_NOTE           131
#define IDR_ContextNoMenu               132
#define IDD_DIALOG3                     133
#define IDI_Duckling                    134
#define IDI_DucklingMute                135
#define IDD_DGLMESSAGE                  136
#define IDD_DGLHISTORY                  137
#define IDD_DGLPASSWORD                 138
#define IDC_TAB1                        1000
#define IDC_LIST1                       1001
#define chbStartup                      1002
#define txtFileStore                    1003
#define btnSave                         1004
#define btnDefault                      1005
#define btnAdd                          1006
#define txtFileSound                    1006
#define btnEdit                         1007
#define chbNSFlashWindow                1007
#define btnDelete                       1008
#define chbNSFullMode                   1008
#define btnHide                         1009
#define chbIEDramaOnTop                 1009
#define btnExit                         1010
#define chbFFDramaOnTop                 1010
#define rbF10                           1011
#define rbF11                           1012
#define IDC_DATETIMEPICKER1             1013
#define IDC_EDIT1                       1014
#define txtMsg                          1014
#define txtHistoryMsg                   1014
#define txtPassword                     1014
#define rbDaily                         1015
#define rbWeekly                        1016
#define rbMonthly                       1017
#define rbOnce                          1018
#define chbDisableSendTCVN              1026
#define txtReCheckAfter                 1027
#define btnSaveNetSend                  1029
#define lblCurrentSecond                1030
#define txtMessage                      1032
#define txtMsgSenTo                     1033
#define btnGet                          1034
#define txtMsgRecieve                   1035
#define wndListNSend                    1036
#define lblMsgCount                     1037
#define btnDeleteNS                     1038
#define btnSendNS                       1039
#define txtTo                           1040
#define btnPre                          1040
#define cbTo                            1041
#define btnNext                         1042
#define btnSaveMsg                      1043
#define chbForward75                    1044
#define chbHuy                          1045
#define chbForward60                    1046
#define btnLoadMsgNS                    1047
#define btnShowModeMsg                  1048
#define m_wndListHistory                1048
#define m_wndListHistoryView            1048
#define btnHistory                      1049
#define chbProtect                      1050
#define pmMitExit                       32771
#define pmMitShow                       32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1051
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
