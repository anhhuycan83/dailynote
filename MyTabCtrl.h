#if !defined(AFX_MYTABCTRL_H__A926F741_3811_4FF4_BD0A_606430650981__INCLUDED_)
#define AFX_MYTABCTRL_H__A926F741_3811_4FF4_BD0A_606430650981__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MyTabCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// MyTabCtrl window

class MyTabCtrl : public CTabCtrl
{
private:
// Construction
public:
	MyTabCtrl();

// Attributes
public:
	int m_nPageCount;

	//Array to hold the list of dialog boxes/tab pages for CTabCtrl
	int m_DialogID[4];

	//CDialog Array Variable to hold the dialogs 
	CDialog *m_Dialog[4];

	//Function to Create the dialog boxes during startup
	void InitDialogs();

	//Function to activate the tab dialog boxes
	void ActivateTabDialogs();


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MyTabCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~MyTabCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(MyTabCtrl)
	afx_msg void OnSelchange(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYTABCTRL_H__A926F741_3811_4FF4_BD0A_606430650981__INCLUDED_)
