// DailyNote.h : main header file for the DAILYNOTE application
//

#if !defined(AFX_DAILYNOTE_H__EEEA012E_4BA3_4EAC_B9AC_24335937E65A__INCLUDED_)
#define AFX_DAILYNOTE_H__EEEA012E_4BA3_4EAC_B9AC_24335937E65A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include "FunctionHelper.h"
/////////////////////////////////////////////////////////////////////////////
// CDailyNoteApp:
// See DailyNote.cpp for the implementation of this class
//


//MYDELenum eFrequency {
//MYDEL	ONCE,
//MYDEL		DAILY,
//MYDEL		WEEKLY,
//MYDEL		MONTHLY
//MYDEL};

class CDailyNoteApp : public CWinApp
{
private:
	HANDLE m_hMutex;
public:
	CDailyNoteApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDailyNoteApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDailyNoteApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAILYNOTE_H__EEEA012E_4BA3_4EAC_B9AC_24335937E65A__INCLUDED_)
