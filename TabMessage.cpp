// TabMessage.cpp : implementation file
//

#include "stdafx.h"
#include "DailyNote.h"
#include "TabMessage.h"
#include "DailyNoteDlg.h"
#include "DateTimeFormat.h"
#include "winsock2.h"
#include "textfile.h"
#include "History.h"
#include "Shlwapi.h" //for pathfile exist function
//#include "DlgPassword.h"
#pragma comment( lib, "shlwapi.lib")

#ifndef AFX_FUNCTIONHELPER_H__473D9A36_D429_44B1_A1DF_1A79827E1E99__INCLUDED_
#include "FunctionHelper.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTabMessage dialog


CTabMessage::CTabMessage(CWnd* pParent /*=NULL*/)
	: CDialog(CTabMessage::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTabMessage)
	m_txtMsgSentoValue = _T("");
	m_txtMsgRecieveValue = _T("");
	m_cbToValue = _T("");
	//}}AFX_DATA_INIT	
	m_maxLength = 150;
	m_strBroadcastName = _T("TCVN");
	m_strDefaultSender = _T("HUY");
	m_bIsExpandMode = false;
	m_password = _T("tinu");
	crypto.DeriveKey(m_password);
}

CTabMessage::~CTabMessage()
{
	m_fontLblMsgCount.DeleteObject();
}


void CTabMessage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTabMessage)
	DDX_Control(pDX, btnHistory, m_btnHistoryCtrl);
	DDX_Control(pDX, chbProtect, m_chbProtectCtrl);
	DDX_Control(pDX, btnPre, m_btnPreviousNS);
	DDX_Control(pDX, btnNext, m_btnNextNS);
	DDX_Control(pDX, btnShowModeMsg, m_btnShowModeMsg);
	DDX_Control(pDX, btnLoadMsgNS, m_btnLoadMsgNS);
	DDX_Control(pDX, chbForward75, m_chbForward75);
	DDX_Control(pDX, chbForward60, m_chbForward60);
	DDX_Control(pDX, chbHuy, m_chbHuy);
	DDX_Control(pDX, cbTo, m_cbTo);
	DDX_Control(pDX, btnSendNS, m_btnSendNS);
	DDX_Control(pDX, btnDeleteNS, m_btnDeleteNS);
	DDX_Control(pDX, lblMsgCount, m_lblMsgCountCtrl);
	DDX_Control(pDX, wndListNSend, m_wndListNSend);
	DDX_Control(pDX, txtMsgRecieve, m_txtMsgRecieveCtrl);
	DDX_Control(pDX, txtMsgSenTo, m_txtMsgSentoCtrl);
	DDX_Text(pDX, txtMsgSenTo, m_txtMsgSentoValue);
	DDX_Text(pDX, txtMsgRecieve, m_txtMsgRecieveValue);
	DDX_CBString(pDX, cbTo, m_cbToValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTabMessage, CDialog)
	//{{AFX_MSG_MAP(CTabMessage)
	ON_BN_CLICKED(btnSendNS, OnbtnSendNS)
	ON_BN_CLICKED(btnGet, OnbtnGet)
	ON_NOTIFY(LVN_ITEMCHANGED, wndListNSend, OnItemchangedwndListNSend)
	ON_BN_CLICKED(btnDeleteNS, OnbtnDeleteNS)
	ON_EN_SETFOCUS(txtMsgSenTo, OnSetfocustxtMsgSenTo)
	ON_BN_CLICKED(btnPre, OnbtnPre)
	ON_BN_CLICKED(btnNext, OnbtnNext)
	ON_BN_CLICKED(btnSaveMsg, OnbtnSaveMsg)
	ON_BN_CLICKED(btnLoadMsgNS, OnbtnLoadMsgNS)
	ON_BN_CLICKED(btnShowModeMsg, OnbtnShowModeMsg)
	ON_BN_CLICKED(chbHuy, OnchbHuy)
	ON_BN_CLICKED(chbForward60, OnchbForward60)
	ON_BN_CLICKED(chbForward75, OnchbForward75)
	ON_BN_CLICKED(btnHistory, OnbtnHistory)
	ON_WM_DESTROY()
	ON_BN_CLICKED(chbProtect, OnchbProtect)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTabMessage message handlers

void CTabMessage::OnbtnSendNS() 
{
	//first update the member variable of combobox
	UpdateData();
	//m_txtMessage2Ctrl.SetSel(m_txtMessage2Ctrl.GetWindowTextLength());
	// TODO: Add your control notification handler code here
	//HANDLE   handle;
	CString strToMailSlot, strErr;
	
//	void*    buffer;
	//strFrom = _T("HUY");
	//strTo = _T("HUY");
	//m_txtToCtrl.GetWindowText(strTo);	
	CString strMsg, strMsgFrom, strMsgTo;
	CStringArray strMsgArr;
	
	if(m_chbHuy.GetCheck() == BST_CHECKED)
		strMsgFrom = _T("HUY");
	else
	{
		strMsgFrom = m_localName;
		//comment out for version 1.0.3.0
		if(strMsgFrom.Compare(_T("HUY")) != 0)
		{
			AfxMessageBox(_T("The local name is not Huy. Please check the config file!"));
			return;
		}
	}

	strMsgTo = m_cbToValue;
	m_txtMsgSentoCtrl.GetWindowText(strMsg);
	////
	strMsgArr.Add(strMsgFrom);
	strMsgArr.Add(strMsgTo);
	strMsgArr.Add(strMsg);
	SendMsgPackage(strMsgArr);
	
	
	//m_localName.MakeUpper();
	//SendMsg(m_cbToValue, strMsg);
}

void CTabMessage::OnOK()
{
	// TODO: Add extra validation here
	CWnd* pwndCtrl = GetFocus();
	CWnd* pwndCtrlNext = pwndCtrl;
	int ctrl_ID = pwndCtrl->GetDlgCtrlID();
	
	switch (ctrl_ID) {
	case txtTo:
	//case txtMessage:
		pwndCtrlNext = GetDlgItem(btnSendNS);
		break;
	case IDOK:
		CDialog::OnOK();
		break;
	default:
		break;
	}
	pwndCtrlNext->SetFocus();
	//CDialog::OnOK();
}


void CTabMessage::OnCancel()
{
	// TODO: Add extra validation here
	CWnd* pwndCtrl = GetFocus();
	CWnd* pwndCtrlNext = pwndCtrl;
	int ctrl_ID = pwndCtrl->GetDlgCtrlID();
	
	switch (ctrl_ID) {
//MYDEL	case IDC_EDIT1:
//MYDEL		pwndCtrlNext = GetDlgItem(IDC_EDIT2);
//MYDEL		break;
	case IDCANCEL:
		CDialog::OnOK();
		break;
	default:
		break;
	}
	pwndCtrlNext->SetFocus();
	//CDialog::OnOK();
}

/* Create a mail slot named blort, allowing any size records
 * to be received (up to 64K). Specify that we'll wait forever
 * when reading a record.
 */
HANDLE CTabMessage::CreateServer()
{	
	m_hServer = CreateMailslot(GetMLServerPath(),
         0,
         MAILSLOT_WAIT_FOREVER,//MAILSLOT_WAIT_FOREVER
         NULL);
	return m_hServer;
}

DWORD CTabMessage::GetMessage()
{
	DWORD   msgSize;
	DWORD   msgCount;
	BOOL    err;
	CString strErr;
	
	/* Get the size of the next record */
	msgCount = msgSize = 0;
	err = GetMailslotInfo(m_hServer, 0, &msgSize, &msgCount, 0);
	if (err==0)
	{
		strErr.Format(_T("GetMailslotInfo error: %d\n"), GetLastError());
		AfxMessageBox(strErr);
		return 0;
	}
	/* Check for an error */
	if(msgCount==0 || msgSize == 0) return 0;
	DWORD    numRead;	
	CString strMsg;
	
	void*    buffer;	
	int i;
	CStringArray arrMsg;
	
	
	bool bIsCorrect = false;
	buffer = GlobalAlloc(GMEM_FIXED, msgSize);
	/* Read the record */
	err = ReadFile(m_hServer, buffer, msgSize, &numRead, 0);
	
	

	int numChar = ((int)msgSize)/sizeof(TCHAR);

	TCHAR* tmp;
	tmp = reinterpret_cast<wchar_t*>(buffer);
	
	for(i = 0; i < numChar - 1; i++)
	{
		
		if(tmp[i] == _T('\0'))
			tmp[i] = 0xAAFF;//tmp[i] = _T('\n');
	}
	
	/* See if an error */
	if (!err) strErr.Format(_T("ReadFile error: %d\n"), GetLastError());
	
	/* Make sure all the bytes were read */
	else if (msgSize != numRead) strErr.Format(_T(
		"ReadFile did not read the correct number of bytes!\n"));
	
	else
	{		
		bIsCorrect = true;
		strMsg.Format(_T("%s"),tmp);
		CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
		
		StringSplit(strMsg, pMainDlg->m_strArrMsg, 0xAAFF);
		//save this ArrMsg to check the next mesg wherethere the same sender and reciever
		
		//0: from, 1: to, 2: msg
		//check 2 last chars of msg, if == @@ => multi and had more, if ## is multi and end, else normal
		bool bIsMsgSplit = true;
		bool bAddGridItem = false;
		bool bIsMulti = false;
		bool bIsWait = false;
		CString strEnd, strRealMsg;
		int msgLeng;
		
		CString sMsg = pMainDlg->m_strArrMsg.GetAt(2);
		msgLeng = sMsg.GetLength();
		//this function is to check the multi msg or normal msg
		if(msgLeng > 1)
		{
			CString y,z;
			y=sMsg.GetAt(msgLeng-2);
			z=sMsg.GetAt(msgLeng-1);
			strEnd.Format(_T("%s%s"),y, z);
//MYDEL			y = sMsg.GetAt(sMsg.GetLength()-2);
//MYDEL			z = sMsg.GetAt(sMsg.GetLength()-1);
			if(strEnd.Compare(_T("@@"))==0)
			{
				bIsMulti = true;
				bIsWait = true;				
				strRealMsg = sMsg.Left(LastIndexOf(sMsg, _T("@@")));
				m_strArrMulti.Add(strRealMsg);
			}
			else
			{
				if(strEnd.Compare(_T("##"))==0)
				{
					bIsMulti = true;
					bIsWait = false;					
					strRealMsg = sMsg.Left(LastIndexOf(sMsg, _T("##")));
					m_strArrMulti.Add(strRealMsg);
				}
				else//single message (normal message)
				{
				}
			}
		}
		//end check multi msg
		/*
		if(pMainDlg->m_strArrMsg.GetAt(2).GetAt(pMainDlg->m_strArrMsg.GetAt(0).GetLength()-1 != 
			|| m_strArrMsgOld.GetAt(1) != pMainDlg->m_strArrMsg.GetAt(1))
			*/
		
		if(bIsMulti)
		{
			//m_strArrMulti.Add(strMsg);
			if(bIsWait)
			{			
				bAddGridItem = false;
			}
			else
			{			
			//bAddGridItem = true;
			//after add to grid, remove all catche				
				CString t1;
				for(int i=0;i<m_strArrMulti.GetSize(); i++)
					t1 +=m_strArrMulti.GetAt(i);
				
				pMainDlg->m_strArrMsg.RemoveAt(2);
				pMainDlg->m_strArrMsg.Add(t1);
				
				AddNewMsg(&pMainDlg->m_strArrMsg);
				
				m_strArrMulti.RemoveAll();
				m_strArrMulti.FreeExtra();				
			}
		}
		else
		{
			//if not multi => add to grid and process normal
			bAddGridItem = true;
			AddNewMsg(&pMainDlg->m_strArrMsg);			
		}
		
		/*
		if(m_strArrMsgOld.GetSize() > 0)
		{
			if(m_strArrMsgOld.GetAt(0) != pMainDlg->m_strArrMsg.GetAt(0)
				|| m_strArrMsgOld.GetAt(1) != pMainDlg->m_strArrMsg.GetAt(1))
			{
				m_strArrMulti.RemoveAll();
				m_strArrMulti.Add(strMsg);
				
				m_byteArrayMsg.RemoveAll();
				//m_strArrMsgOld = pMainDlg->m_strArrMsg;
				for(int i = 0; i<pMainDlg->m_strArrMsg.GetSize(); i++)
				{
					m_strArrMsgOld.Add(pMainDlg->m_strArrMsg.GetAt(i));
				}
				bIsMsgSplit = false;
				bAddGridItem = true;
			}
			
			if(bIsMsgSplit)
			{
			/*
			byte* b;
			b = reinterpret_cast<byte*>(buffer);
			for(int i = 0; i<msgSize; i++)
			{
			m_byteArrayMsg.Add(b[i]);
			}
			//update message
			numChar = ((int)m_byteArrayMsg.GetSize())/sizeof(TCHAR);
			tmp = reinterpret_cast<wchar_t*>(m_byteArrayMsg.GetData());
			for(i = 0; i < numChar - 1; i++)
			{
			
			  if(tmp[i] == _T('\0'))
			  tmp[i] = 0xAAFF;//tmp[i] = _T('\n');
			  }
			  strMsg.Format(_T("%s"),tmp);
			  
				StringSplit(strMsg, pMainDlg->m_strArrMsg, 0xAAFF);
				////
				UpdateMsgItem(&pMainDlg->m_strArrMsg, 0);				
				bAddGridItem = false;
				//update to grid
			}
		}
		else
		{
			m_byteArrayMsg.RemoveAll();
			m_strArrMulti.RemoveAll();
			m_strArrMulti.Add(strMsg);
			m_strArrMsgOld.RemoveAll();
			for(int i = 0; i<pMainDlg->m_strArrMsg.GetSize(); i++)
			{
				m_strArrMsgOld.Add(pMainDlg->m_strArrMsg.GetAt(i));
			}
			bAddGridItem = true;			
		}
		//int msgLength = pMainDlg->m_strArrMsg.GetAt(2).GetLength();
		CString te = pMainDlg->m_strArrMsg.GetAt(2);
		int msgLength = te.GetLength();
		TRACE(_T("chuoi: %s\r\nlength:%d max: %d"), 
			te, msgLength, m_maxLength);
		if(msgLength != m_maxLength)//for multi msg with the same name from at 2pm and 3pm
		{
			m_strArrMsgOld.RemoveAll();			
		}
		if(bAddGridItem)
		{
			AddNewMsg(&pMainDlg->m_strArrMsg);
		}
		*/

//MYDEL		for(i = 0; i < arrMsg.GetSize(); i++)
//MYDEL		{
//MYDEL			AfxMessageBox(arrMsg.GetAt(i));
//MYDEL		}
		
		//strMsg.Format(_T("%s %d %d %d"), tmp, _tcsclen(tmp), msgSize, &numRead);//_tcsclen(tmp)
		//AfxMessageBox(strMsg);
		//m_txtMsgRecieveCtrl.SetWindowText(tmp);
	}
	if(!bIsCorrect)
		AfxMessageBox(strErr);
	/* "buffer" now contains the contents of the record. Don't */
		/* forget to GlobalFree() it at some point!*/
	GlobalFree(buffer);
	//delete tmp; //DONT DELETE TMP BECAUSE IT WAS NOT CREATE NEW MEMORY WITH NEW KEYWORD
	
	return msgSize;
}

void CTabMessage::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	

	CloseHandle(m_hServer);
	
	MyClearMem();	
	CDialog::PostNcDestroy();
}

void CTabMessage::OnbtnGet() 
{
	// TODO: Add your control notification handler code here
	
	//GetMessage();
	//GetPCName();
	
	//UpdateData();
	
	//t.Format(_T(""), m_cbToValue);
	//m_cbTo.GetDlgItemText(m_cbTo.GetCurSel(), t);
//MYDEL	CString t;
//MYDEL	t.Format(_T("%s -- %s"), m_localName, m_cbToValue);
//MYDEL	AfxMessageBox(t);
	//SetLableFont(&m_lblMsgCountCtrl, _T("Tahoma"), 25, FW_BOLD);

//MYDEL	int i = 0;
//MYDEL	bool bRs = false;
//MYDEL	CString sRow;
//MYDEL	int totalRow = m_wndListNSend.GetItemCount();
//MYDEL	
//MYDEL//MYDEL	CTextFileWrite myfile(fullFileName, 
//MYDEL//MYDEL		CTextFileWrite::UTF_8);
//MYDEL	CStringArray arStrings;
//MYDEL	for(i = 0; i<totalRow; i++)
//MYDEL	{
//MYDEL		sRow.Format(_T("%s%c%s%c%s%c%s%c%s%c%c%c"), 
//MYDEL				m_wndListNSend.GetItemText(i, 0), 0xAAFF,//No
//MYDEL				m_wndListNSend.GetItemText(i, 1), 0xAAFF,//Time
//MYDEL				m_wndListNSend.GetItemText(i, 2), 0xAAFF,//From
//MYDEL				m_wndListNSend.GetItemText(i, 3), 0xAAFF,//Msg
//MYDEL				m_wndListNSend.GetItemText(i, 4), 0xAAFF, 0x0D, 0x0A);//To
//MYDEL		//encrypt every row		
//MYDEL		//comment out for version 1.0.2.6
//MYDEL		//CFunctionHelper::EnDeCrypt(&sRow);
//MYDEL		arStrings.Add(sRow);
//MYDEL//MYDEL		myfile.Write(sRow);
//MYDEL//MYDEL		myfile.WriteEndl();
//MYDEL	}
//MYDEL	//
//MYDEL	CString sTest = _T("hello world!");
//MYDEL	CByteArray arBytes;
//MYDEL	if(crypto.Encrypt(sTest, arBytes) == true)
//MYDEL	{
//MYDEL//MYDEL		// Store the byte array in a file, the registry, whatever.
//MYDEL//MYDEL		CFile myFile;
//MYDEL//MYDEL		//CByteArray m_baToques;
//MYDEL//MYDEL		
//MYDEL//MYDEL		if(myFile.Open(fullFileName, CFile::modeReadWrite | CFile::modeCreate))
//MYDEL//MYDEL		{
//MYDEL//MYDEL			myFile.Write(arBytes.GetData(),arBytes.GetSize());
//MYDEL//MYDEL		}
//MYDEL//MYDEL		myFile.Close();
//MYDEL		//convert arrbyte to cstring
//MYDEL		CString s1 = (LPCTSTR)arBytes.GetData();
//MYDEL		CString s2;
//MYDEL		if(crypto.Decrypt(arBytes, s2) == true)
//MYDEL		{
//MYDEL			//show
//MYDEL			CString msg;
//MYDEL			msg.Format(_T("ori:%s \nencode: %s \ndecode: %s"), sTest, s1, s2);
//MYDEL			m_txtMsgSentoCtrl.SetWindowText(msg);
//MYDEL		}
//MYDEL		
//MYDEL	}
	

}

void CTabMessage::StringSplit(const CString &str, CStringArray &arr, TCHAR chDelimitior)
{
	int nStart = 0, nEnd = 0;
	arr.RemoveAll();

	while (nEnd < str.GetLength())
	{
		// determine the paragraph ("xxx,xxx,xxx;")
		nEnd = str.Find(chDelimitior, nStart);
		if( nEnd == -1 )
		{
			// reached the end of string
			nEnd = str.GetLength();
		}

		CString s = str.Mid(nStart, nEnd - nStart);
		if (!s.IsEmpty())
			arr.Add(s);

		nStart = nEnd + 1;
	}
}

BOOL CTabMessage::OnInitDialog() 
{
	CDialog::OnInitDialog();
	//set font to main dialog font
	CDailyNoteDlg* parentWnd = ((CDailyNoteDlg*)GetParent());
	SendMessageToDescendants(WM_SETFONT,
		(WPARAM)parentWnd->GetFont()->m_hObject,
		MAKELONG(FALSE, 0), 
		FALSE);
	
	// TODO: Add extra initialization here
	m_wndListNSend.SetColumnHeader(_T("No, 22, 1; Time, 45, 1; Name, 65, 1; Msg, 60, 1; To, 2, 1"));
	m_wndListNSend.SetGridLines(TRUE); // SHow grid lines
	m_wndListNSend.SetCheckboxeStyle(RC_CHKBOX_NONE); // Enable checkboxes
	m_wndListNSend.SetEditable(FALSE); // Allow sub-text edit
	

	//m_wndListNSend.SortItems(0, TRUE); // sort the 1st column, ascending
	//m_bSortable = m_wndListNSend.IsSortable();
	UpdateData(FALSE);
	
	//GetDlgItem(IDC_ALLOWSORT)->EnableWindow(m_wndListNSend.HasColumnHeader());

	// now play some colorful stuff
	
	// Set the 3rd column background color to yellow, text color to blue
	m_wndListNSend.SetItemTextColor(-1, 2, RGB(0, 0, 0));
	//m_wndListNSend.SetItemBkColor(-1, 2, RGB(255, 255, 0));

	//m_pRecordNoteDlg = 0;
	GetPCName();
	CreateServer();
	///
	SetMsgCount();
	m_bIsFirstType = false;
	
	if(m_cbTo.GetCount() > 0)
	{
		m_cbTo.SelectString(0, m_localName);
	}
	
	LoadSettingFlashWindow();
	//set lable msg count
	SetLableFont(&m_lblMsgCountCtrl, _T("Tahoma"), 30, FW_BOLD);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTabMessage::AddNewMsg(CStringArray *arrMsg)
{
	if(arrMsg->GetSize()==3)
	{
		TCHAR temp[5];
		CDateTimeFormat dtFormat;
		COleDateTime oleDTNow = COleDateTime::GetCurrentTime();
		dtFormat.SetDateTime(oleDTNow);
		dtFormat.SetFormat(_T("h:mm"));
		//0: from, 1: to, 2: msg
		//item: 0: stt, 1: time, 2: from, 3: msg, 4: to

		const int IDX = m_wndListNSend.InsertItem(0, _T(""));
		m_wndListNSend.SetItemText(IDX, 0, _itow(m_wndListNSend.GetItemCount(), temp, 10));
		m_wndListNSend.SetItemText(IDX, 1, dtFormat.GetString());
		m_wndListNSend.SetItemText(IDX, 2, arrMsg->GetAt(0));
		m_wndListNSend.SetItemText(IDX, 3, arrMsg->GetAt(2));
		m_wndListNSend.SetItemText(IDX, 4, arrMsg->GetAt(1));
		//check to forward msg
		CString msgContent;
		msgContent.Format(_T("Forward from %s at %s\r\n%s"),
			arrMsg->GetAt(0),
			dtFormat.GetString(),
			arrMsg->GetAt(2));

		if(m_chbForward75.GetCheck() == BST_CHECKED)
			SendForward(_T("75-WIN8"),msgContent);
		if(m_chbForward60.GetCheck() == BST_CHECKED)
			SendForward(_T("TUYEN"),msgContent);
	}
	
	//check if list only has one row => auto select this row
	if(m_wndListNSend.GetItemCount()==1)
	{
		m_wndListNSend.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);		
	}
	//update label row selected
	SetMsgCount();
}

//set the lable msg count ex: 5/24
void CTabMessage::SetMsgCount()
{
	CString strMsgCount;
	POSITION pos = m_wndListNSend.GetFirstSelectedItemPosition();	
	//int nItem = m_wndListNSend.GetNextSelectedItem(pos);
	int nItem = 0;
	if(pos != NULL)
	{
		nItem = m_wndListNSend.GetNextSelectedItem(pos);
		strMsgCount.Format(_T("%s/%d"),
		m_wndListNSend.GetItemText(nItem, 0),
		m_wndListNSend.GetItemCount());
		if(m_chbProtectCtrl.GetCheck() == BST_UNCHECKED)
			m_btnDeleteNS.EnableWindow(TRUE);
		//now set the combobox sent to
		CString nameTo = m_wndListNSend.GetItemText(nItem, 2);
		nameTo.MakeUpper();
		int nSel = m_cbTo.SelectString(-1, nameTo);
		if(nSel == CB_ERR)
			m_cbTo.SetWindowText(nameTo);
		//set button next, pre
		if(m_wndListNSend.GetItemCount() == 1)
		{
			m_btnNextNS.EnableWindow(FALSE);
			m_btnPreviousNS.EnableWindow(FALSE);
		}
		else
		{
			if(m_chbProtectCtrl.GetCheck() == BST_UNCHECKED)
			{
				m_btnNextNS.EnableWindow(TRUE);
				m_btnPreviousNS.EnableWindow(TRUE);
			}
		}
	}
	else
	{
		strMsgCount.Format(_T("%d/%d"),
		0,
		m_wndListNSend.GetItemCount());
		m_btnDeleteNS.EnableWindow(FALSE);
		m_txtMsgRecieveCtrl.SetWindowText(_T(""));
		//set button next, pre
		m_btnNextNS.EnableWindow(FALSE);
		m_btnPreviousNS.EnableWindow(FALSE);
	}
	
	m_lblMsgCountCtrl.SetWindowText(strMsgCount);
}

void CTabMessage::SetCountNumber()
{
	TCHAR temp[5];
	int n = m_wndListNSend.GetItemCount();
	int num = 0;
	while(n>0)
	{
		n--;
		num++;
		m_wndListNSend.SetItemText(n, 0, _itow(num, temp, 10));
	}
}

void CTabMessage::OnItemchangedwndListNSend(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	if( pNMListView->uNewState & LVIS_SELECTED )
	{		
		int selectedItem = pNMListView->iItem;
		CString strMsg;
		//stt - 0, hour - 1, from - 2, msg - 3, To - 4
		strMsg.Format(_T("Message from %s to %s at %s\r\n\r\n%s"),
			m_wndListNSend.GetItemText(selectedItem, 2),
			m_wndListNSend.GetItemText(selectedItem, 4),
			m_wndListNSend.GetItemText(selectedItem, 1),
			m_wndListNSend.GetItemText(selectedItem, 3));
		m_txtMsgRecieveCtrl.SetWindowText(strMsg);
		SetMsgCount();
	}

	*pResult = 0;
}


void CTabMessage::OnbtnDeleteNS() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	
	//int nItem = m_wndListNSend.GetNextSelectedItem(pos);
	int nItem = 0;
	int nextItem = -1;
	POSITION pos = m_wndListNSend.GetFirstSelectedItemPosition();
	
	if(pos != NULL)
	{
		nItem = m_wndListNSend.GetNextSelectedItem(pos);
		m_wndListNSend.DeleteItem(nItem, FALSE);
		int itemCount = m_wndListNSend.GetItemCount();

		if(nItem == 0)
		{
			if(itemCount > 0)
				nextItem = 0;
		}
		else
		{
			nextItem = nItem -1;
		}
		if(nextItem >= 0)
		{	
			m_wndListNSend.SetItemState(nextItem, LVIS_SELECTED, LVIS_SELECTED);
		}
		SetCountNumber();
		
		//update label row selected
		SetMsgCount();
		
	}
}

void CTabMessage::OnSetfocustxtMsgSenTo() 
{
	// TODO: Add your control notification handler code here
	m_txtMsgSentoCtrl.SetSel(0,-1, TRUE);
}

BOOL CTabMessage::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(m_bIsFirstType)
	{
		bool bIsCheck = false;		
		switch(pMsg->message)
		{
		case WM_KEYDOWN:
			
			bIsCheck = true;
			break;
		case WM_LBUTTONDOWN:
			// Grab the edit control window rects in screen coords.
			CRect edit1Rect;
			m_txtMsgSentoCtrl.GetWindowRect( &edit1Rect );
			
			// Convert to client coordinates relative to their parent (ie this) window.
			ScreenToClient( &edit1Rect );
			// Test if the point passed in to this function is in the control's rectangle.
			const BOOL bIsMouseClick	= edit1Rect.PtInRect( pMsg->pt );
			if(bIsMouseClick)
			{
				bIsCheck = true;
			}		
			
			break;
		}
		if(bIsCheck)
		{
			CWnd* pFocus = GetFocus();
			if (pFocus && pFocus->IsKindOf(RUNTIME_CLASS(CEdit)))
			{
				if(m_txtMsgSentoCtrl.GetDlgCtrlID() == ::GetDlgCtrlID(*pFocus))
				{
					if(m_bIsFirstType)
					{
						m_txtMsgSentoCtrl.SetWindowText(_T(""));
						m_bIsFirstType = false;
						//return TRUE;
					}				
				}            
			}
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
	
}


void CTabMessage::GetPCName()
{
	CString strTemp;
	struct hostent *host;
	
	struct in_addr *ptr; // To retrieve the IP Address 
	
	DWORD dwScope = RESOURCE_CONTEXT;
	NETRESOURCE *NetResource = NULL;
	HANDLE hEnum;
	WNetOpenEnum( dwScope, NULL, NULL, 
		NULL, &hEnum );
	
	WSADATA wsaData;
	WSAStartup(MAKEWORD(1,1),&wsaData);
	USES_CONVERSION;
	if ( hEnum )
	{
		DWORD Count = 0xFFFFFFFF;
		DWORD BufferSize = 2048;
		LPVOID Buffer = new TCHAR[2048];
		WNetEnumResource( hEnum, &Count, 
			Buffer, &BufferSize );
		NetResource = (NETRESOURCE*)Buffer;
		
		TCHAR szHostName[200];
		unsigned int i;
		//get current host name
		char a[128];		
		::gethostname(a, sizeof(a));		
		ConvertCharToCString(m_localName, a);
		m_localName.MakeUpper();
		
		for ( i = 0; 
        i < BufferSize/sizeof(NETRESOURCE); 
        i++, NetResource++ )
		{
			if ( NetResource->dwUsage == 
				RESOURCEUSAGE_CONTAINER && 
				NetResource->dwType == 
				RESOURCETYPE_ANY )
			{
				if ( NetResource->lpRemoteName )
				{
					CString strFullName = 
						NetResource->lpRemoteName;
					if ( 0 == 
						strFullName.Left(2).Compare(_T("\\\\") ))
						strFullName = 
                        strFullName.Right(
						strFullName.GetLength()-2);
					
					LPSTR myChar;
					myChar = W2A(szHostName);
					gethostname( myChar, 
						_tcsclen( szHostName ) );
					myChar = W2A(strFullName);
					host = gethostbyname(myChar);
					
					if(host == NULL) continue; 
					ptr = (struct in_addr *) 
						host->h_addr_list[0];                    
					
					// Eg. 211.40.35.76 split up like this.             
					int a = ptr->S_un.S_un_b.s_b1;  // 211           
					int b = ptr->S_un.S_un_b.s_b2;  // 40
					int c = ptr->S_un.S_un_b.s_b3;  // 35
					int d = ptr->S_un.S_un_b.s_b4;  // 76
					
					//strTemp.Format(_T("%s -->  %d.%d.%d.%d"),
					//	strFullName,a,b,c,d);
					//AfxMessageBox(strTemp);
					m_cbTo.AddString(strFullName);
				}
			}
		}
		
		delete Buffer;
		WNetCloseEnum( hEnum ); 
	}
	
	WSACleanup();
}

void CTabMessage::ConvertCharToCString(CString &s, char *a)
{
	//	CString myCString;
	//  char* myChar = "I love Viet Nam"; // You mustn't insert  'L' before this quote although you can use Unicode format
	for(int i = 0; a[i] != NULL; i++)
	{
		s+=a[i];
	}
}

//MYDELvoid CTabMessage::UpdateMsgItem(CStringArray *arrMsg, int index)
//MYDEL{
//MYDEL	if(arrMsg->GetSize()==3 && index < m_wndListNSend.GetItemCount())
//MYDEL	{
//MYDEL		//0: from, 1: to, 2: msg
//MYDEL		//item: 0: stt, 1: time, 2: from, 3: msg, 4: to		
//MYDEL		CString strNew;
//MYDEL		CString strOld = m_wndListNSend.GetItemText(index, 3);
//MYDEL		//strMsg.Format(_T("Message from %s to %s at %s\r\n\r\n%s"),
//MYDEL		strNew.Format(_T("Message from %s to %s at %s\r\n\r\n%s%s"),
//MYDEL			arrMsg->GetAt(0),
//MYDEL			arrMsg->GetAt(1),
//MYDEL			strOld, arrMsg->GetAt(2));
//MYDEL		m_wndListNSend.SetItemText(index, 3, strNew);
//MYDEL		//refresh the txt recieve
//MYDEL		m_txtMsgRecieveCtrl.SetWindowText(strNew);
//MYDEL	}
//MYDEL}

//MYDELbool CTabMessage::SendMailSlot(HANDLE hML, CString strMsg, CString strMsgTo)
//MYDEL{	
//MYDEL	//AfxMessageBox(x);
//MYDEL	BOOL     err;
//MYDEL	DWORD    numOfBytesMsg, numOfFrom, numOfTo;
//MYDEL	DWORD    numWritten;
//MYDEL	DWORD    totalByte;
//MYDEL	
//MYDEL	bool bIsCorrect = false;
//MYDEL	CString strFrom, strTo, strErr;
//MYDEL	
//MYDEL	TRACE(_T("chuoi: %s\r\nlength:%d max: %d"), 
//MYDEL			strMsg, strMsg.GetLength(), m_maxLength);
//MYDEL	
//MYDEL	TCHAR *szSource = strMsg.GetBuffer(0);	
//MYDEL	TCHAR *szFrom = strFrom.GetBuffer(0);
//MYDEL	//TCHAR *szFrom = strMsgFrom.GetBuffer(0);
//MYDEL//MYDEL	if(m_chbHuy.GetCheck() == BST_CHECKED)
//MYDEL//MYDEL	{
//MYDEL//MYDEL		strFrom = _T("HUY");
//MYDEL//MYDEL		szFrom = strFrom.GetBuffer(0);
//MYDEL//MYDEL	}
//MYDEL//MYDEL	else
//MYDEL//MYDEL	{
//MYDEL//MYDEL		szFrom = m_localName.GetBuffer(0);
//MYDEL//MYDEL	}
//MYDEL	//TCHAR *szTo = strTo.GetBuffer(0);
//MYDEL	TCHAR *szTo = strMsgTo.GetBuffer(0);
//MYDEL
//MYDEL	numOfBytesMsg = (_tcslen(szSource)+1)*sizeof(TCHAR);
//MYDEL	numOfFrom = (_tcslen(szFrom)+1)*sizeof(TCHAR);
//MYDEL	numOfTo = (_tcslen(szTo)+1)*sizeof(TCHAR);
//MYDEL
//MYDEL	
//MYDEL	//memcpy(pByte, szSource, numOfBytesMsg);//(VOID*)LPCTSTR(strMsg)
//MYDEL	//byte bTo[3]={0};
//MYDEL	//HAR* bufMsg = new TCHAR[numOfFrom+2+numOfTo+2+numOfBytesMsg];
//MYDEL	//tprintf(bufMsg, _T("%s%s%s\n"),strFrom, strTo, strMsg);
//MYDEL	totalByte = numOfFrom+numOfTo+numOfBytesMsg;
//MYDEL
//MYDEL    BYTE *pByte = new BYTE[totalByte];
//MYDEL	//AfxMessageBox(pByte);
//MYDEL	memcpy(pByte, szFrom, numOfFrom);
//MYDEL	//memcpy(&pByte[numOfFrom], bSplit, 1);
//MYDEL	//AfxMessageBox(pByte);
//MYDEL	memcpy(&pByte[numOfFrom], szTo, numOfTo);
//MYDEL    //memcpy(&pByte[numOfFrom+numOfTo], bSplit, 1);
//MYDEL	memcpy(&pByte[numOfFrom+numOfTo], szSource, numOfBytesMsg);
//MYDEL
//MYDEL	
//MYDEL	//void *   buffer;
//MYDEL	/* Write out our nul-terminated string to a record */
//MYDEL	err = WriteFile(hML, pByte,//&MyMessage, 
//MYDEL		totalByte,
//MYDEL		&numWritten, 0);
//MYDEL//MYDEL	strErr.Format(_T("%s %d %d"), pByte, totalByte, numWritten);
//MYDEL//MYDEL	AfxMessageBox(strErr);
//MYDEL	/* See if an error */
//MYDEL	if (!err) 
//MYDEL	{
//MYDEL		strErr.Format(_T("WriteFile error: %d\n"), GetLastError());
//MYDEL		AfxMessageBox(strErr);
//MYDEL	}
//MYDEL	
//MYDEL	/* Make sure all the bytes were written */
//MYDEL	else 
//MYDEL	{
//MYDEL		if (totalByte != numWritten) 
//MYDEL		{
//MYDEL			strErr.Format(_T(
//MYDEL				"WriteFile did not read the correct number of bytes!\n"));
//MYDEL		}
//MYDEL		else 
//MYDEL		{
//MYDEL			//strErr.Format(_T("%s %d %d"), pByte, totalByte, numWritten);
//MYDEL			//AfxMessageBox(strErr);
//MYDEL			CString strDTNow;
//MYDEL			CDateTimeFormat dtFormat;
//MYDEL			COleDateTime oleDTNow = COleDateTime::GetCurrentTime();
//MYDEL			dtFormat.SetDateTime(oleDTNow);
//MYDEL			dtFormat.SetFormat(_T("hh:mm tt"));
//MYDEL			strDTNow = dtFormat.GetString();
//MYDEL			strErr.Format(_T("Message was sent to %s at %s"), strMsgTo, strDTNow);
//MYDEL			m_txtMsgSentoCtrl.SetWindowText(strErr);
//MYDEL			bIsCorrect = true;
//MYDEL			m_bIsFirstType = true;
//MYDEL		}
//MYDEL	}
//MYDEL	if(!bIsCorrect)
//MYDEL	{
//MYDEL		AfxMessageBox(strErr);
//MYDEL	}
//MYDEL
//MYDEL	delete pByte;
//MYDEL	strMsg.ReleaseBuffer();
//MYDEL	strFrom.ReleaseBuffer();
//MYDEL	//strMsgFrom.ReleaseBuffer();
//MYDEL	//m_localName.ReleaseBuffer();
//MYDEL	strMsgTo.ReleaseBuffer();
//MYDEL	return bIsCorrect;
//MYDEL}

int CTabMessage::SplitMsg(CStringArray *strArr, CString msg)
{
	//int m_maxLength = 179;//max is 199 with send from 75 to huy
	strArr->RemoveAll();
	CString strItem = _T("");
	int length = msg.GetLength();
	bool bMustAdd = false;
	bool bIsMulti = false;
	for(int i = 1; i <= length; i++)
	{	
		strItem += msg.GetAt(i-1);
		bMustAdd = true;
		if(i%m_maxLength == 0)
		{
			//index = 0;
			if(i == length)
			{
				strItem += _T("##");
				bIsMulti = false;
			}
			else
			{
				strItem += _T("@@");
				bIsMulti = true;
			}
			strArr->Add(strItem);
			strItem = _T("");
			bMustAdd = false;			
		}
	}
	if(bIsMulti)
	{
		strItem += _T("##");
	}
	if(bMustAdd)
		strArr->Add(strItem);

	return strArr->GetSize();
}

int CTabMessage::LastIndexOf(const CString& strSource, const CString& strSub)
{
	
    int start = strSource.Find(strSub, 0);

    if (start >= 0) 
    {
        while (start < strSource.GetLength()) 
        {
            int idx = strSource.Find(strSub, start+1); 
            if (idx >= 0) 
                start = idx; 
            else 
                break; 
        }
    }

    return start;
}

void CTabMessage::OnbtnPre() 
{
	//int nItem = m_wndListNSend.GetNextSelectedItem(pos);
	int nItem = 0;
	//int nextItem = -1;
	POSITION pos = m_wndListNSend.GetFirstSelectedItemPosition();
	
	if(pos != NULL)
	{
		nItem = m_wndListNSend.GetNextSelectedItem(pos);
		//m_wndListNSend.DeleteItem(nItem, FALSE);
		int itemCount = m_wndListNSend.GetItemCount();
		//caculate for the next item
		if(nItem == itemCount - 1)
			nItem = 0;
		else
			nItem++;		

		m_wndListNSend.SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);

		//SetCountNumber();
		
		//update label row selected
		SetMsgCount();		
	}
}

void CTabMessage::OnbtnNext()
{
//MYDEL	//rem to test decode
//MYDEL	CString HelloCrypt = _T("duoc roi");
//MYDEL	//TCHAR HelloCrypt[] = _T("Hello Crypt");
//MYDEL	CFunctionHelper::EnDeCrypt(&HelloCrypt);
//MYDEL	CFunctionHelper::EnDeCrypt(&HelloCrypt);
//MYDEL	m_txtMsgSentoCtrl.SetWindowText(HelloCrypt);
//MYDEL	//int nItem = m_wndListNSend.GetNextSelectedItem(pos);
//MYDEL	return;
	int nItem = 0;
	//int nextItem = -1;
	POSITION pos = m_wndListNSend.GetFirstSelectedItemPosition();
	
	if(pos != NULL)
	{
		nItem = m_wndListNSend.GetNextSelectedItem(pos);
		//m_wndListNSend.DeleteItem(nItem, FALSE);
		int itemCount = m_wndListNSend.GetItemCount();
		//caculate for the next item
		if(nItem > 0)
		{
			nItem--;		
		}
		else
			nItem = itemCount - 1;

		m_wndListNSend.SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);

		//SetCountNumber();
		
		//update label row selected
		SetMsgCount();
	}
}

void CTabMessage::OnbtnSaveMsg() 
{
	// TODO: Add your control notification handler code here
	CString ft = _T("Daily Note(*.dnt)|*.dnt||");
   
	CString fileName;
	TCHAR* p = fileName.GetBuffer( FILE_LIST_BUFFER_SIZE );
	CFileDialog *pFD = new CFileDialog( FALSE, _T(""), p,  OFN_OVERWRITEPROMPT|OFN_PATHMUSTEXIST, ft, this);

	if( pFD->DoModal ()==IDOK )
	{
		fileName.ReleaseBuffer();
		//CString m_strPathname = pFD->GetPathName();
		CString info;
		info.Format(_T("Total record(s) saved: %d"), SaveMsgToFile(pFD->GetPathName()));
		AfxMessageBox(info);
		//AfxMessageBox(m_strPathname);
	}
	delete pFD;
}

int CTabMessage::SaveMsgToFile(CString fullFileName)
{
	int i = 0;
	bool bRs = false;
	CString sRow;
	int totalRow = m_wndListNSend.GetItemCount();
	
//MYDEL	CTextFileWrite myfile(fullFileName, 
//MYDEL		CTextFileWrite::UTF_8);
	//CStringArray arStrings;
	CString fileConten;
	for(i = 0; i<totalRow; i++)
	{
		sRow.Format(_T("%s%c%s%c%s%c%s%c%s%c%c%c"), 
				m_wndListNSend.GetItemText(i, 0), 0xAAFF,//No
				m_wndListNSend.GetItemText(i, 1), 0xAAFF,//Time
				m_wndListNSend.GetItemText(i, 2), 0xAAFF,//From
				m_wndListNSend.GetItemText(i, 3), 0xAAFF,//Msg
				m_wndListNSend.GetItemText(i, 4), 0xAAFF, 0x0D, 0x0A);//To
		//encrypt every row		
		//comment out for version 1.0.2.6
		//CFunctionHelper::EnDeCrypt(&sRow);
		//arStrings.Add(sRow);
		fileConten += sRow;
//MYDEL		myfile.Write(sRow);
//MYDEL		myfile.WriteEndl();
	}
	CByteArray arBytes;
	if(crypto.Encrypt(fileConten, arBytes) == true)
	{
		// Store the byte array in a file, the registry, whatever.
		CFile myFile;
		//CByteArray m_baToques;
		
		if(myFile.Open(fullFileName, CFile::modeReadWrite | CFile::modeCreate | CFile::typeBinary))
		{
			myFile.Write(arBytes.GetData(),arBytes.GetSize());

//MYDEL			CString ori = _T("test");
//MYDEL			if(crypto.Encrypt(ori, arBytes) == true)
//MYDEL	{
//MYDEL			}
//MYDEL			CString strDecrypred;
//MYDEL			if(crypto.Decrypt(arBytes, strDecrypred) == true)
//MYDEL			{
//MYDEL				CString s;
//MYDEL				s = strDecrypred;
//MYDEL			}
		}
		myFile.Close();
	}
	//myfile.Close();
	return i;
}

bool CTabMessage::SendMsgPackage(CStringArray &strArr)
{
	HANDLE   handle;
	CString strMsgFrom, strMsgTo, strToMailSlot, strErr, strMsg;
	strMsgFrom = strArr.GetAt(0);
	strMsgTo = strArr.GetAt(1);
	strMsg = strArr.GetAt(2);
		
	strMsgTo.MakeUpper();

	if(strMsgTo.IsEmpty())
	{
		AfxMessageBox(_T("Please enter the name that you want to send message to!"));
		return 0;
	}

//MYDEL	if(strMsgTo.Find(m_strBroadcastName) >= 0)
//MYDEL	{
//MYDEL		int answer = AfxMessageBox(_T("Would you like to send a TCVN message?"), 
//MYDEL			MB_OKCANCEL|MB_ICONQUESTION);
//MYDEL		if(answer == IDCANCEL)
//MYDEL			return 0;
//MYDEL	}
	bool bIsCancel = false;
	STR_SWITCH(strMsgTo)
	{	
		STR_CASE(m_strBroadcastName)
		{	
			bIsCancel = ConfirmCancelSendMsg(strMsgTo);
			break;
		}		
		STR_CASE(_T("PHAM"))
		{
			bIsCancel = ConfirmCancelSendMsg(strMsgTo);
			break;
		}
		DEFAULT_CASE()
		{			
			break;
		}
	}
	STR_SWITCH_END()

	if(bIsCancel)
		return false;
	
	//strToMailSlot.Format(_T("\\\\%s\\mailslot\\netsend"), strMsgTo);
	strToMailSlot = GetMailSlotFolderPath(&strArr);
	/* Open the blort mailslot on MyComputer */
	handle = CreateFile(strToMailSlot,
		GENERIC_WRITE, 
		FILE_SHARE_READ,
		0, 
		OPEN_EXISTING, 
		FILE_ATTRIBUTE_NORMAL, 
		0); 
	if (handle == INVALID_HANDLE_VALUE) 
	{
		strErr.Format(_T("CreateFile failed: %d\n"), GetLastError());
		AfxMessageBox(strErr);
		return 0;
	}

	CStringArray strArrMsgSplit;
	SplitMsg(&strArrMsgSplit, strMsg);
	for(int i = 0; i<strArrMsgSplit.GetSize(); i++)
	{
		//comment out for version 1.0.3.1
		if(!SendMailSlotPackage(handle, &strArr, strArrMsgSplit.GetAt(i)))
			break;		
	}
	//save msg history	
	CDateTimeFormat dtFormat;
	COleDateTime oleDTNow = COleDateTime::GetCurrentTime();
	dtFormat.SetDateTime(oleDTNow);
	dtFormat.SetFormat(_T("h:mm"));
	
	CHistoryMsg *hisNew = new CHistoryMsg();
	hisNew->m_strArrMsgHistory.Copy(strArr);
	hisNew->m_strArrMsgHistory.Add(dtFormat.GetString());
	//CHistoryMsg *hisNew ;
	m_historyArr.Add(hisNew);

	CloseHandle(handle);
	return 1;
}

bool CTabMessage::SendMailSlotPackage(HANDLE hML, CStringArray *arrPackage, CString strMsgItem)
{
	//AfxMessageBox(x);
	BOOL     err;
	DWORD    numOfBytesMsg, numOfFrom, numOfTo;
	DWORD    numWritten;
	DWORD    totalByte;
	
	bool bIsCorrect = false;
	CString strFrom, strTo, strErr, strMsg;

	strFrom = arrPackage->GetAt(0);
	strTo = arrPackage->GetAt(1);
	//comment out for version 1.0.3.1
	//strMsg = arrPackage->GetAt(2);
	strMsg = strMsgItem;
	TRACE(_T("chuoi: %s\r\nlength:%d max: %d"), 
			strMsg, strMsg.GetLength(), m_maxLength);
	
	TCHAR *szSource = strMsg.GetBuffer(0);
	TCHAR *szFrom = strFrom.GetBuffer(0);
	TCHAR *szTo = strTo.GetBuffer(0);
	//TCHAR *szFrom = strFrom.GetBuffer(0);
	//TCHAR *szFrom = strMsgFrom.GetBuffer(0);
//MYDEL	if(m_chbHuy.GetCheck() == BST_CHECKED)
//MYDEL	{
//MYDEL		strFrom = _T("HUY");
//MYDEL		szFrom = strFrom.GetBuffer(0);
//MYDEL	}
//MYDEL	else
//MYDEL	{
//MYDEL		szFrom = m_localName.GetBuffer(0);
//MYDEL	}
//MYDEL	//TCHAR *szTo = strTo.GetBuffer(0);
	

	numOfBytesMsg = (_tcslen(szSource)+1)*sizeof(TCHAR);
	numOfFrom = (_tcslen(szFrom)+1)*sizeof(TCHAR);
	numOfTo = (_tcslen(szTo)+1)*sizeof(TCHAR);

	
	//memcpy(pByte, szSource, numOfBytesMsg);//(VOID*)LPCTSTR(strMsg)
	//byte bTo[3]={0};
	//HAR* bufMsg = new TCHAR[numOfFrom+2+numOfTo+2+numOfBytesMsg];
	//tprintf(bufMsg, _T("%s%s%s\n"),strFrom, strTo, strMsg);
	totalByte = numOfFrom+numOfTo+numOfBytesMsg;

    BYTE *pByte = new BYTE[totalByte];
	//AfxMessageBox(pByte);
	memcpy(pByte, szFrom, numOfFrom);
	//memcpy(&pByte[numOfFrom], bSplit, 1);
	//AfxMessageBox(pByte);
	memcpy(&pByte[numOfFrom], szTo, numOfTo);
    //memcpy(&pByte[numOfFrom+numOfTo], bSplit, 1);
	memcpy(&pByte[numOfFrom+numOfTo], szSource, numOfBytesMsg);

	
	//void *   buffer;
	/* Write out our nul-terminated string to a record */
	err = WriteFile(hML, pByte,//&MyMessage, 
		totalByte,
		&numWritten, 0);
//MYDEL	strErr.Format(_T("%s %d %d"), pByte, totalByte, numWritten);
//MYDEL	AfxMessageBox(strErr);
	/* See if an error */
	if (!err) 
	{
		//comment out for version 1.0.3.3
		strErr.Format(_T("The message can not be sent!\nWriteFile error: %d\n"), GetLastError());
		//AfxMessageBox(strErr);
	}
	
	/* Make sure all the bytes were written */
	else 
	{
		if (totalByte != numWritten) 
		{
			strErr.Format(_T(
				"WriteFile did not read the correct number of bytes!\n"));
			TRACE(strErr);
		}
		else 
		{
			//strErr.Format(_T("%s %d %d"), pByte, totalByte, numWritten);
			//AfxMessageBox(strErr);
			CString strDTNow;
			CDateTimeFormat dtFormat;
			COleDateTime oleDTNow = COleDateTime::GetCurrentTime();
			dtFormat.SetDateTime(oleDTNow);
			dtFormat.SetFormat(_T("hh:mm tt"));
			strDTNow = dtFormat.GetString();
			strErr.Format(_T("Message was sent to %s at %s"), strTo, strDTNow);
			m_txtMsgSentoCtrl.SetWindowText(strErr);
			bIsCorrect = true;
			m_bIsFirstType = true;
			dtFormat.SetDateTime(COleDateTime::GetCurrentTime());
			dtFormat.SetFormat(_T("hh:mm:ss tt"));
			strDTNow = dtFormat.GetString();
			TRACE(_T("Message was sent to %s at %s"), strTo, strDTNow);
		}
	}
	if(!bIsCorrect)
	{
		AfxMessageBox(strErr);
	}

	delete pByte;
	strMsg.ReleaseBuffer();
	strFrom.ReleaseBuffer();
	strTo.ReleaseBuffer();
	//strMsgFrom.ReleaseBuffer();
	//m_localName.ReleaseBuffer();
	
	return bIsCorrect;
}

void CTabMessage::SendForward(CString strTo, CString strMsg)
{
	CStringArray arrPackage;
	if(m_localName.CompareNoCase(strTo)==0)
		return;//the same name
	CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;	
	arrPackage.Add(pMainDlg->m_strArrMsg.GetAt(0));
	arrPackage.Add(strTo);
	arrPackage.Add(strMsg);
	SendMsgPackage(arrPackage);
}

CString CTabMessage::GetMailSlotFolderPath(CStringArray *arrMsg)
{
	////check if msg is forward type => 
	//check if forward to 60 => change the mailslot folder to .\\\\%s\\mailslot\\netsend
	CString strToMailSlot, strMsgTo;
	strMsgTo = arrMsg->GetAt(1);
	bool bIsFolderChanged = false;
	if(m_chbForward60.GetCheck() == BST_CHECKED)
	{
		if(strMsgTo.CompareNoCase(_T("TUYEN")) == 0)
		{
			strToMailSlot.Format(_T("\\\\%s\\mailslot\\forward\\netsend"), strMsgTo);
			bIsFolderChanged = true;
		}
	}
	if(!bIsFolderChanged)//set default
		strToMailSlot.Format(_T("\\\\%s\\mailslot\\netsend"), strMsgTo);

	return strToMailSlot;
}

CString CTabMessage::GetMLServerPath()
{
	CString strServerPath;
	if(m_localName.CompareNoCase(_T("TUYEN")) == 0)
		strServerPath = _T("\\\\.\\mailslot\\forward\\netsend");
	else
		strServerPath = _T("\\\\.\\mailslot\\netsend");
	return strServerPath;
}

void CTabMessage::LoadSettingFlashWindow()
{
	//load config of flashwindow
	if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("NETSENDFLASH"), _T("1")) == _T("0"))
		m_bIsFlashWindow = false;
	else
		m_bIsFlashWindow = true;
	//load config of show mode
	if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("NETSENDFULLMODE"), _T("0")) == _T("0"))
		m_bIsExpandMode = false;
	else
		m_bIsExpandMode = true;
	//NETSEND_CB_HUY
	if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("NETSEND_CB_HUY"), _T("0")) == _T("0"))
		m_chbHuy.SetCheck(false);
	else
		m_chbHuy.SetCheck(true);
	//NETSEND_CB_60
	if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("NETSEND_CB_60"), _T("0")) == _T("0"))
		m_chbForward60.SetCheck(false);
	else
		m_chbForward60.SetCheck(true);
	//NETSEND_CB_75
	if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("NETSEND_CB_75"), _T("0")) == _T("0"))
		m_chbForward75.SetCheck(false);
	else
		m_chbForward75.SetCheck(true);
	//show mode
	OnbtnShowModeMsg();
}

bool CTabMessage::ConfirmCancelSendMsg(CString msgTo)
{
	bool bIsCancel = false;
	CString msg;
	msg.Format(_T("Would you like to send a %s message?"), msgTo);
	int answer = AfxMessageBox(msg, 
		MB_OKCANCEL|MB_ICONQUESTION);
	if(answer == IDCANCEL)
		bIsCancel = true;
	return bIsCancel;
}



void CTabMessage::OnbtnLoadMsgNS()
{
	// TODO: Add your control notification handler code here
	CString ft = _T("Daily Note(*.dnt)|*.dnt||");
   
	CString fileName;
	TCHAR* p = fileName.GetBuffer( FILE_LIST_BUFFER_SIZE );
	CFileDialog *pFD = new CFileDialog( TRUE, _T(""), p,  OFN_FILEMUSTEXIST|OFN_HIDEREADONLY, ft, this);
	
	if( pFD->DoModal ()==IDOK )
	{
		fileName.ReleaseBuffer();
		LoadMsgNS(pFD->GetPathName());
	}
	delete pFD;
	SetCountNumber();
}

bool CTabMessage::LoadMsgNS(CString sFullFileName)
{
	bool bRs = false;	
	int totalRow = m_wndListNSend.GetItemCount();
	
//MYDEL	CTextFileRead myfile(sFullFileName);
	CString sLine, sTemp;
	CStringArray sArrMsgRow;
	CFileFind finder;

	if(!finder.FindFile(sFullFileName))
	{
		return false;
	}
	//decode before get messages
	CFile myFile;    
    if(myFile.Open(sFullFileName, CFile::modeRead | CFile::typeBinary | CFile::modeNoTruncate))
    {
        CByteArray buffer, allBytes;
		buffer.SetSize(1024); // ensure that buffer is allocated and the size we want it
		UINT bytesRead;
//MYDEL		UINT bytesRead = myFile.Read(buffer.GetData(), buffer.GetSize());
//MYDEL		CFunctionHelper::AddByte(&allBytes, buffer.GetData(), bytesRead);
		do
		{
			bytesRead = myFile.Read(buffer.GetData(), buffer.GetSize());
			CFunctionHelper::AddByte(allBytes, buffer.GetData(), bytesRead);
		}while(bytesRead == 1024);

		myFile.Close();
		// use bytesRead value and data now in buffer as needed
		//now decode:
		CString strDecrypred;
		if(crypto.Decrypt(allBytes, strDecrypred) == true)
		{
			//save to temp file
			CString tempFileName = sFullFileName + _T("t");
			CTextFileWrite fw(tempFileName, 
				CTextFileWrite::UTF_8);
			fw.Write(strDecrypred);
			fw.Close();
			CTextFileRead fr(tempFileName);
			while(fr.ReadLine(sLine))
			{
				//decode this line
				//comment out for version 1.0.2.6
				//MYDEL		CFunctionHelper::EnDeCrypt(&sLine);
				
				StringSplit(sLine, sArrMsgRow, 0xAAFF);
				if(sArrMsgRow.GetSize() == 5)
				{
					AddItemFromFile(&sArrMsgRow, totalRow);
				}
				else
				{			
					bool bEndItem = false;
					int pos;
					while(!bEndItem && fr.ReadLine(sLine))
					{	
						pos = sLine.Find(0xAAFF);
						if(pos >= 0)
						{
							sTemp += sLine.Left(pos);
							bEndItem = true;
						}
						else
							sTemp += sLine;
					}
					if(bEndItem)
					{
						sArrMsgRow.Add(sTemp);
						AddItemFromFile(&sArrMsgRow, totalRow);
					}
				}
				totalRow++;
			}
			fr.Close();
			//delete temp file
			DeleteFile(tempFileName);
			//now select first item if not yeat selected
			if(totalRow > 0)
			{
				POSITION pos = m_wndListNSend.GetFirstSelectedItemPosition();
				if(pos == NULL)
				{
					//now init the first item with selected
					m_wndListNSend.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
					SetMsgCount();
				}
			}
			
			bRs = true;
		}		
    }
	//comment out for version 1.0.2.6
	//return false;
//MYDEL	while(myfile.ReadLine(sLine))
//MYDEL	{
//MYDEL		//decode this line
//MYDEL		//comment out for version 1.0.2.6
//MYDEL//MYDEL		CFunctionHelper::EnDeCrypt(&sLine);
//MYDEL
//MYDEL		StringSplit(sLine, sArrMsgRow, 0xAAFF);
//MYDEL		if(sArrMsgRow.GetSize() == 5)
//MYDEL		{
//MYDEL			AddItemFromFile(&sArrMsgRow, totalRow);
//MYDEL		}
//MYDEL		else
//MYDEL		{			
//MYDEL			bool bEndItem = false;
//MYDEL			int pos;
//MYDEL			while(!bEndItem && myfile.ReadLine(sLine))
//MYDEL			{	
//MYDEL				pos = sLine.Find(0xAAFF);
//MYDEL				if(pos >= 0)
//MYDEL				{
//MYDEL					sTemp += sLine.Left(pos);
//MYDEL					bEndItem = true;
//MYDEL				}
//MYDEL				else
//MYDEL					sTemp += sLine;
//MYDEL			}
//MYDEL			if(bEndItem)
//MYDEL			{
//MYDEL				sArrMsgRow.Add(sTemp);
//MYDEL				AddItemFromFile(&sArrMsgRow, totalRow);
//MYDEL			}
//MYDEL		}
//MYDEL		totalRow++;
//MYDEL	}
//MYDEL	//now select first item if not yeat selected
//MYDEL	if(totalRow > 0)
//MYDEL	{
//MYDEL		POSITION pos = m_wndListNSend.GetFirstSelectedItemPosition();
//MYDEL		if(pos == NULL)
//MYDEL		{
//MYDEL			//now init the first item with selected
//MYDEL			m_wndListNSend.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
//MYDEL			SetMsgCount();
//MYDEL		}
//MYDEL	}
//MYDEL	myfile.Close();
//MYDEL	bRs = true;
	return bRs;
}

bool CTabMessage::AddItemFromFile(CStringArray *sArrMsgRow, int rowIndex)
{
	bool bRs = false;
	if(sArrMsgRow->GetSize() == 5)
	{
		const int IDX = m_wndListNSend.InsertItem(rowIndex, _T(""));
		m_wndListNSend.SetItemText(IDX, 0, sArrMsgRow->GetAt(0));
		m_wndListNSend.SetItemText(IDX, 1, sArrMsgRow->GetAt(1));
		m_wndListNSend.SetItemText(IDX, 2, sArrMsgRow->GetAt(2));
		m_wndListNSend.SetItemText(IDX, 3, sArrMsgRow->GetAt(3));
		m_wndListNSend.SetItemText(IDX, 4, sArrMsgRow->GetAt(4));
		bRs = true;
	}
	return bRs;
}

void CTabMessage::OnbtnShowModeMsg() 
{
	// TODO: Add your control notification handler code here
	//ShowMode(false);
	m_bIsExpandMode = !m_bIsExpandMode;
	if(m_bIsExpandMode)
		ShowExpand();
	else
		ShowLitle();
}

void CTabMessage::ShowExpand()
{
	CString log;
	//m_bIsExpandMode = true;
	CRect l_rectDgl;
	CRect l_rectWnd;
	CRect l_rectRecieve;
	//int widthAdd;
	
	GetClientRect(l_rectDgl);
	
	//AdjustRect(FALSE,l_rectDgl);
	m_txtMsgSentoCtrl.GetWindowRect(l_rectWnd);
	m_txtMsgRecieveCtrl.GetWindowRect(l_rectRecieve);
	ScreenToClient(l_rectWnd);
	ScreenToClient(l_rectRecieve);
	//MYDEL	log.Format(_T("tab rec: %d, %d, %d, %d, Width: %d Heigh: %d\r\nTextbox rec: %d, %d, %d, %d Width: %d Heigh: %d\r\n"),
	//MYDEL		l_rectDgl.top, l_rectDgl.left, l_rectDgl.bottom, l_rectDgl.right, l_rectDgl.Width(), l_rectDgl.Height(), 
	//MYDEL		l_rectWnd.top, l_rectWnd.left, l_rectWnd.bottom, l_rectWnd.right, l_rectWnd.Width(), l_rectWnd.Height());
	//MYDEL	TRACE(log);
	
	l_rectWnd.left = 0;
	l_rectRecieve.left = 0;
	//ClientToScreen(l_rectWnd);
	//l_rectWnd.right += l_rectWnd.left;
	//l_rectWnd.OffsetRect(-l_rectWnd.left, 0);
	
	//MYDEL	log.Format(_T("textbox resize %d, %d, %d, %d, Width: %d Heigh: %d\r\n"),
	//MYDEL		l_rectWnd.top, l_rectWnd.left, l_rectWnd.bottom, l_rectWnd.right, l_rectWnd.Width(), l_rectWnd.Height());
	//MYDEL	TRACE(log);
	//m_txtMsgSentoCtrl.SetWindowPos(&wndTop, l_rectWnd.left, l_rectWnd.top, l_rectWnd.Width(), l_rectWnd.Height(), SWP_SHOWWINDOW);
	m_txtMsgSentoCtrl.MoveWindow(l_rectWnd.left, l_rectWnd.top, l_rectWnd.Width(), l_rectWnd.Height());
	m_txtMsgRecieveCtrl.MoveWindow(l_rectRecieve.left, l_rectRecieve.top, l_rectRecieve.Width(), l_rectRecieve.Height());
	//m_txtMsgSentoCtrl.ShowWindow(SW_SHOW);
	
	//widthAdd = l_rectWnd.left;
	
	m_wndListNSend.ShowWindow(SW_HIDE);
}

void CTabMessage::ShowLitle()
{
	CString log;
	//m_bIsExpandMode = false;
	CRect l_rectDgl;
	CRect l_rectWnd;
	CRect l_rectRecieve;
	CRect l_rectList;
	//int widthAdd;
	
	GetClientRect(l_rectDgl);
	
	//AdjustRect(FALSE,l_rectDgl);
	m_txtMsgSentoCtrl.GetWindowRect(l_rectWnd);
	m_txtMsgRecieveCtrl.GetWindowRect(l_rectRecieve);
	m_wndListNSend.GetWindowRect(l_rectList);
	ScreenToClient(l_rectWnd);
	ScreenToClient(l_rectRecieve);
	ScreenToClient(l_rectList);
	//MYDEL	log.Format(_T("tab rec: %d, %d, %d, %d, Width: %d Heigh: %d\r\nTextbox rec: %d, %d, %d, %d Width: %d Heigh: %d\r\n"),
	//MYDEL		l_rectDgl.top, l_rectDgl.left, l_rectDgl.bottom, l_rectDgl.right, l_rectDgl.Width(), l_rectDgl.Height(), 
	//MYDEL		l_rectWnd.top, l_rectWnd.left, l_rectWnd.bottom, l_rectWnd.right, l_rectWnd.Width(), l_rectWnd.Height());
	//MYDEL	TRACE(log);
	
	l_rectWnd.left = l_rectList.Width() + 8;
	l_rectRecieve.left = l_rectList.Width() + 8;
	//ClientToScreen(l_rectWnd);
	//l_rectWnd.right += l_rectWnd.left;
	//l_rectWnd.OffsetRect(-l_rectWnd.left, 0);
	
	//MYDEL	log.Format(_T("textbox resize %d, %d, %d, %d, Width: %d Heigh: %d\r\n"),
	//MYDEL		l_rectWnd.top, l_rectWnd.left, l_rectWnd.bottom, l_rectWnd.right, l_rectWnd.Width(), l_rectWnd.Height());
	//MYDEL	TRACE(log);
	//m_txtMsgSentoCtrl.SetWindowPos(&wndTop, l_rectWnd.left, l_rectWnd.top, l_rectWnd.Width(), l_rectWnd.Height(), SWP_SHOWWINDOW);
	m_txtMsgSentoCtrl.MoveWindow(l_rectWnd.left, l_rectWnd.top, l_rectWnd.Width(), l_rectWnd.Height());
	m_txtMsgRecieveCtrl.MoveWindow(l_rectRecieve.left, l_rectRecieve.top, l_rectRecieve.Width(), l_rectRecieve.Height());
	//m_txtMsgSentoCtrl.ShowWindow(SW_SHOW);
	
	//widthAdd = l_rectWnd.left;
	
	m_wndListNSend.ShowWindow(SW_SHOW);
	
	//MYDEL	for(int nCount=0; nCount < m_nPageCount; nCount++){
	//MYDEL	 m_Dialog[nCount]->SetWindowPos(&wndTop, l_rectDgl.left, l_rectDgl.top, l_rectDgl.Width(), l_rectDgl.Height(), SWP_HIDEWINDOW);
	//MYDEL	}
	//MYDEL	m_Dialog[nSel]->SetWindowPos(&wndTop, l_rectDgl.left, l_rectDgl.top, l_rectDgl.Width(), l_rectDgl.Height(), SWP_SHOWWINDOW);
}

//MYDELvoid CTabMessage::SetRtbFont(CRichEditCtrl *rtbCtrl, CString ins_strFont)
//MYDEL{
//MYDEL	CDC *pDC = GetDC();
//MYDEL
//MYDEL	// create UNICODE font
//MYDEL	LOGFONT lf;
//MYDEL	
//MYDEL	memset(&lf, 0, sizeof(lf));
//MYDEL	lf.lfHeight =
//MYDEL	MulDiv(25, ::GetDeviceCaps(pDC->m_hDC,
//MYDEL		 LOGPIXELSY), 172);
//MYDEL	lf.lfWeight = FW_NORMAL;
//MYDEL	lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
//MYDEL	wcscpy(lf.lfFaceName, ins_strFont);
//MYDEL	m_fontLblMsgCount.CreateFontIndirect(&lf);
//MYDEL
//MYDEL	// apply the font to the controls
//MYDEL	//m_list.SetFont(&m_font);
//MYDEL	//m_edit.SetFont(&m_font);	
//MYDEL	rtbCtrl->SetFont(&m_fontLblMsgCount);
//MYDEL	// release the device context.
//MYDEL	ReleaseDC(pDC);
//MYDEL}

void CTabMessage::SetLableFont(CStatic *lable, CString ins_strFont, int PointSize, int Weight)
{
	CDC *pDC = GetDC();

	// create UNICODE font
	LOGFONT lf;
	
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight =
	MulDiv(PointSize, ::GetDeviceCaps(pDC->m_hDC,
		 LOGPIXELSY), 172);
	lf.lfWeight = Weight;
	lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	wcscpy(lf.lfFaceName, ins_strFont);
	m_fontLblMsgCount.CreateFontIndirect(&lf);

	// apply the font to the controls
	//m_list.SetFont(&m_font);
	//m_edit.SetFont(&m_font);	
	lable->SetFont(&m_fontLblMsgCount);
	// release the device context.
	ReleaseDC(pDC);
}


void CTabMessage::OnchbHuy() 
{
	// TODO: Add your control notification handler code here
	CString strTemp;
	//NETSEND_CB_HUY
	if(m_chbHuy.GetCheck())	
	{
		strTemp = _T("1");
	}
	else
	{
		strTemp = _T("0");
	}
	AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("NETSEND_CB_HUY"), strTemp);
}

void CTabMessage::OnchbForward60() 
{
	// TODO: Add your control notification handler code here
	CString strTemp;
	//NETSEND_CB_60
	if(m_chbForward60.GetCheck())	
	{
		strTemp = _T("1");
	}
	else
	{
		strTemp = _T("0");
	}
	AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("NETSEND_CB_60"), strTemp);	
}

void CTabMessage::OnchbForward75() 
{
	// TODO: Add your control notification handler code here
	CString strTemp;
	//NETSEND_CB_75
	if(m_chbForward75.GetCheck())	
	{
		strTemp = _T("1");
	}
	else
	{
		strTemp = _T("0");
	}
	AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("NETSEND_CB_75"), strTemp);	
}

void CTabMessage::MyClearMem()
{
	for(int i = 0; i < m_historyArr.GetSize(); i++)
	{
		CHistoryMsg *his = (CHistoryMsg*)m_historyArr.GetAt(i);
		his->m_strArrMsgHistory.RemoveAll();
		delete his;
	}
	m_historyArr.RemoveAll();
}

void CTabMessage::OnbtnHistory() 
{
	// TODO: Add your control notification handler code here
	CHistory m_dlg;
	m_dlg.m_pListHistory = &m_historyArr;
	int nResponse = m_dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
		//CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
		//pMainDlg->MyReRunTimer();
		//m_dlg.m_txtHistoryMsgCtrl.GetWindowText()
		//comment out for version 1.0.3.2: for preventing textbox is clearned the containt when typing chars
		m_bIsFirstType = false;
		m_txtMsgSentoCtrl.SetWindowText(m_dlg.m_strMsgSelected);
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

}

void CTabMessage::OnDestroy() 
{
	SaveMsgDefault();
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here

	//save messages to default file
	//"msgbk.dnt"
	
}

void CTabMessage::SaveMsgDefault()
{
	if(m_wndListNSend.GetItemCount() == 0)
		return;
	CString fullFN;
	CDateTimeFormat dtFormat;
	COleDateTime oleDTNow = COleDateTime::GetCurrentTime();
	dtFormat.SetDateTime(oleDTNow);
	dtFormat.SetFormat(_T("MM-dd-HH-mm"));
	
//MYDEL	fullFN.Format(_T("%s\\%s.dnt"), 
//MYDEL		CFunctionHelper::GetCurrentDirectory(), 
//MYDEL		dtFormat.GetString());
	CString foNS = _T("c:\\netsend");
	if(!PathFileExists(foNS))
	{
		CreateDirectory(_T("c:\\netsend"), NULL);
	}

	fullFN.Format(_T("c:\\netsend\\%s.dnt"), 
		dtFormat.GetString());
	SaveMsgToFile(fullFN);
}

void CTabMessage::OnchbProtect() 
{
	// TODO: Add your control notification handler code here
	if(m_chbProtectCtrl.GetCheck() == BST_CHECKED)
	{
		m_btnShowModeMsg.EnableWindow(FALSE);
		m_btnPreviousNS.EnableWindow(FALSE);
		m_btnNextNS.EnableWindow(FALSE);
		m_btnSendNS.EnableWindow(FALSE);
		m_btnLoadMsgNS.EnableWindow(FALSE);
		m_btnDeleteNS.EnableWindow(FALSE);
		m_btnHistoryCtrl.EnableWindow(FALSE);
	}
	else
	{
		//first check the input pass
		m_dlgPassword.m_txtPasswordValue.Empty();
		int nResponse = m_dlgPassword.DoModal();		
		//m_dlgPassword.UpdateData(false);
		if (nResponse == IDOK)
		{
			// TODO: Place code here to handle when the dialog is
			//check whether the password is correct?
			if(m_dlgPassword.m_txtPasswordValue.Compare(m_password) != 0)
			{
				//incorrect
				MessageBox(_T("Password is incorrect!"));
				//recheck
				m_chbProtectCtrl.SetCheck(1);
				return;
			}
		}
		else if (nResponse == IDCANCEL)
		{
			//user doest not enter password => reuturn
			//recheck
			m_chbProtectCtrl.SetCheck(1);
			return;
		}
//MYDEL		//if pass is correct => unprotect all buttons
		m_btnShowModeMsg.EnableWindow(TRUE);
//MYDEL		m_btnPreviousNS.EnableWindow(TRUE);
//MYDEL		m_btnNextNS.EnableWindow(TRUE);
		m_btnSendNS.EnableWindow(TRUE);
		m_btnLoadMsgNS.EnableWindow(TRUE);
//MYDEL		//check if the list of messages has at least one row
//MYDEL		if(m_wndListNSend.GetItemCount() > 0)
//MYDEL			m_btnDeleteNS.EnableWindow(TRUE);
		SetMsgCount();
		m_btnHistoryCtrl.EnableWindow(TRUE);
	}
}
