// DailyNote.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DailyNote.h"
#include "DailyNoteDlg.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDailyNoteApp

BEGIN_MESSAGE_MAP(CDailyNoteApp, CWinApp)
	//{{AFX_MSG_MAP(CDailyNoteApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDailyNoteApp construction

CDailyNoteApp::CDailyNoteApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDailyNoteApp object

CDailyNoteApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CDailyNoteApp initialization

BOOL CDailyNoteApp::InitInstance()
{
	//my check if have only one instance begin
	// Create mutex
	//CWnd *pWndFrame;
	//CWnd *pWndChild;
	//WNDCLASS wndcls;

	m_hMutex = ::CreateMutex(NULL, TRUE, _T(ONCE_INSTANCE));

	switch(::GetLastError())
	{
		case ERROR_SUCCESS:
		// Mutex created successfully. There is no instance running						
			break;

		case ERROR_ALREADY_EXISTS:
		// Mutex already exists so there is a running instance of our app.
			TCHAR msg[50];
			_stprintf(msg, _T("%s is already running!"), _T(ONCE_INSTANCE));
			//msg = _T("is already running!") + _T(ONCE_INSTANCE);
			//msg.Format(_T("%s is already running!"), ONCE_INSTANCE);
			AfxMessageBox(msg);
			return FALSE;

		default:
		// Failed to create mutex by unknown reason
			return FALSE;
	}
	
	//my end

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	

	CDailyNoteDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
