// DailyNoteDlg.h : header file
//

#if !defined(AFX_DAILYNOTEDLG_H__C7CD3AF7_CC47_4944_B4AE_E43E88F62D51__INCLUDED_)
#define AFX_DAILYNOTEDLG_H__C7CD3AF7_CC47_4944_B4AE_E43E88F62D51__INCLUDED_

#include "hightime.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
#include "MyTabCtrl.h"
#include "afxtempl.h" //for list object
#include "textfile.h"
#include "TabMessage.h"

/////////////////////////
//begin define for using switch with cstring
#define STR_SWITCH(str)  {TCHAR* __ps = (TCHAR*)((const TCHAR*)str);while(1) {

#define STR_SWITCH_END()  break; } }

#define STR_CASE(str) if(0 == _tcsicmp(__ps,((const TCHAR*)str)))

#define STR_CASE_EXACT(str)  if( 0 == _tcscmp( __ps,((const TCHAR*)str) ) )

#define DEFAULT_CASE()
//end define for using switch with cstring



class CRecordNote : public CObject
{
public:	
	//CString m_strTime;
	//CString m_strMessage;
	CString m_line;

public:
	//bool m_bIsChecked;
	//CRecordNote(CString strTime, CString strMessage);
	CRecordNote(CString strLine);
};

// CDailyNoteDlg dialog
typedef bool (*MYPROC)(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath);
typedef bool (*DELPROC)(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath, int index);
typedef bool (*ADDPROC)(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath, CString strLineAdd);

//hook key board
typedef bool (*MYKBPROC)(HHOOK hHook);
typedef bool (*MYKBPROC1)();
typedef bool (*MYKBPROC2)(bool);
typedef bool (*MYKBPROC3)(HWND ins_hWndRecMsg);//setHWNDRecMsg

//static function
BOOL CALLBACK ChildWndProcMediaPlay(HWND , LPARAM );
BOOL CALLBACK ChildWndProcMediaNext(HWND hwnd, LPARAM lParam);

////////// add nick
static BOOL CALLBACK ChildWndProcTotalCommand(HWND hwnd,LPARAM lParam);
static BOOL CALLBACK EnumWindowsProcTotalCommand(HWND hwnd,LPARAM lParam);
static BOOL CALLBACK EnumWindowsProcIEDramas(HWND hwnd,LPARAM lParam);
static HWND m_hIEDramas;
static HWND m_hFFDramas;
static BOOL CALLBACK EnumWindowsProcFFDramas(HWND hwnd,LPARAM lParam);
static HWND m_h1KApp;
static BOOL CALLBACK EnumWindowsProc1kApps(HWND hwnd,LPARAM lParam);

static HWND m_hTotalCommand;
static HWND m_hFunctions;
static int m_countWindow;

static BOOL CALLBACK EnumWindowsProcVideoMonitorClient(HWND hwnd,LPARAM lParam);
static BOOL CALLBACK ChildWndProcVideoMonitorClient(HWND hwnd,LPARAM lParam);
static HWND m_hBtnVideoMonitorClient;
static HWND m_hBtnVideoMonitorClientRecordMam1;
static void MyVideoMonitorClientClick(HWND hwnd, bool bIsForgeGround);
static void SetForegroundWindowInternal(HWND hWnd);
static int m_windowSize = 0;

class CDailyNoteDlg : public CDialog
{
// Construction
public: //for mute volume
	CTabMessage* m_pTabMessage;
	CStringArray m_strArrMsg;
	HICON m_hIconSystemTray;
	void ToggleMuteXP();
	void ToggleMute();
	void SetVolume( const DWORD dwVolume );
	DWORD GetVolume( void );
	void CheckIsMute();
	bool bIsWindowsVistaOrLater;
	int TrayMessageShowBalloon(DWORD dwMessage, CString sTip, CString sTitle, CString sMsg, int timeout);
	//void ToggleMuteMedia();
	bool m_bIsMediaPlaying;
	HWND MyGetMediaApp();
	HWND m_hMedia;
	void ToggleMediaPlay();
	void ToggleMediaNext();
	HWND m_hTotalApp;
	void ShowTotalApp();
	HWND MyGetTotalApp();
	void TogleIEDramas();
	void TogleFFDramas();
	HWND MyGetIEDramas();
	HWND MyGetFFDramas();
	HWND MyGet1KApps(bool bIsSignle);
	
	//RDP app
	HWND m_hRemoteDesktop;
	void ResizeRemoteDesktopApp(CString strRemoteDesktopCaption);
	HWND MyGetRemoteDesktopApp(CString strRemoteDesktopCaption);

	
public:	
	bool MyProccessContainMessage(CString sMsgContain);
	HWND MyGetVideoMonitorClient();
	bool m_bIsForgeGround;
	BOOL m_bIsOnTopDramas;
	bool IsFileExist(CString fullFileName, bool &vCanAccess);
	void myShowDlg(bool ins_isShow, int flag);
	void MyStartThreadMailSlot();
	void MyStartThreadDramas();
	void ResetForcegroundIEDramas();
	void ResetForcegroundFFDramas();
	bool IsWindowsVistaOrLater();
	HWND MyGetHelperApp();
	bool m_bIsMute;
	DWORD dwVolume;
	COleDateTime MyAddMonth(COleDateTime dtInput, int monthToAdd);
	void SetStringNotify(CString strMsg, CString strSequence);
	int MyGetDaysInMonth(int month, int year);
	COleDateTime MySetDateTime(int year, int month, int day, int hour, int minute, int second);
	CString m_strMsgNotify;
	int TrayMessage(DWORD dwMessage, CString strMsg);
	eFrequency GetFrequencyEnum(CString strFre);
	bool MyCompareYMDEqual(COleDateTime startDT, COleDateTime endDT);
	COleDateTime GetPresentDT(COleDateTime dtInput, eFrequency eFre);
	bool MyCompareHMS(COleDateTime startDT, COleDateTime endDT);
	bool MyCompareYMD(COleDateTime startDT, COleDateTime endDT);
	void SetItemBkColor(int indexRow, COLORREF colorToSet);
	void MyReRunTimer();
	int m_currentIndexWaitingToShow;
	bool m_isHaveCurrentTimeCheking;
	void MySetCurrentCheckingTime();
	COleDateTime m_timeChecking;
	void MyStartTimer();
	void setUnHookKeyBoard();
	bool setHookKeyBoard();
	void myShowContextNotMenu();
	int TrayMessage(DWORD dwMessage);	
	void myShowDlg(bool ins_isShow);
	bool m_visible;
	static void StringSplit(const CString &str, CStringArray &arr, TCHAR chDelimitior);
	void MyDeleteItemDLL(int index);
	void MyAddItemDLL(CString strLineAdd);
	void MyClearMem();
	void MyRefreshListReport(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote);
	void MyUnUseDLL();
	void MyUseDLL();
	void MyGetArrRecordNote();
//	CString m_fileStore;
	CDailyNoteDlg(CWnd* pParent = NULL);	// standard constructor
	CTypedPtrArray<CObArray, CRecordNote*> m_RecordNoteArray;

	//status variable
	enum TimerStatus {
       MYSTOP = 0,
       MYRUN = 1,
       MYPAUSE = 2,
    };
	TimerStatus m_myStatus;
	TimerStatus m_myStatusMailSlot;
	TimerStatus m_myStatusDramas;
	
	//thread timer
	static UINT MyTimer(LPVOID pParam);
	static UINT MyThreadMailSlot(LPVOID pParam);
	static UINT MyThreadDramas(LPVOID pParam);
	CWinThread *m_pTimerThread;
	CWinThread *m_pTimerThreadML;
	CWinThread *m_pTimerThreadDramas;
	CWinThread *m_pMessageBoxThread;
	static UINT MyThreadMessageBox(LPVOID pParam);
	void MyStartThreadMessageBox(CString sMsg);
	CString m_sMsgContain;


// Dialog Data
	//{{AFX_DATA(CDailyNoteDlg)
	enum { IDD = IDD_DAILYNOTE_DIALOG };
	MyTabCtrl	m_tbCtrl;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDailyNoteDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual int DoModal();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

private:	
	bool FixF2TotalCommand();
	void FixShowTrayIcon();
	HWND hHelper;
	CFont m_font;
// Implementation
protected:	
	HICON m_hIcon;
	HINSTANCE hUpdateRecordDll;

	//KEYBOARD HOOK
	HHOOK oldKeyBoardHook;	
	HINSTANCE hmeDll;	
	MYKBPROC pSetHHook;
	MYKBPROC1 pSetHHook1;
	MYKBPROC2 pSetHHook2;
	MYKBPROC3 pSetHWNDRectMsg;
	HMODULE hModuleKeyboard; //key

	// Generated message map functions
	//{{AFX_MSG(CDailyNoteDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnbtnHide();
	afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
	afx_msg void OnbtnExit();
	afx_msg void OnpmMitExit();
	afx_msg void OnpmMitShow();
	afx_msg LRESULT OnCheckTime(WPARAM, LPARAM);
	afx_msg LRESULT OnDisableBroadCast(WPARAM, LPARAM);
	afx_msg LRESULT OnReadMailSlot(WPARAM, LPARAM);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAILYNOTEDLG_H__C7CD3AF7_CC47_4944_B4AE_E43E88F62D51__INCLUDED_)
