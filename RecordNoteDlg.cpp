// RecordNoteDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DailyNote.h"
#include "RecordNoteDlg.h"
#include "DailyNoteDLg.h"
#include "MAINTAB.h"
#include "DateTimeFormat.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRecordNoteDlg dialog


CRecordNoteDlg::CRecordNoteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRecordNoteDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRecordNoteDlg)
	m_txtMsgValue = _T("");
	//}}AFX_DATA_INIT
}


void CRecordNoteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRecordNoteDlg)
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_dtpDateTime);
	DDX_Text(pDX, IDC_EDIT1, m_txtMsgValue);
	DDX_Control(pDX, rbOnce, m_rbOnceCtrl);
	DDX_Control(pDX, rbDaily, m_rbDailyCtrl);
	DDX_Control(pDX, rbWeekly, m_rbWeeklyCtrl);
	DDX_Control(pDX, rbMonthly, m_rbMonthlyCtrl);	
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRecordNoteDlg, CDialog)
	//{{AFX_MSG_MAP(CRecordNoteDlg)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRecordNoteDlg message handlers

BOOL CRecordNoteDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//set font to main dialog font
	CDailyNoteDlg* parentWnd = ((CDailyNoteDlg*)GetParent());
	SendMessageToDescendants(WM_SETFONT,
		(WPARAM)parentWnd->GetFont()->m_hObject,
		MAKELONG(FALSE, 0), 
		FALSE);
	//set font of CEdit textbox
	//setRtbFont(_T("Tahoma"));

	//set format of dtpDateTime
	DateTime_SetFormat(m_dtpDateTime, _T("dd'/'MM'/'yyyy' 'hh':'mm' 'tt"));

	//get values init
	//m_isNew = true;
	getValues();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRecordNoteDlg::getValues()
{
	if(!m_isNew)
	{
		if(!m_msgText.IsEmpty())
		{
			m_txtMsgValue = m_msgText;
			DateTime_SetSystemtime(m_dtpDateTime,GDT_VALID, &m_editSysTime);
			//
			SetFreRb(GetFrequencyEnum(m_strFrequency));
			UpdateData(false);
		}
	}
	else
	{
		m_txtMsgValue = "";
		//m_dtpDateTime
		SetFreRb(ONCE);
		//m_rbOnceCtrl.SetCheck(true);
		UpdateData(false);
	}
}

void CRecordNoteDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(true);
	if(m_txtMsgValue.IsEmpty())
	{
		MessageBox(_T("You must enter value in the textbox!"));	
		return;
	}
	
	CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;

	TCHAR temp[5];
	CString strAdd;
	CDateTimeFormat dtFormat;
	SYSTEMTIME systime;	
	DateTime_GetSystemtime(m_dtpDateTime, &systime);	
	COleDateTime oleDateTime;
	oleDateTime = systime;
	dtFormat.SetDateTime(oleDateTime);

	dtFormat.SetFormat(_T("dd MMMM yyyy hh:mm tt"));
	CString strFre = GetFrequencyString(GetSelectedFrequencyRb());

	strAdd = dtFormat.GetString(); 
	strAdd += _T(";") + strFre;
	strAdd += _T(";") + m_txtMsgValue;

	CMAINTAB *pDlgMain = (CMAINTAB *)(pMainDlg->m_tbCtrl.m_Dialog[0]);

	if(m_isNew)//add new record
	{
		
		//pDlgMain->m_wndList.ins
		const int IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
		pDlgMain->m_wndList.SetItemText(IDX, 0, _itow(pDlgMain->m_wndList.GetItemCount() - 1, temp, 10));
		pDlgMain->m_wndList.SetItemText(IDX, 1, dtFormat.GetString());
		pDlgMain->m_wndList.SetItemText(IDX, 2, strFre);
		pDlgMain->m_wndList.SetItemText(IDX, 3, m_txtMsgValue);
		
		//MessageBox(strAdd);

		CDailyNoteDlg* pMainFrom = (CDailyNoteDlg*) AfxGetMainWnd();
		pMainFrom->MyAddItemDLL(strAdd);

		CRecordNote* pNewRectNote = new CRecordNote(strAdd);

		pMainFrom->m_RecordNoteArray.Add(pNewRectNote);
		//increase the index of selected current record
		//pMainFrom->m_currentIndexWaitingToShow--;
	}
	else
	{
		//edit record		
		//const int IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
		//pDlgMain->m_wndList.SetItemText(m_indexItemUpdating, 0, _itow(pDlgMain->m_wndList.GetItemCount() - 1, temp, 10));
		pDlgMain->m_wndList.SetItemText(m_indexItemUpdating, 1, dtFormat.GetString());
		pDlgMain->m_wndList.SetItemText(m_indexItemUpdating, 2, strFre);
		pDlgMain->m_wndList.SetItemText(m_indexItemUpdating, 3, m_txtMsgValue);

		//m_dlgRecordNote.m_msgText = m_wndList.GetItemText(nItem,1);
		//m_wndList.DeleteItem(nItem);
		//MyRefreshSTT();

		//delete item in list record
		

		pMainDlg->m_RecordNoteArray.GetAt(m_indexItemUpdating)->m_line = strAdd;

		CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
		pMainDlg->MyDeleteItemDLL(-1);
	}

	CDialog::OnOK();
}

CString CRecordNoteDlg::GetFrequencyString(eFrequency eFre)
{
	CString sRs = "";
	switch(eFre)
	{
	case ONCE:
		sRs = "ONCE";
		break;
	case DAILY:
		sRs = "DAILY";
		break;
	case WEEKLY:
		sRs = "WEEKLY";
		break;
	case MONTHLY:
		sRs = "MONTHLY";
		break;
	default:
		sRs = "ONCE";
		break;
	}
	return sRs;
}



void CRecordNoteDlg::SetFreRb(eFrequency eFre)
{
	switch(eFre)
	{
	case ONCE:
		m_rbOnceCtrl.SetCheck(true);
		break;
	case DAILY:
		m_rbDailyCtrl.SetCheck(true);
		break;
	case WEEKLY:
		m_rbWeeklyCtrl.SetCheck(true);		
		break;
	case MONTHLY:
		m_rbMonthlyCtrl.SetCheck(true);		
		break;
	default:
		m_rbOnceCtrl.SetCheck(true);		
		break;
	}
}

eFrequency CRecordNoteDlg::GetFrequencyEnum(CString strFre)
{
	eFrequency eRs = ONCE;
	//CString sCase("AAAA");
	STR_SWITCH(strFre)      //Start of switch
	{                        //Opening and closing braces
		//NOT MANDATORY for switch.
		STR_CASE(_T("ONCE"))
		{                      //MANDATORY for case.
			eRs = ONCE;
			break;               //break has to in braces of case
		}  
		STR_CASE(_T("DAILY"))
		{                      //MANDATORY for case.
			eRs = DAILY;
			break;               //break has to in braces of case
		}                      //Opening and closing braces
		//MANDATORY for case.
		
		STR_CASE(_T("WEEKLY"))
		{
			eRs = WEEKLY;
			break;
		}
		STR_CASE(_T("MONTHLY"))
		{
			eRs = MONTHLY;
			break;
		}
		DEFAULT_CASE()
		{
			eRs = ONCE;//Default handling if any
			break;
		}
	}                        //Opening and closing braces
	//NOT MANDATORY for switch
	STR_SWITCH_END()         //MANDATORY statement
		

	return eRs;
}

eFrequency CRecordNoteDlg::GetSelectedFrequencyRb()
{
	eFrequency eRs = ONCE;

	if( BST_CHECKED == m_rbOnceCtrl.GetCheck())
		eRs = ONCE;
	else
		if(m_rbDailyCtrl.GetCheck() == BST_CHECKED)
			eRs = DAILY;
		else
			if(m_rbWeeklyCtrl.GetCheck() == BST_CHECKED)
				eRs = WEEKLY;
			else
				if(m_rbMonthlyCtrl.GetCheck() == BST_CHECKED)
					eRs = MONTHLY;
				else
					eRs = DAILY;
	return eRs;
	//return DAILY;
			
}

void CRecordNoteDlg::setRtbFont(CString ins_strFont)
{
	CDC *pDC = GetDC();

	// create UNICODE font
	LOGFONT lf;
	
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight =
	MulDiv(25, ::GetDeviceCaps(pDC->m_hDC,
		 LOGPIXELSY), 172);
	lf.lfWeight = FW_NORMAL;
	lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	wcscpy(lf.lfFaceName, ins_strFont);
	if(NULL == (HFONT) m_font)
		m_font.CreateFontIndirect(&lf);

	// apply the font to the controls
	//m_list.SetFont(&m_font);
	//m_edit.SetFont(&m_font);
	CRichEditCtrl* pTxtToSet;
	pTxtToSet = (CRichEditCtrl*) GetDlgItem(txtMsg);
	pTxtToSet->SetFont(&m_font);
	// release the device context.
	ReleaseDC(pDC);
}



void CRecordNoteDlg::OnDestroy()
{
	CDialog::OnDestroy();	
	// TODO: Add your message handler code here	

	//When you no longer need the font, call the DeleteObject function to delete the font.
	//especially if you have a sub dialog
	m_font.DeleteObject();
}

//DEL int CRecordNoteDlg::DoModal() 
//DEL {
//DEL 	// TODO: Add your specialized code here and/or call the base class
//DEL 	CDialogTemplate dlt;
//DEL 	int nResult;
//DEL 	// load dialog template
//DEL 	if (!dlt.Load(MAKEINTRESOURCE(CRecordNoteDlg::IDD))) return -1;
//DEL 	// set your own font, for example �Arial�, 10 pts.
//DEL 	dlt.SetFont(_T("Arial"), 10);
//DEL 	// get pointer to the modified dialog template
//DEL 	LPSTR pdata = (LPSTR)GlobalLock(dlt.m_hTemplate);
//DEL 	// let MFC know that you are using your own template
//DEL 	m_lpszTemplateName = NULL;
//DEL 	InitModalIndirect(pdata);
//DEL 	// display dialog box
//DEL 	nResult = CDialog::DoModal();
//DEL 	// unlock memory object
//DEL 	GlobalUnlock(dlt.m_hTemplate);
//DEL 	return nResult;
//DEL 	//	return CDialog::DoModal();
//DEL }
