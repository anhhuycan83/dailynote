////// MAINTAB.cpp : implementation file

#include "stdafx.h"
#include "DailyNote.h"
#include "MAINTAB.h"
#include "DailyNoteDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMAINTAB dialog


CMAINTAB::CMAINTAB(CWnd* pParent /*=NULL*/)
	: CDialog(CMAINTAB::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMAINTAB)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


//CMAINTAB::CMAINTAB(CWnd* pParent /*=NULL*/)
//: CDialog(CMAINTAB::IDD, pParent)
//{
//    {{AFX_DATA_INIT(CMAINTAB)
//     NOTE: the ClassWizard will add member initialization here
//    }}AFX_DATA_INIT
//}

void CMAINTAB::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMAINTAB)
	DDX_Control(pDX, btnEdit, m_btnEdit);
	DDX_Control(pDX, btnDelete, m_btnDelete);
	DDX_Control(pDX, IDC_LIST1, m_wndList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMAINTAB, CDialog)
	//{{AFX_MSG_MAP(CMAINTAB)
	ON_BN_CLICKED(btnAdd, OnbtnAdd)
	ON_BN_CLICKED(btnEdit, OnbtnEdit)
	ON_BN_CLICKED(btnDelete, OnbtnDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



BOOL CMAINTAB::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//set font to main dialog font
	CDailyNoteDlg* parentWnd = ((CDailyNoteDlg*)GetParent());
	SendMessageToDescendants(WM_SETFONT,
		(WPARAM)parentWnd->GetFont()->m_hObject,
		MAKELONG(FALSE, 0), 
		FALSE);

	
	m_wndList.SetGridLines(TRUE); // SHow grid lines
	m_wndList.SetCheckboxeStyle(RC_CHKBOX_NONE); // Enable checkboxes
	m_wndList.SetEditable(FALSE); // Allow sub-text edit
	

	//m_wndList.SortItems(0, TRUE); // sort the 1st column, ascending
	//m_bSortable = m_wndList.IsSortable();
	UpdateData(FALSE);
	
	//GetDlgItem(IDC_ALLOWSORT)->EnableWindow(m_wndList.HasColumnHeader());

	// now play some colorful stuff
	
	// Set the 3rd column background color to yellow, text color to blue
	m_wndList.SetItemTextColor(-1, 2, RGB(0, 0, 0));
	//m_wndList.SetItemBkColor(-1, 2, RGB(255, 255, 0));

	//m_pRecordNoteDlg = 0;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMAINTAB::_StringSplit(const CString &str, CStringArray &arr, TCHAR chDelimitior)
{

}

void CMAINTAB::OnbtnAdd() 
{
	// TODO: Add your control notification handler code here
	//m_dlgRecordNote.m_msgText = _T("khoi tao");

	m_dlgRecordNote.m_isNew = true;//set to mode new
	m_dlgRecordNote.m_msgText.Empty();
	int nResponse = m_dlgRecordNote.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
		CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
		pMainDlg->MyReRunTimer();
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

}

void CMAINTAB::OnbtnEdit() 
{
	// TODO: Add your control notification handler code here
	COleDateTime oleDT;

	POSITION pos = m_wndList.GetFirstSelectedItemPosition();
	int nItem = m_wndList.GetNextSelectedItem(pos);
	//m_wndList.GetItemText(nItem,0);

	
	//DateTime_SetSystemtime(m_dlgRecordNote.m_dtpDateTime,
	CString temp = m_wndList.GetItemText(nItem,1);	
	//MessageBox(temp);
	oleDT.ParseDateTime(temp);
	//1066 is vietnam datetime
	//if(!oleDT.ParseDateTime(temp, LOCALE_NOUSEROVERRIDE, 1066))
	//		oleDT.ParseDateTime(temp);

	SYSTEMTIME systime;
	oleDT.GetAsSystemTime(systime);
	//m_dlgRecordNote.m_msgText = m_wndList.GetItemText(nItem,2);
	m_dlgRecordNote.m_strFrequency = m_wndList.GetItemText(nItem,2);
	m_dlgRecordNote.m_msgText = m_wndList.GetItemText(nItem,3);
	m_dlgRecordNote.m_editSysTime = systime;	

	//set config values
	m_dlgRecordNote.m_isNew = false;//set to mode edit
	m_dlgRecordNote.m_indexItemUpdating = nItem;

	int nResponse = m_dlgRecordNote.DoModal();
	

	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
		CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
		pMainDlg->MyReRunTimer();
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}	
}

void CMAINTAB::OnbtnDelete() 
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_wndList.GetFirstSelectedItemPosition();
	int nItem = m_wndList.GetNextSelectedItem(pos);
	//m_wndList.GetItemText(nItem,0);

	//m_dlgRecordNote.m_msgText = m_wndList.GetItemText(nItem,1);
	m_wndList.DeleteItem(nItem);
	MyRefreshSTT();

	//delete item in list record
	CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
	pMainDlg->MyDeleteItemDLL(nItem);

	pMainDlg->m_RecordNoteArray.RemoveAt(nItem);

//	CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
	pMainDlg->MyReRunTimer();

	//int nResponse = m_dlgRecordNote.DoModal();
	MySetStatusBtn();
}

void CMAINTAB::MyRefreshSTT()
{
	TCHAR temp[5];	
	for (int i = 0; i < m_wndList.GetItemCount(); i++)
	{
		m_wndList.SetItemText(i,0,_itow(i, temp, 10));
	}
}

void CMAINTAB::MyRefreshListReport(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote)
{
	CStringArray arrStr;

	//CMAINTAB *pDlgMain = (CMAINTAB *)(m_tbCtrl.m_Dialog[0]);
	//pDlgMain->m_wndList.ins
	//pDlgMain->m_wndList.SetColumnHeader(_T("STT, 50; Date time, 200; Frequency, 50; Message, 250, 2"));
	m_wndList.SetColumnHeader(_T("STT, 30; Date time, 170; Frequency, 80; Message, 240"));
	m_wndList.SetSortable(false);

	CRecordNote *pRectNote;
	TCHAR temp[5];	
	int i;
	for (i = 0; i < arrayRecordNote.GetSize(); i++)
	{
		pRectNote = arrayRecordNote.GetAt(i);
		// arrStr
		CDailyNoteDlg::StringSplit(pRectNote->m_line, arrStr, _T(';'));
		const int IDX = m_wndList.InsertItem(i, _T(""));
		m_wndList.SetItemText(IDX, 0, _itow(i, temp, 10));
		m_wndList.SetItemText(IDX, 1, arrStr.GetAt(0));
		m_wndList.SetItemText(IDX, 2, arrStr.GetAt(1));
		m_wndList.SetItemText(IDX, 3, arrStr.GetAt(2));
	}
	MySetStatusBtn();
}

void CMAINTAB::MySetStatusBtn()
{
	if(m_wndList.RefreshSelectedItem() >= 0)
	{
		m_btnDelete.EnableWindow();
		m_btnEdit.EnableWindow();
	}
	else
	{
		m_btnDelete.EnableWindow(false);
		m_btnEdit.EnableWindow(false);
	}
}



//void CMAINTAB::OnKeydownList1(NMHDR* pNMHDR, LRESULT* pResult) 
//{
//	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
//	// TODO: Add your control notification handler code here
//	if ( pLVKeyDow->wVKey == VK_RETURN )
//	{
//		AfxMessageBox(_T("test"));
//	}
//	
//	*pResult = 0;
//}

BOOL CMAINTAB::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	switch (wParam)
	{
	case IDOK:
		//AfxMessageBox(_T("test"));
		return FALSE;
		break;		
	default:break;
	}
	return CDialog::OnCommand(wParam, lParam);
}
