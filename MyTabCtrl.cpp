// MyTabCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "DailyNote.h"
#include "MyTabCtrl.h"
#include "MyTabCtrl.h"
#include "MAINTAB.h"
#include "OptionTab.h"
#include "NetSendTab.h"
#include "TabMessage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MyTabCtrl

MyTabCtrl::MyTabCtrl()
{
	m_DialogID[0] =IDD_DIALOG1;
	m_DialogID[1] =IDD_DIALOG2;
	m_DialogID[2] =IDD_DIALOG3;
	m_DialogID[3] = IDD_DGLMESSAGE;


	m_Dialog[0] = new CMAINTAB();
	m_Dialog[1] = new COptionTab();
	m_Dialog[2] = new CNetSendTab();
	m_Dialog[3] = new CTabMessage();

	m_nPageCount = 4;
}



//This function creates the Dialog boxes once
void MyTabCtrl::InitDialogs()
{
	m_Dialog[0]->Create(m_DialogID[0],GetParent());
	m_Dialog[1]->Create(m_DialogID[1],GetParent());
	m_Dialog[2]->Create(m_DialogID[2],GetParent());
	m_Dialog[3]->Create(m_DialogID[3],GetParent());
}

MyTabCtrl::~MyTabCtrl()
{
}


BEGIN_MESSAGE_MAP(MyTabCtrl, CTabCtrl)
	//{{AFX_MSG_MAP(MyTabCtrl)
	ON_NOTIFY_REFLECT(TCN_SELCHANGE, OnSelchange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MyTabCtrl message handlers

void MyTabCtrl::OnSelchange(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	ActivateTabDialogs();

	*pResult = 0;
}

 void MyTabCtrl::ActivateTabDialogs()
{
	int nSel = GetCurSel();
	if(m_Dialog[nSel]->m_hWnd)
	 m_Dialog[nSel]->ShowWindow(SW_HIDE);

	CRect l_rectClient;
	CRect l_rectWnd;

	GetClientRect(l_rectClient);
	AdjustRect(FALSE,l_rectClient);
	GetWindowRect(l_rectWnd);
	GetParent()->ScreenToClient(l_rectWnd);
	l_rectClient.OffsetRect(l_rectWnd.left,l_rectWnd.top);
	for(int nCount=0; nCount < m_nPageCount; nCount++){
	 m_Dialog[nCount]->SetWindowPos(&wndTop, l_rectClient.left, l_rectClient.top, l_rectClient.Width(), l_rectClient.Height(), SWP_HIDEWINDOW);
	}
	m_Dialog[nSel]->SetWindowPos(&wndTop, l_rectClient.left, l_rectClient.top, l_rectClient.Width(), l_rectClient.Height(), SWP_SHOWWINDOW);

	m_Dialog[nSel]->ShowWindow(SW_SHOW);

}
