// History.cpp : implementation file
//

#include "stdafx.h"
#include "DailyNote.h"
#include "History.h"
#include "DailyNoteDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHistory dialog


CHistory::CHistory(CWnd* pParent /*=NULL*/)
	: CDialog(CHistory::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHistory)
	//}}AFX_DATA_INIT
}


void CHistory::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHistory)
	DDX_Control(pDX, txtHistoryMsg, m_txtHistoryMsgCtrl);
	DDX_Control(pDX, m_wndListHistoryView, m_wndListHistoryCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHistory, CDialog)
	//{{AFX_MSG_MAP(CHistory)
	ON_NOTIFY(LVN_ITEMCHANGED, m_wndListHistoryView, OnItemchangedwndListHistoryView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHistory message handlers

void CHistory::LoadHistory()
{
	if(m_pListHistory == NULL)
		return;
	for(int i = 0; i<m_pListHistory->GetSize(); i++)
	{
		AddNewMsg((CHistoryMsg*) m_pListHistory->GetAt(i));
	}
}

BOOL CHistory::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//set font to main dialog font
	CDailyNoteDlg* parentWnd = ((CDailyNoteDlg*)GetParent());
	SendMessageToDescendants(WM_SETFONT,
		(WPARAM)parentWnd->GetFont()->m_hObject,
		MAKELONG(FALSE, 0), 
		FALSE);
	
	// TODO: Add extra initialization here
	m_wndListHistoryCtrl.SetColumnHeader(_T("No, 22, 1; Time, 45, 1; Name, 65, 1; Msg, 120, 1; To, 65, 1"));
	m_wndListHistoryCtrl.SetGridLines(TRUE); // SHow grid lines
	m_wndListHistoryCtrl.SetCheckboxeStyle(RC_CHKBOX_NONE); // Enable checkboxes
	m_wndListHistoryCtrl.SetEditable(FALSE); // Allow sub-text edit
	

	//m_wndListHistoryCtrl.SortItems(0, TRUE); // sort the 1st column, ascending
	//m_bSortable = m_wndListHistoryCtrl.IsSortable();
	UpdateData(FALSE);
	
	//GetDlgItem(IDC_ALLOWSORT)->EnableWindow(m_wndListHistoryCtrl.HasColumnHeader());

	// now play some colorful stuff
	
	// Set the 3rd column background color to yellow, text color to blue
	m_wndListHistoryCtrl.SetItemTextColor(-1, 2, RGB(0, 0, 0));
	//m_wndListHistoryCtrl.SetItemBkColor(-1, 2, RGB(255, 255, 0));

	LoadHistory();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHistory::AddNewMsg(CHistoryMsg *hisItem)
{	
	if(hisItem->m_strArrMsgHistory.GetSize()==4)
	{
		TCHAR temp[5];
//MYDEL		CDateTimeFormat dtFormat;
//MYDEL		COleDateTime oleDTNow = COleDateTime::GetCurrentTime();
//MYDEL		dtFormat.SetDateTime(oleDTNow);
//MYDEL		dtFormat.SetFormat(_T("h:mm"));
		//0: from, 1: to, 2: msg
		//item: 0: stt, 1: time, 2: from, 3: msg, 4: to

		const int IDX = m_wndListHistoryCtrl.InsertItem(0, _T(""));
		m_wndListHistoryCtrl.SetItemText(IDX, 0, _itow(m_wndListHistoryCtrl.GetItemCount(), temp, 10));
		m_wndListHistoryCtrl.SetItemText(IDX, 1, hisItem->m_strArrMsgHistory.GetAt(3));//dtFormat.GetString()
		m_wndListHistoryCtrl.SetItemText(IDX, 2, hisItem->m_strArrMsgHistory.GetAt(0));
		m_wndListHistoryCtrl.SetItemText(IDX, 3, hisItem->m_strArrMsgHistory.GetAt(2));
		m_wndListHistoryCtrl.SetItemText(IDX, 4, hisItem->m_strArrMsgHistory.GetAt(1));
	}
}

void CHistory::OnOK() 
{
	// TODO: Add extra validation here
//MYDEL	int nItem = 0;
//MYDEL	int nextItem = -1;
//MYDEL	POSITION pos = m_wndListHistoryCtrl.GetFirstSelectedItemPosition();
//MYDEL	
//MYDEL	if(pos != NULL)
//MYDEL	{		
//MYDEL		nItem = m_wndListHistoryCtrl.GetNextSelectedItem(pos);
//MYDEL		m_strMsgSelected.Format(_T("Message from %s to %s at %s\r\n\r\n%s"),
//MYDEL			m_wndListHistoryCtrl.GetItemText(nItem, 2),
//MYDEL			m_wndListHistoryCtrl.GetItemText(nItem, 4),
//MYDEL			m_wndListHistoryCtrl.GetItemText(nItem, 1),
//MYDEL			m_wndListHistoryCtrl.GetItemText(nItem, 3));
//MYDEL	}
	CDialog::OnOK();
}

void CHistory::OnItemchangedwndListHistoryView(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	if( pNMListView->uNewState & LVIS_SELECTED )
	{		
		int selectedItem = pNMListView->iItem;
		CString strMsg;
		//stt - 0, hour - 1, from - 2, msg - 3, To - 4
//MYDEL		strMsg.Format(_T("Message from %s to %s at %s\r\n\r\n%s"),
//MYDEL			m_wndListHistoryCtrl.GetItemText(selectedItem, 2),
//MYDEL			m_wndListHistoryCtrl.GetItemText(selectedItem, 4),
//MYDEL			m_wndListHistoryCtrl.GetItemText(selectedItem, 1),
//MYDEL			m_wndListHistoryCtrl.GetItemText(selectedItem, 3));
		strMsg.Format(_T("%s"),m_wndListHistoryCtrl.GetItemText(selectedItem, 3));
		m_txtHistoryMsgCtrl.SetWindowText(strMsg);
		//m_strMsgSelected.Format(_T("%s"),m_wndListHistoryCtrl.GetItemText(selectedItem, 3));
		m_strMsgSelected = strMsg;
	}
	*pResult = 0;
}
