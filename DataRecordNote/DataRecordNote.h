// DataRecordNote.h : main header file for the DATARECORDNOTE DLL
//

#if !defined(AFX_DATARECORDNOTE_H__47763319_E421_4A09_866C_F03B3406C8BC__INCLUDED_)
#define AFX_DATARECORDNOTE_H__47763319_E421_4A09_866C_F03B3406C8BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include "afxtempl.h" //for list object

/////////////////////////////////////////////////////////////////////////////

#define DLLEXPORT __declspec(dllexport)
#define DLLIMPORT __declspec(dllimport)

// CDataRecordNoteApp
// See DataRecordNote.cpp for the implementation of this class
//
class CRecordNote : public CObject
{
public:	
	//CString m_strTime;
	//CString m_strMessage;
	CString m_line;

public:
	//CRecordNote(CString strTime, CString strMessage);
	CRecordNote(CString strLine);
};


class CDataRecordNoteApp : public CWinApp
{
public:
	int GetNumberRecordNote();
	CRecordNote* GetRecordNoteAt(int index);
	void AddRecordNote(CRecordNote *pRecordNote);
	CDataRecordNoteApp();
	CTypedPtrArray<CObArray, CRecordNote*> m_RecordNoteArray;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataRecordNoteApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CDataRecordNoteApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATARECORDNOTE_H__47763319_E421_4A09_866C_F03B3406C8BC__INCLUDED_)
