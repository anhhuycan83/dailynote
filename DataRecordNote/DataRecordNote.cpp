// DataRecordNote.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "DataRecordNote.h"
#include "textfile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CDataRecordNoteApp

BEGIN_MESSAGE_MAP(CDataRecordNoteApp, CWinApp)
	//{{AFX_MSG_MAP(CDataRecordNoteApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataRecordNoteApp construction

CDataRecordNoteApp::CDataRecordNoteApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDataRecordNoteApp object

CDataRecordNoteApp theApp;


//CRecordNote::CRecordNote(CString strTime, CString strMessage)
CRecordNote::CRecordNote(CString strLine)
{
	//m_strTime = strTime;
	//m_strMessage = strMessage;
	m_line = strLine;
}

void CDataRecordNoteApp::AddRecordNote(CRecordNote *pRecordNote)
{
	m_RecordNoteArray.Add(pRecordNote);
}

CRecordNote* CDataRecordNoteApp::GetRecordNoteAt(int index)
{
	if(index>=0 && index <= m_RecordNoteArray.GetUpperBound())
	{
		return m_RecordNoteArray.GetAt(index);
	}
	else
	{
		return 0;
	}
}

int CDataRecordNoteApp::GetNumberRecordNote()
{
	return m_RecordNoteArray.GetSize();
}

extern "C" DLLEXPORT void GetListRecordNote(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath)
{
	//AfxMessageBox(filePath);
	CTextFileRead myfile(filePath);
	
	ASSERT(myfile.IsOpen());
	CString line;
	while(!myfile.Eof())
	{		
		myfile.ReadLine(line);
		if(!line.IsEmpty())
		{
			CRecordNote *pRecordNote = new CRecordNote(line);
			arrayRecordNote.Add(pRecordNote);
		}
	}
}

extern "C" DLLEXPORT void DelRecordNote(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath, int index)
{
	CTextFileWrite myfile(filePath, 
            CTextFileWrite::UTF_8);

	ASSERT(myfile.IsOpen());

	for(int i = 0; i < arrayRecordNote.GetSize(); i++)
	{
		if(i != index)
		{
			CRecordNote* pRecordNote = arrayRecordNote.GetAt(i);
			myfile.Write(pRecordNote->m_line);
			myfile.WriteEndl();
		}
	}
	myfile.Close();
}

extern "C" DLLEXPORT void AddRecordNote(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath, CString strLineAdd)
{
	CTextFileWrite myfile(filePath, 
            CTextFileWrite::UTF_8);

	ASSERT(myfile.IsOpen());

	for(int i = 0; i < arrayRecordNote.GetSize(); i++)
	{	
		CRecordNote* pRecordNote = arrayRecordNote.GetAt(i);
		myfile.Write(pRecordNote->m_line);
		myfile.WriteEndl();
	}
	myfile.Write(strLineAdd);
	myfile.WriteEndl();
	myfile.Close();
}

