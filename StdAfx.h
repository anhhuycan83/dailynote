// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//
#if !defined(AFX_STDAFX_H__2F921E4C_7A66_4A90_904B_D69363CFD9FE__INCLUDED_)
#define AFX_STDAFX_H__2F921E4C_7A66_4A90_904B_D69363CFD9FE__INCLUDED_

#define _WIN32_WINNT 0x0500
#define _WIN32_IE 0x0500
#define WINVER 0x0500 



#if _MSC_VER > 1000


#pragma once
#endif // _MSC_VER > 1000



#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <windows.h>

//My define
#include <AFXPRIV.H> // for set default font for CDialog
#include <mmsystem.h> //for mute sound
#include <afxtempl.h> //for CTypedPtrArray
//define for file save dialog
#define MAX_CFileDialog_FILE_COUNT 99
#define FILE_LIST_BUFFER_SIZE ((MAX_CFileDialog_FILE_COUNT * (MAX_PATH + 1)) + 1)



#define ONCE_INSTANCE "DailyNote"
#define MYVERSION "1.0.4.3"
#define MYWM_NOTIFYICON (WM_USER + 2)
#define MYWM_KEYPRESS_F10 (WM_USER + 3) //F10
#define MYWM_KEYPRESS_F11 (WM_USER + 4) //F11
#define MYWM_CHECKTIME (WM_USER + 5) //on check time
#define MYWM_DISABLEBROADCAST (WM_USER + 6) //on check disable check box broadcast on netsend GUI
#define MYWM_KEYPRESS_F2 (WM_USER + 7) //F2 for mute sound
#define MYWM_MTHELPER_TOGGLE_MUTE (WM_USER + 8) //for sending message to C# form
#define ICON_VALUE1 1
#define MYWM_MEDIA_TOGGLE_PLAY (WM_USER + 9) //for sending message left click to Play button in WMP
#define MYWM_MEDIA_NEXT (WM_USER + 10) //for sending message left click to next button in WMP
#define MYWM_SHOW_TOTAL_APP (WM_USER + 11) //for sending message left click to total commander
#define MYWM_RESIZE_REMOTE_DESKTOP (WM_USER + 12) // Alt + Ctrl + S for resize remote desktop
#define CLASS_SIZE 500 //for get window title text
#define MYWM_READMAILSLOT (WM_USER + 13) //start new my thread check netsend message frequently
#define MYWM_F2_TUYEN (WM_USER + 14) //start new my thread check netsend message frequently
#define MYWM_IE_DRAMAS (WM_USER + 15) //Togle OnTop for Dramas in IE
#define MYWM_FORCEGROUND_IE_DRAMAS (WM_USER + 16) //Togle OnTop for Dramas in IE
#define MYWM_FF_DRAMAS (WM_USER + 15) //Togle OnTop for Dramas in FF
#define MYWM_FORCEGROUND_FF_DRAMAS (WM_USER + 16) //Togle OnTop for Dramas in FF
#define MYWM_RUN_ALL_SEN1K (WM_USER + 17) // Alt + F12 for running all Sen1K
#define MYWM_RUN_ALL_SEN1K_MULTI (WM_USER + 18) // Alt + F11 for running all Sen1K Multi
#define MY_VIDEO_MONITOR_CLIENT_CLICK _T("MyVideoMonitorClientClick")
//#define CAPTION_TOTALCOMMANDER_75 "Total Commander 7.50 - INTEL MATAM HAIFA"
//WM_USER = 0x0400

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__2F921E4C_7A66_4A90_904B_D69363CFD9FE__INCLUDED_)
