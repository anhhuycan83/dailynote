#if !defined(AFX_TABMESSAGE_H__8A496061_7557_494F_A989_8B799A61F253__INCLUDED_)
#define AFX_TABMESSAGE_H__8A496061_7557_494F_A989_8B799A61F253__INCLUDED_

#include "Crypto.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TabMessage.h : header file
//
#include "ReportCtrl.h"
#include "DlgPassword.h"	// Added by ClassView
#include "Crypto.h"
/////////////////////////////////////////////////////////////////////////////
// CTabMessage dialog

//MYDELstruct tHistoryMsg
//MYDEL{
//MYDEL	CStringArray m_strArrMsgHistory;
//MYDEL};
//MYDELtypedef tHistoryMsg HistoryMsg;

class CHistoryMsg : public CObject
{
public:
	CStringArray m_strArrMsgHistory;
};

class CTabMessage : public CDialog
{
// Construction
public:
	CString m_password;
	MFC::CCrypto crypto;
	CString m_localName;
	void SaveMsgDefault();
	void MyClearMem();
	CTypedPtrArray<CObArray, CHistoryMsg*> m_historyArr;
	void ShowExpand();
	void ShowLitle();
	bool m_bIsExpandMode;
	bool AddItemFromFile(CStringArray *sArrMsgRow, int rowIndex);
	bool LoadMsgNS(CString sFullFileName);
	int SaveMsgToFile(CString fullFileName);
	void LoadSettingFlashWindow();
	bool m_bIsFlashWindow;
	CString m_strMailSlotFolder60;
	CString m_strDefaultSender;
	void SendForward(CString strTo, CString strMsg);
	bool SendMailSlotPackage(HANDLE hML, CStringArray *arrPackage, CString strMsgItem);
	bool SendMsgPackage(CStringArray &strArr);
	//void SendMsg(CString strMsgTo, CString strMsgContent);
	int LastIndexOf(const CString& strSource, const CString& strSub);
//MYDEL	void UpdateMsgItem(CStringArray *arrMsg, int index);
	void ConvertCharToCString(CString &s, char* a);
	void AddNewMsg(CStringArray *arrMsg);
	void StringSplit(const CString &str, CStringArray &arr, TCHAR chDelimitior);
	DWORD GetMessage();
	HANDLE CreateServer();
	HANDLE m_hServer;
	CTabMessage(CWnd* pParent = NULL);   // standard constructor
	~CTabMessage();

// Dialog Data
	//{{AFX_DATA(CTabMessage)
	enum { IDD = IDD_DGLMESSAGE };
	CButton	m_btnHistoryCtrl;
	CButton	m_chbProtectCtrl;
	CButton	m_btnPreviousNS;
	CButton	m_btnNextNS;
	CButton	m_btnShowModeMsg;
	CButton	m_btnLoadMsgNS;
	CButton	m_chbForward75;
	CButton	m_chbForward60;
	CButton	m_chbHuy;
	CComboBox	m_cbTo;
	CButton	m_btnSendNS;
	CButton	m_btnDeleteNS;
	CStatic	m_lblMsgCountCtrl;
	CReportCtrl	m_wndListNSend;
	CEdit	m_txtMsgRecieveCtrl;
	CEdit	m_txtMsgSentoCtrl;
	CString	m_txtMsgSentoValue;
	CString	m_txtMsgRecieveValue;	
	CString	m_cbToValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabMessage)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTabMessage)
	afx_msg void OnbtnSendNS();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnbtnGet();
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedwndListNSend(NMHDR* pNMHDR, LRESULT* pResult);	
	afx_msg void OnbtnDeleteNS();
	afx_msg void OnSetfocustxtMsgSenTo();
	afx_msg void OnbtnPre();
	afx_msg void OnbtnNext();
	afx_msg void OnbtnSaveMsg();
	afx_msg void OnbtnLoadMsgNS();
	afx_msg void OnbtnShowModeMsg();
	afx_msg void OnchbHuy();
	afx_msg void OnchbForward60();
	afx_msg void OnchbForward75();
	afx_msg void OnbtnHistory();
	afx_msg void OnDestroy();
	afx_msg void OnchbProtect();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CDlgPassword m_dlgPassword;
	void SetLableFont(CStatic *lable, CString ins_strFont, int PointSize, int Weight);
	//void SetRtbFont(CRichEditCtrl *rtbCtrl, CString ins_strFont);
	bool ConfirmCancelSendMsg(CString msgTo);
	CString GetMLServerPath();
	CString GetMailSlotFolderPath(CStringArray *arrMsg);
	CString m_strBroadcastName;
	int m_maxLength;
	int SplitMsg(CStringArray *strArr, CString msg);
	//bool SendMailSlot(HANDLE hML, CString strMsg, CString strMsgTo);
	byte m_sentByte[398];
	CStringArray m_strArrMsgOld;	
	CStringArray m_strArrMulti;
	void GetPCName();
	bool m_bIsFirstType;
	void SetCountNumber();
	void SetMsgCount();
	CFont m_fontLblMsgCount;	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABMESSAGE_H__8A496061_7557_494F_A989_8B799A61F253__INCLUDED_)
