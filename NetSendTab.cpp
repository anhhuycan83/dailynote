// NetSendTab.cpp : implementation file
//

#include "stdafx.h"
#include "DailyNote.h"
#include "NetSendTab.h"
//#include "Windowsx.h"
#include "DailyNoteDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNetSendTab dialog


CNetSendTab::CNetSendTab(CWnd* pParent /*=NULL*/)
	: CDialog(CNetSendTab::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNetSendTab)
	m_chbDisableSendTCVNValue = FALSE;
	m_txtReCheckAfterValue = _T("");
	m_lblCurrentSecond = _T("");
	//}}AFX_DATA_INIT
	m_currentSecond = 0;
}


void CNetSendTab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetSendTab)
	DDX_Control(pDX, txtReCheckAfter, m_txtReCheckAfterCtrl);
	DDX_Control(pDX, chbDisableSendTCVN, m_chbDisableSendTCVNCtrl);
	DDX_Check(pDX, chbDisableSendTCVN, m_chbDisableSendTCVNValue);
	DDX_Text(pDX, txtReCheckAfter, m_txtReCheckAfterValue);
	DDX_Text(pDX, lblCurrentSecond, m_lblCurrentSecond);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNetSendTab, CDialog)
	//{{AFX_MSG_MAP(CNetSendTab)
	ON_BN_CLICKED(btnSaveNetSend, OnbtnSaveNetSend)
	ON_BN_CLICKED(chbDisableSendTCVN, OnchbDisableSendTCVN)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetSendTab message handlers

BOOL CNetSendTab::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	setConfigValue(false);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNetSendTab::setConfigValue(bool bIsSave)
{
	UpdateData();
	if(bIsSave) //set to config file
	{
		CString strTemp;
		//recheck after (minute)
		GetDlgItemText(txtReCheckAfter, strTemp);
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("RECHECKAFTER"), strTemp);

		//DisableSendTCVN
		if(IsDlgButtonChecked(chbDisableSendTCVN))	
		{
			strTemp = _T("1");
		}
		else
		{
			strTemp = _T("0");
		}

		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("DISABLESENDTCVN"), strTemp);
	}
	else
	{
		//recheck after (minute)
		SetDlgItemText(txtReCheckAfter, AfxGetApp()->GetProfileString(_T("INITIAL"), _T("RECHECKAFTER"), _T("10")));
		
		//DisableSendTCVN
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("DISABLESENDTCVN"), _T("1")) == _T("1"))
		{
			m_chbDisableSendTCVNCtrl.SetCheck(true);
		}
		else
		{
			m_chbDisableSendTCVNCtrl.SetCheck(false);
		}
	}
}

void CNetSendTab::OnbtnSaveNetSend() 
{
	// TODO: Add your control notification handler code here
	setConfigValue(true);
	//temp to send mailslot
	
	HANDLE   handle;
	CString strErr;
//	void*    buffer;
	CString strMsg = _T("This is some text");
	
	/* Open the blort mailslot on MyComputer */
	handle = CreateFile(_T("\\\\huy\\mailslot\\netsend"),
		GENERIC_WRITE, 
		FILE_SHARE_READ,
		0, 
		OPEN_EXISTING, 
		FILE_ATTRIBUTE_NORMAL, 
		0); 
	if (handle == INVALID_HANDLE_VALUE) 
	{
		strErr.Format(_T("CreateFile failed: %d\n"), GetLastError());
		AfxMessageBox(strErr);
	}
	//static LPTSTR MyMessage = _T("This is some text");
	TCHAR MyMessage[100] = _T("th\x1EED ti\x1EBFng vi\x1EC7t");
	//TCHAR *MyMessage; //= _T("th\x1EED ti\x1EBFng vi\x1EC7t");
	//MyMessage = new TCHAR[100];
	//MyMessage = _T("th\x1EED ti\x1EBFng vi\x1EC7t");
	
	//CString MyMessage(_T("This is some text"));
	BOOL     err;
	DWORD    numWritten;
	bool bIsCorrect = false;
	//void *   buffer;
	/* Write out our nul-terminated string to a record */
	err = WriteFile(handle, &MyMessage, sizeof(MyMessage), &numWritten, 0);//sizeof(MyMessage)
	
	/* See if an error */
	if (!err) strErr.Format(_T("WriteFile error: %d\n"), GetLastError());
	
	/* Make sure all the bytes were written */
	else if (sizeof(MyMessage) != numWritten) strErr.Format(_T(
		"WriteFile did not read the correct number of bytes!\n"));
		else 
		{			
		AfxMessageBox(MyMessage);
		bIsCorrect = true;		}
	if(!bIsCorrect)
		AfxMessageBox(strErr);

	//delete MyMessage;
	CloseHandle(handle);

}

void CNetSendTab::OnchbDisableSendTCVN() 
{
	// TODO: Add your control notification handler code here
	
}

/*
//REM for no longer use the netsend program
void CNetSendTab::CheckDisableBroadCast()
{
	if(m_currentSecond < GetNumSecondToCheck())
	{
		m_currentSecond++;
		UpdateCurrentSecondGUI();
		return;
	}

	m_currentSecond = 0;
	UpdateCurrentSecondGUI();

	if(IsDlgButtonChecked(chbDisableSendTCVN))	
	{
		HWND hWnd = ::FindWindow(NULL, _T("NetSend.NET version 2.1.1.3"));
		//FindWindowEx(null, null, _(""), _T(""));
		if(hWnd != NULL)
		{
			//MessageBox(_T("found!!"));
			HWND hWndChbSendToTCVN = ::FindWindowEx(hWnd, NULL, NULL, _T("Broadcast"));
			//HWND hWndChbSendToTCVN = ::FindWindowEx(hWnd, NULL, NULL, _T("My Group"));
			if(hWndChbSendToTCVN)
			{	
				
				//disable this check box
				if(::EnableWindow(hWndChbSendToTCVN, FALSE))
				{
				}
				//MessageBox(_T("disabled!!"));
				
				//set hide to this check box
				//::ShowWindow(hWndChbSendToTCVN, SW_HIDE);
				
			}
			else
			{
			}
		}
		else
		{
			//MessageBox(_T("not found!!"));
		}
	}
	else
	{
		
	}
}
*/

//change to disable the skype group chatting
//comment out for version 1.0.4.2
void CNetSendTab::CheckDisableBroadCast()
{
	if(m_currentSecond < GetNumSecondToCheck())
	{
		m_currentSecond++;
		UpdateCurrentSecondGUI();
		return;
	}

	m_currentSecond = 0;
	UpdateCurrentSecondGUI();

	if(IsDlgButtonChecked(chbDisableSendTCVN))	
	{
		//playboy

		MyGetSkypeGroupWindowPlayBoy();
		//HWND hWnd = ::FindWindow(NULL, groupName);
		
		if(m_hSkypeGroupRichTextBoxPlayBoy != NULL)
		{
			//MessageBox(_T("found rich text box window!!"));
			//HWND hWndChbSendToTCVN = ::FindWindowEx(hWnd, NULL, _T("TChatRichEdit"), NULL);
			//disable this check box
			if(::EnableWindow(m_hSkypeGroupRichTextBoxPlayBoy, FALSE))
			{
			}
			//MessageBox(_T("disabled!!"));
		}
		else
		{
			//MessageBox(_T("not found!!"));
		}

		//tc group		

		MyGetSkypeGroupWindowTCGroup();
		//HWND hWnd = ::FindWindow(NULL, groupName);
		
		if(m_hSkypeGroupRichTextBoxTCGroup != NULL)
		{
			//MessageBox(_T("found rich text box window!!"));
			//HWND hWndChbSendToTCVN = ::FindWindowEx(hWnd, NULL, _T("TChatRichEdit"), NULL);
			//disable this check box
			if(::EnableWindow(m_hSkypeGroupRichTextBoxTCGroup, FALSE))
			{
			}
			//MessageBox(_T("disabled!!"));
		}
		else
		{
			//MessageBox(_T("not found!!"));
		}
	}
	else
	{
		
	}
}

/*
void CDailyNoteDlg::ShowTotalApp()
{
	MyGetTotalApp();
	if(m_hTotalCommand)
	{
		//now find child window
		//CDailyNoteApp *pApp = (CDailyNoteApp*) AfxGetApp();
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Total App"), _T("Show!"), 2000);
		//SendMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(345,555));
		SetForegroundWindowInternal(m_hTotalCommand);		
		//EnumChildWindows(m_hMedia, ChildWndProcMediaNext,0);
	}
	else
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Total App"), _T("Not found!"), 2000);
}
*/
HWND CNetSendTab::MyGetSkypeGroupWindowTCGroup()
{	
	TCHAR		szBuff[100];

	if(m_hSkypeGroupTCGroup == NULL)
	{
		
		m_hSkypeGroupTCGroup = NULL;
		m_hSkypeGroupRichTextBoxTCGroup = NULL;
		if (!EnumWindows((WNDENUMPROC )EnumWindowsProcSkypeGroupTCGroup,0))
		{
			if(m_hSkypeGroupRichTextBoxTCGroup != NULL)
			{
			}
		}
	}
	else
	{		
		::GetClassName(m_hSkypeGroupTCGroup, szBuff, sizeof (szBuff) - 1);
		CString currentWnd(szBuff);
		if(currentWnd.Find(_T("PLAYBOY")) == -1)
		{
			m_hSkypeGroupTCGroup = NULL;
			m_hSkypeGroupRichTextBoxTCGroup = NULL;
			if (!EnumWindows((WNDENUMPROC )EnumWindowsProcSkypeGroupTCGroup,0))
			{
				if(m_hSkypeGroupRichTextBoxTCGroup != NULL)
				{
				}
			}
		}		
	}

	
	return m_hSkypeGroupTCGroup;
}




BOOL CALLBACK EnumWindowsProcSkypeGroupTCGroup(HWND hWnd, LPARAM lParam)
{
	TCHAR buff[500];
	int buffsize=500;
	//HWND hYahooWnd;
	//hYahooWnd=NULL;
		
	::GetWindowText(hWnd,buff,sizeof (buff) - 1);
	//::GetClassName(hWnd, buff, sizeof (buff) - 1);
	if (_tcslen(buff)<1)
		return TRUE;

	CString strTemp(buff);

	//CHECK for Yahoo MESSENGER CHAT WINDOW
	string::size_type pos=0;
	//int pos=0;
	//pos=strTemp.rfind("-- Instant Message",strTemp.length());
	//pos=strTemp.Find(_T("anhhuycan"));
	pos=strTemp.Find(_T("TC Group"));
	//pos=strTemp.rfind("nhutnhut",strTemp.length());

	
	//pos=strTemp.rfind(" @",strTemp.length());
	if (pos!=string::npos)
	//if (pos!=-1)
	{
		//char buf[20];
		//AfxMessageBox(_itoa((int) pos, buf, 10));		
		m_hSkypeGroupTCGroup = hWnd;		
		//m_countWindow = 0;
		EnumChildWindows(m_hSkypeGroupTCGroup,ChildWndProcSkypeGroupTCGroup,0);
		return FALSE;

	}
	return TRUE;
}

BOOL CALLBACK ChildWndProcSkypeGroupTCGroup(HWND hWnd, LPARAM lParam)
{
	/*
	TCHAR wndowclass[CLASS_SIZE];

	if (GetClassName(hwnd,wndowclass,CLASS_SIZE)==0)
		return TRUE;
		*/
	TCHAR buff[500];
	string::size_type pos=0;
	//::GetWindowText(hwnd,buff,buffsize);
	::GetClassName(hWnd, buff, sizeof (buff) - 1);	

//MYDEL	if (_tcslen(buff)<5)
//MYDEL		return TRUE;
	//m_strMessage = wndowclass;

	CString strTemp(buff);
	pos=strTemp.Find(_T("TChatRichEdit"));//TMyPanel
	//const TCHAR* myCharString = LPCTSTR(strTemp);
	//CString sSwitch("bbbb");
	if (pos!=string::npos)	
	{
		/*
		m_countWindow++;
		if(m_countWindow == 4)
		{
			m_hFunctions = hWnd;
			return FALSE;
		}
		*/
		m_hSkypeGroupRichTextBoxTCGroup = hWnd;
		return FALSE;
	}
	
	return TRUE;
}

HWND CNetSendTab::MyGetSkypeGroupWindowPlayBoy()
{	
	TCHAR		szBuff[100];

	if(m_hSkypeGroupPlayBoy == NULL)
	{
		
		m_hSkypeGroupPlayBoy = NULL;
		m_hSkypeGroupRichTextBoxPlayBoy = NULL;
		if (!EnumWindows((WNDENUMPROC )EnumWindowsProcSkypeGroupPlayBoy,0))
		{
			if(m_hSkypeGroupRichTextBoxPlayBoy != NULL)
			{
			}
		}
	}
	else
	{		
		::GetClassName(m_hSkypeGroupPlayBoy, szBuff, sizeof (szBuff) - 1);
		CString currentWnd(szBuff);
		if(currentWnd.Find(_T("PLAYBOY")) == -1)
		{
			m_hSkypeGroupPlayBoy = NULL;
			m_hSkypeGroupRichTextBoxPlayBoy = NULL;
			if (!EnumWindows((WNDENUMPROC )EnumWindowsProcSkypeGroupPlayBoy,0))
			{
				if(m_hSkypeGroupRichTextBoxPlayBoy != NULL)
				{
				}
			}
		}		
	}

	
	return m_hSkypeGroupPlayBoy;
}

BOOL CALLBACK EnumWindowsProcSkypeGroupPlayBoy(HWND hWnd, LPARAM lParam)
{
	TCHAR buff[500];
	int buffsize=500;
	//HWND hYahooWnd;
	//hYahooWnd=NULL;
		
	::GetWindowText(hWnd,buff,sizeof (buff) - 1);
	//::GetClassName(hWnd, buff, sizeof (buff) - 1);
	if (_tcslen(buff)<1)
		return TRUE;

	CString strTemp(buff);

	//CHECK for Yahoo MESSENGER CHAT WINDOW
	string::size_type pos=0;
	//int pos=0;
	//pos=strTemp.rfind("-- Instant Message",strTemp.length());
	//pos=strTemp.Find(_T("anhhuycan"));
	pos=strTemp.Find(_T("PLAYBOY"));
	//pos=strTemp.rfind("nhutnhut",strTemp.length());

	
	//pos=strTemp.rfind(" @",strTemp.length());
	if (pos!=string::npos)
	//if (pos!=-1)
	{
		//char buf[20];
		//AfxMessageBox(_itoa((int) pos, buf, 10));		
		m_hSkypeGroupPlayBoy = hWnd;		
		//m_countWindow = 0;
		EnumChildWindows(m_hSkypeGroupPlayBoy,ChildWndProcSkypeGroupPlayBoy,0);
		return FALSE;

	}
	return TRUE;
}

BOOL CALLBACK ChildWndProcSkypeGroupPlayBoy(HWND hWnd, LPARAM lParam)
{
	/*
	TCHAR wndowclass[CLASS_SIZE];

	if (GetClassName(hwnd,wndowclass,CLASS_SIZE)==0)
		return TRUE;
		*/
	TCHAR buff[500];
	string::size_type pos=0;
	//::GetWindowText(hwnd,buff,buffsize);
	::GetClassName(hWnd, buff, sizeof (buff) - 1);	

//MYDEL	if (_tcslen(buff)<5)
//MYDEL		return TRUE;
	//m_strMessage = wndowclass;

	CString strTemp(buff);
	pos=strTemp.Find(_T("TChatRichEdit"));//TMyPanel
	//const TCHAR* myCharString = LPCTSTR(strTemp);
	//CString sSwitch("bbbb");
	if (pos!=string::npos)	
	{
		/*
		m_countWindow++;
		if(m_countWindow == 4)
		{
			m_hFunctions = hWnd;
			return FALSE;
		}
		*/
		m_hSkypeGroupRichTextBoxPlayBoy = hWnd;
		return FALSE;
	}
	
	return TRUE;
}

int CNetSendTab::GetNumSecondToCheck()
{
	UpdateData();
	int iRs = 15;
	iRs = _ttoi(m_txtReCheckAfterValue);
//	int nFields = sscanf(m_txtReCheckAfterValue, _T("%d"), &iRs );
//	
//	if (nFields == 0 || nFields == EOF)
//	{
//		// Something went wrong if we get here
//		iRs = 15;
//	}
	return iRs;
}

void CNetSendTab::UpdateCurrentSecondGUI()
{
	CDailyNoteDlg* parentWnd = ((CDailyNoteDlg*)GetParent());
	if(parentWnd->IsWindowVisible())
	{
		CString strTemp;
		strTemp.Format(_T("%d"), m_currentSecond);
		m_lblCurrentSecond = strTemp;
		UpdateData(false);
	}
}
