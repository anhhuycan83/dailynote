#if !defined(AFX_OPTIONTAB_H__970394A0_01E0_4AAE_9DD4_4D91F35EC6CA__INCLUDED_)
#define AFX_OPTIONTAB_H__970394A0_01E0_4AAE_9DD4_4D91F35EC6CA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionTab.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptionTab dialog

class COptionTab : public CDialog
{
// Construction
public:
	void setConfigValue(bool ins_getSet);
	CString GetProgramDir();
	void createConfigFile(CString ins_FullPath);
	void loadConfigValue();
	CString GetFileStorePath();
	void SetFileStorePath(CString ins_fullPath);
	COptionTab(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptionTab)
	enum { IDD = IDD_DIALOG2 };
	CButton	m_chbFFDramaOnTop;
	CButton	m_chbIEDramaOnTop;
	CButton	m_chbNSFullMode;
	CButton	m_chbNSFlashWindow;
	CButton	m_rbF11Ctrl;
	CButton	m_rbF10Ctrl;
	CString	m_txtFileStore;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptionTab)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptionTab)
	virtual BOOL OnInitDialog();
	afx_msg void OnchbStartup();
	afx_msg void OnbtnSave();
	afx_msg void OnbtnDefault();
	afx_msg void OnrbF10();
	afx_msg void OnrbF11();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONTAB_H__970394A0_01E0_4AAE_9DD4_4D91F35EC6CA__INCLUDED_)
