; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=COptionTab
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "DailyNote.h"

ClassCount=11
Class1=CDailyNoteApp
Class2=CDailyNoteDlg
Class3=CAboutDlg

ResourceCount=11
Resource1=IDD_POPUP_RECORD_NOTE
Resource2=IDR_MAINFRAME
Class4=MyTabCtrl
Resource3=IDD_ABOUTBOX
Resource4=IDD_DIALOG3
Class5=CMAINTAB
Class6=COptionTab
Resource5=IDD_DAILYNOTE_DIALOG
Class7=CRecordNoteDlg
Resource6=IDD_DIALOG1
Resource7=IDD_DIALOG2
Class8=CNetSendTab
Resource8=IDD_DGLHISTORY
Class9=CTabMessage
Resource9=IDD_DGLPASSWORD
Class10=CHistory
Resource10=IDD_DGLMESSAGE
Class11=CDlgPassword
Resource11=IDR_ContextNoMenu

[CLS:CDailyNoteApp]
Type=0
HeaderFile=DailyNote.h
ImplementationFile=DailyNote.cpp
Filter=N

[CLS:CDailyNoteDlg]
Type=0
HeaderFile=DailyNoteDlg.h
ImplementationFile=DailyNoteDlg.cpp
Filter=W
LastObject=btnExit
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=DailyNoteDlg.h
ImplementationFile=DailyNoteDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_DAILYNOTE_DIALOG]
Type=1
Class=CDailyNoteDlg
ControlCount=3
Control1=IDC_TAB1,SysTabControl32,1342177280
Control2=btnHide,button,1342242816
Control3=btnExit,button,1342242816

[CLS:MyTabCtrl]
Type=0
HeaderFile=MyTabCtrl.h
ImplementationFile=MyTabCtrl.cpp
BaseClass=CTabCtrl
Filter=W
VirtualFilter=UWC
LastObject=MyTabCtrl

[DLG:IDD_DIALOG1]
Type=1
Class=CMAINTAB
ControlCount=4
Control1=IDC_LIST1,SysListView32,1350631436
Control2=btnAdd,button,1342242816
Control3=btnEdit,button,1342242816
Control4=btnDelete,button,1342242816

[DLG:IDD_DIALOG2]
Type=1
Class=COptionTab
ControlCount=14
Control1=IDC_STATIC,static,1342308352
Control2=chbStartup,button,1342242819
Control3=txtFileStore,edit,1350631552
Control4=btnSave,button,1342242816
Control5=btnDefault,button,1342242816
Control6=IDC_STATIC,static,1342308352
Control7=txtFileSound,edit,1350631552
Control8=IDC_STATIC,button,1342177287
Control9=rbF10,button,1342308361
Control10=rbF11,button,1342177289
Control11=chbNSFlashWindow,button,1342242819
Control12=chbNSFullMode,button,1342242819
Control13=chbIEDramaOnTop,button,1342242819
Control14=chbFFDramaOnTop,button,1342242819

[CLS:CMAINTAB]
Type=0
HeaderFile=MAINTAB.h
ImplementationFile=MAINTAB.cpp
BaseClass=CDialog
Filter=D
LastObject=btnAdd
VirtualFilter=dWC

[CLS:COptionTab]
Type=0
HeaderFile=OptionTab.h
ImplementationFile=OptionTab.cpp
BaseClass=CDialog
Filter=D
LastObject=chbIEDramaOnTop
VirtualFilter=dWC

[DLG:IDD_POPUP_RECORD_NOTE]
Type=1
Class=CRecordNoteDlg
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_DATETIMEPICKER1,SysDateTimePick32,1342242848
Control6=txtMsg,edit,1350631552
Control7=IDC_STATIC,button,1342177287
Control8=rbDaily,button,1342177289
Control9=rbWeekly,button,1342177289
Control10=rbMonthly,button,1342177289
Control11=rbOnce,button,1342177289

[CLS:CRecordNoteDlg]
Type=0
HeaderFile=RecordNoteDlg.h
ImplementationFile=RecordNoteDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CRecordNoteDlg

[MNU:IDR_ContextNoMenu]
Type=1
Class=?
Command1=pmMitExit
Command2=pmMitShow
CommandCount=2

[DLG:IDD_DIALOG3]
Type=1
Class=CNetSendTab
ControlCount=6
Control1=chbDisableSendTCVN,button,1342242819
Control2=txtReCheckAfter,edit,1350631552
Control3=IDC_STATIC,static,1342308352
Control4=btnSaveNetSend,button,1476460544
Control5=IDC_STATIC,static,1342308352
Control6=lblCurrentSecond,static,1342308352

[CLS:CNetSendTab]
Type=0
HeaderFile=NetSendTab.h
ImplementationFile=NetSendTab.cpp
BaseClass=CDialog
Filter=D
LastObject=btnSaveNetSend
VirtualFilter=dWC

[DLG:IDD_DGLMESSAGE]
Type=1
Class=CTabMessage
ControlCount=19
Control1=cbTo,combobox,1344348418
Control2=txtMsgSenTo,edit,1352732740
Control3=btnPre,button,1342242816
Control4=btnNext,button,1342242816
Control5=btnSendNS,button,1342242816
Control6=btnDeleteNS,button,1476395008
Control7=wndListNSend,SysListView32,1350565900
Control8=IDC_STATIC,static,1342308352
Control9=btnGet,button,1073741824
Control10=lblMsgCount,static,1342308352
Control11=txtMsgRecieve,edit,1352669252
Control12=btnSaveMsg,button,1073741824
Control13=chbForward75,button,1073741827
Control14=chbHuy,button,1073741827
Control15=chbForward60,button,1073741827
Control16=btnLoadMsgNS,button,1342177280
Control17=btnShowModeMsg,button,1342177280
Control18=btnHistory,button,1342177280
Control19=chbProtect,button,1342242819

[CLS:CTabMessage]
Type=0
HeaderFile=TabMessage.h
ImplementationFile=TabMessage.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=chbProtect

[DLG:IDD_DGLHISTORY]
Type=1
Class=CHistory
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=m_wndListHistoryView,SysListView32,1350631424
Control4=txtHistoryMsg,edit,1352728580

[CLS:CHistory]
Type=0
HeaderFile=History.h
ImplementationFile=History.cpp
BaseClass=CDialog
Filter=D
LastObject=m_wndListHistoryView
VirtualFilter=dWC

[DLG:IDD_DGLPASSWORD]
Type=1
Class=CDlgPassword
ControlCount=3
Control1=txtPassword,edit,1350631584
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816

[CLS:CDlgPassword]
Type=0
HeaderFile=DlgPassword.h
ImplementationFile=DlgPassword.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDlgPassword

