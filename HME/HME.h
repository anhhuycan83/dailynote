// HME.h : main header file for the HME DLL
//

#if !defined(AFX_HME_H__D30F7C5B_BBAF_46EE_B8E3_1E3A18B4CF48__INCLUDED_)
#define AFX_HME_H__D30F7C5B_BBAF_46EE_B8E3_1E3A18B4CF48__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
#define DLLEXPORT __declspec(dllexport)
#define DLLIMPORT __declspec(dllimport)
// CHMEApp
// See HME.cpp for the implementation of this class
//

//globle function
bool IsWindowsVistaOrLater();
bool m_bIsWindowsVistaOrLater;

class CHMEApp : public CWinApp
{
public:
	CHMEApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHMEApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CHMEApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HME_H__D30F7C5B_BBAF_46EE_B8E3_1E3A18B4CF48__INCLUDED_)
