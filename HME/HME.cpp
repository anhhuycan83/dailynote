// HME.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "HME.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CHMEApp

BEGIN_MESSAGE_MAP(CHMEApp, CWinApp)
	//{{AFX_MSG_MAP(CHMEApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHMEApp construction

CHMEApp::CHMEApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	//IsWindowsVistaOrLater();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CHMEApp object

CHMEApp theApp;


//my shared data
#pragma data_seg("SHARED_DATA")
HHOOK hGlobalKeyBoardHook = NULL;
bool gb_stop = false;
HWND hWndRecMsg = NULL;
//HHOOK hGlobalMouseHook = NULL;
#pragma data_seg()


extern "C" DLLEXPORT LRESULT CALLBACK myKeyBoardHook(int nCode, WPARAM wParam, LPARAM lParam)
{
	if ((nCode == HC_ACTION) && (wParam > 0))
	{	
		WORD nVirtKey;
		//nVirtKey = GetKeyState(VK_LCONTROL);				
		
		switch(wParam)
		{
			case  0x32: // 2 key - for IE DRAMAS
			//CHECK if the user press F2 so break;
			nVirtKey = GetKeyState(0x32);				
			if (nVirtKey & 0x8000) //key press down
			{
				nVirtKey = GetKeyState(VK_LMENU);				
				if (nVirtKey & 0x8000) //key press down
				{
					::SendMessage(hWndRecMsg, MYWM_IE_DRAMAS, 0, 0);
					return 1; //for loop//					
				}				
			}
			break;
			/*
		case  VK_F2:// FIX FOR TOTAL COMMAND FOR TUYEN PC
			//CHECK if the user press F2 so break;
			nVirtKey = GetKeyState(VK_F2);
			if (nVirtKey & 0x8000) //key press down
			{
				::SendMessage(hWndRecMsg, MYWM_F2_TUYEN, 0, 0);
				return 1; //for loop//					
			}
			break;
			*/
			/*
		case  0x53:// Alt + Ctrl + S for resize remote desktop
			nVirtKey = GetKeyState(0x53);
			if (nVirtKey & 0x8000) //key press down
			{
				//check the ship key VK_SHIFT, VK_LSHIFT
				nVirtKey = GetKeyState(VK_LMENU);
				if (nVirtKey & 0x8000) //key press down
				{
					nVirtKey = GetKeyState(VK_LCONTROL);
					if (nVirtKey & 0x8000) //key press down
					{
						::SendMessage(hWndRecMsg, WM_USER + 12, 0, 0);
						return 1; //for loop//					
					}
				}
			}
			break;
			*/
		case  VK_OEM_3:// '~' character for show up the totol commander app			
			nVirtKey = GetKeyState(VK_OEM_3);
			if (nVirtKey & 0x8000) //key press down
			{
				//check the ship key VK_SHIFT, VK_LSHIFT
				nVirtKey = GetKeyState(VK_LMENU);
				if (nVirtKey & 0x8000) //key press down
				{
					::SendMessage(hWndRecMsg, WM_USER + 11, 0, 0);
					return 1; //for loop//					
				}
			}
			break;
			/*
		case  VK_SUBTRACT:// subtract numpad button for next button in WMP
			//CHECK if the user press F2 so break;
			nVirtKey = GetKeyState(VK_SUBTRACT);				
			if (nVirtKey & 0x8000) //key press down
			{
				::SendMessage(hWndRecMsg, WM_USER + 10, 0, 0);
				return 1; //for loop//					
			}
			break;
			*/
			
			//0x57 character: W (if win 7 => next)
		case  0x57:// subtract numpad button for next button in WMP
			//CHECK if the user press F2 so break;
			nVirtKey = GetKeyState(0x57);				
			if (nVirtKey & 0x8000) //key press down
			{
				nVirtKey = GetKeyState(VK_LMENU);
				if (nVirtKey & 0x8000) //key press down
				{
					::SendMessage(hWndRecMsg, WM_USER + 10, 0, 0);
					return 1; //for loop//					
				}
			}
			break;
			
			/*
		case  VK_ADD://VK_RETURN: // enter numpad key for play or pause media player
			
			nVirtKey = GetKeyState(VK_ADD);				
			if (nVirtKey & 0x8000) //key press down
			{
				::SendMessage(hWndRecMsg, WM_USER + 9, 0, 0);
				return 1; //for loop
				//					//now check if vk-enter is from numpad
				//					if(lParam & 0x01000000)
				//					{
				//						::SendMessage(hWndRecMsg, WM_USER + 9, 0, 0);
				//						return 1; //for loop
				//					}					
			}
			break;
			/*
			//0x51 character: Q (if win 7 => play/pause)
		case  0x51:// subtract numpad button for next button in WMP
			//CHECK if the user press F2 so break;
			nVirtKey = GetKeyState(0x51);				
			if (nVirtKey & 0x8000) //key press down
			{
				nVirtKey = GetKeyState(VK_LMENU);
				if (nVirtKey & 0x8000) //key press down
				{
					::SendMessage(hWndRecMsg, WM_USER + 9, 0, 0);
					return 1; //for loop//					
				}
			}
			break;
			
		case  0x31: // 1 key - for mute sound
			//CHECK if the user press F2 so break;
			nVirtKey = GetKeyState(0x31);				
			if (nVirtKey & 0x8000) //key press down
			{
				nVirtKey = GetKeyState(VK_LMENU);				
				if (nVirtKey & 0x8000) //key press down
				{
					::SendMessage(hWndRecMsg, WM_USER + 7, 0, 0);
					return 1; //for loop//					
				}				
			}
			break;
			/*
		case  VK_F10: // F10 - break
			//CHECK if the user press F10 so break;
			nVirtKey = GetKeyState(VK_F10);				
			if (nVirtKey & 0x8000) //key press down
			{
				//CPrcDlg::gb_Stop = true;
				gb_stop = true;
				//CPrcDlg::setUnHookKeyBoard();
				::SendMessage(hWndRecMsg, WM_USER + 3, 0, 0);
				//					AfxMessageBox(_T("f10 down"));
				//setUnHookKeyBoard();
				return 1; //for loop
			}
			break;
		case  VK_F11: // F10 - break
			//CHECK if the user press F10 so break;
			nVirtKey = GetKeyState(VK_F11);				
			if (nVirtKey & 0x8000) //key press down
			{
				//CPrcDlg::gb_Stop = true;
				gb_stop = true;
				//CPrcDlg::setUnHookKeyBoard();
				::SendMessage(hWndRecMsg, WM_USER + 4, 0, 0);
				//					AfxMessageBox(_T("f10 down"));
				//setUnHookKeyBoard();
				return 1; //for loop
			}
			break;
			*/
		case  VK_OEM_PLUS: // =+ key - for show dailynote dialog
			//CHECK if the user press F2 so break;
			nVirtKey = GetKeyState(VK_OEM_PLUS);				
			if (nVirtKey & 0x8000) //key press down
			{
				nVirtKey = GetKeyState(VK_LMENU);				
				if (nVirtKey & 0x8000) //key press down
				{
					::SendMessage(hWndRecMsg, WM_USER + 3, 0, 0);
					::SendMessage(hWndRecMsg, WM_USER + 4, 0, 0);
					return 1; //for loop//					
				}				
			}
			break;
		case  VK_F12: // F12 - break
			//CHECK if the user press F10 so break;
			nVirtKey = GetKeyState(VK_F12);				
			if (nVirtKey & 0x8000) //key press down
			{
				nVirtKey = GetKeyState(VK_LMENU);				
				if (nVirtKey & 0x8000) //key press down
				{
					::SendMessage(hWndRecMsg, WM_USER + 17, 0, 0);
					return 1; //for loop//					
				}
			}
			break;
		case  VK_F11: // F11 - break
			//CHECK if the user press F10 so break;
			nVirtKey = GetKeyState(VK_F11);				
			if (nVirtKey & 0x8000) //key press down
			{
				nVirtKey = GetKeyState(VK_LMENU);				
				if (nVirtKey & 0x8000) //key press down
				{
					::SendMessage(hWndRecMsg, WM_USER + 18, 0, 0);
					return 1; //for loop//					
				}
			}
			break;
		default:
			break;
		}
	}
	return CallNextHookEx(hGlobalKeyBoardHook, nCode, wParam, lParam);
}

bool IsWindowsVistaOrLater()
{
	OSVERSIONINFO osvi;
    //bool bIsWindowsVistaOrLater;
	
    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	
    GetVersionEx(&osvi);
	
    //bIsWindowsVistaOrLater = (osvi.dwMajorVersion == 6) && (osvi.dwMinorVersion >= 0));
	m_bIsWindowsVistaOrLater = (osvi.dwMajorVersion > 5);
	
    return m_bIsWindowsVistaOrLater;
	
}

extern "C" DLLEXPORT bool setHKeyBoardHook(HHOOK ins_hhook)
{	
	/*
	CHMEApp *pApp = (CHMEApp *) AfxGetApp();
	pApp->m_hhook = ins_hhook;
	*/
	
	hGlobalKeyBoardHook = ins_hhook;
/*
	CString msg;
	msg.Format("dll file: key pressed %f", hGlobalKeyBoardHook);

	AfxMessageBox(msg, 1, MB_OK);
*/	

	return true;
}

extern "C" DLLEXPORT bool isStop()
{	
	return gb_stop;
}

extern "C" DLLEXPORT void setStopVar(bool ins_bValue)
{	
	gb_stop = ins_bValue;
}

extern "C" DLLEXPORT void setHWNDRecMsg(HWND ins_hWndRecMsg)
{
	hWndRecMsg = ins_hWndRecMsg;
}