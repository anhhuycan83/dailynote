#if !defined(AFX_NETSENDTAB_H__AE6FFBC7_1A6B_4196_B3FE_112996187C43__INCLUDED_)
#define AFX_NETSENDTAB_H__AE6FFBC7_1A6B_4196_B3FE_112996187C43__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NetSendTab.h : header file
//

static BOOL CALLBACK EnumWindowsProcSkypeGroupPlayBoy(HWND hwnd,LPARAM lParam);
static BOOL CALLBACK ChildWndProcSkypeGroupPlayBoy(HWND hwnd,LPARAM lParam);
static HWND m_hSkypeGroupPlayBoy;
static HWND m_hSkypeGroupRichTextBoxPlayBoy;

static BOOL CALLBACK EnumWindowsProcSkypeGroupTCGroup(HWND hwnd,LPARAM lParam);
static BOOL CALLBACK ChildWndProcSkypeGroupTCGroup(HWND hwnd,LPARAM lParam);
static HWND m_hSkypeGroupTCGroup;
static HWND m_hSkypeGroupRichTextBoxTCGroup;




/////////////////////////////////////////////////////////////////////////////
// CNetSendTab dialog

class CNetSendTab : public CDialog
{
// Construction
public:
	int m_currentSecond;
	int GetNumSecondToCheck();
	void CheckDisableBroadCast();
	CNetSendTab(CWnd* pParent = NULL);   // standard constructor
	HWND MyGetSkypeGroupWindowPlayBoy();
	HWND MyGetSkypeGroupWindowTCGroup();

// Dialog Data
	//{{AFX_DATA(CNetSendTab)
	enum { IDD = IDD_DIALOG3 };
	CEdit	m_txtReCheckAfterCtrl;
	CButton	m_chbDisableSendTCVNCtrl;
	BOOL	m_chbDisableSendTCVNValue;
	CString	m_txtReCheckAfterValue;
	CString	m_lblCurrentSecond;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetSendTab)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void setConfigValue(bool);
	// Generated message map functions
	//{{AFX_MSG(CNetSendTab)
	virtual BOOL OnInitDialog();
	afx_msg void OnbtnSaveNetSend();
	afx_msg void OnchbDisableSendTCVN();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void UpdateCurrentSecondGUI();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETSENDTAB_H__AE6FFBC7_1A6B_4196_B3FE_112996187C43__INCLUDED_)
