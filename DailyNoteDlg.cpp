// DailyNoteDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DailyNote.h"
#include "DailyNoteDlg.h"
#include "MAINTAB.h"
#include "NetSendTab.h"
#include "OptionTab.h"
#include "Shlwapi.h"
//#include "XWinVer.h"

#include "DateTimeFormat.h"
//#include "TabMessage.h"
#include "FileVersionInfo.h"


#include <windows.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
CRecordNote::CRecordNote(CString strLine)
{
	//m_strTime = strTime;
	//m_strMessage = strMessage;
	m_line = strLine;
}

// CDailyNoteDlg dialog

CDailyNoteDlg::CDailyNoteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDailyNoteDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDailyNoteDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hIcon = AfxGetApp()->LoadIcon(IDI_Duckling);

	m_visible = false;// when first run is move to tray icon
	m_pTimerThread = 0;
	m_pTimerThreadML = 0;
	m_pTimerThreadDramas = 0;
	m_pMessageBoxThread = 0;
/*
	oldKeyBoardHook = 0;
	hmeDll = 0;
	pSetHHook = 0;
	pGetVK = 0;
	hUpdateRecordDll = 0;
	pSetHwnReceiveMsg = 0;
	*/
	//for mute volumn
	m_bIsMute = false;
	dwVolume = 0;
	hHelper = NULL;
	IsWindowsVistaOrLater();
	m_bIsMediaPlaying = true;
	m_hMedia = NULL;
	m_hTotalCommand = NULL;
	m_hFunctions = NULL;
	m_bIsOnTopDramas = FALSE;

	m_hIEDramas = NULL;
	m_hFFDramas = NULL;

	m_hBtnVideoMonitorClient = NULL;
	m_hBtnVideoMonitorClientRecordMam1 = NULL;
}


void CDailyNoteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDailyNoteDlg)
	DDX_Control(pDX, IDC_TAB1, m_tbCtrl);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDailyNoteDlg, CDialog)
	//{{AFX_MSG_MAP(CDailyNoteDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(btnHide, OnbtnHide)
	ON_WM_WINDOWPOSCHANGING()
	ON_BN_CLICKED(btnExit, OnbtnExit)
	ON_COMMAND(pmMitExit, OnpmMitExit)
	ON_COMMAND(pmMitShow, OnpmMitShow)
	ON_MESSAGE(MYWM_CHECKTIME, OnCheckTime)
	ON_MESSAGE(MYWM_DISABLEBROADCAST, OnDisableBroadCast)
	ON_MESSAGE(MYWM_READMAILSLOT, OnReadMailSlot)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDailyNoteDlg message handlers

BOOL CDailyNoteDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	//adding file version to the title
	
	CFileVersionInfo fvi;
	if(fvi.Create())
	{
		CString sCurrentTitle, sNewTitle, sVersion;
		//CString t;
		TRACE(_T("version: %s"), fvi.GetFileVersion());
		AfxGetApp()->m_pMainWnd->GetWindowText(sCurrentTitle);
		sVersion.Format(_T("%d.%d.%d.%d")
			, fvi.GetFileVersion(3), fvi.GetFileVersion(2), fvi.GetFileVersion(1), fvi.GetFileVersion(0));
		sNewTitle.Format(_T("%s v%s"), sCurrentTitle, sVersion);
		AfxGetApp()->m_pMainWnd->SetWindowText(sNewTitle);
		AfxGetApp()->m_pMainWnd->UpdateData();
	}
	//

	m_tbCtrl.InitDialogs();

    m_tbCtrl.InsertItem(0,_T("List Notes"));
    m_tbCtrl.InsertItem(1,_T("OpTion"));
	m_tbCtrl.InsertItem(2,_T("DisSend"));
	m_tbCtrl.InsertItem(3,_T("NetSend"));

    m_tbCtrl.ActivateTabDialogs();
		//check if the mailslot was not created => exit program
	CTabMessage *pDlgNetSend = (CTabMessage *)(m_tbCtrl.m_Dialog[3]);
	if(pDlgNetSend->m_hServer == INVALID_HANDLE_VALUE)
	{
		::MessageBox(NULL,
								_T("Can not create mailslot. The program will exit now!"),
								_T("Alert"),
								MB_ICONINFORMATION | MB_OK | MB_SETFOREGROUND | MB_TOPMOST);
		PostQuitMessage(0);
	}
	//save this object
	m_pTabMessage = pDlgNetSend;
	//load config of flash on netsend dialog
	m_pTabMessage->LoadSettingFlashWindow();

	//set full path for filestore
	//m_fileStore = _T("C:\\myRecordNotes");
	MyUseDLL();
	MyGetArrRecordNote();
	MyRefreshListReport(m_RecordNoteArray);
	
	//set hook
	setHookKeyBoard();
//
	m_myStatus = MYSTOP;
	m_myStatusMailSlot = MYSTOP;
	m_myStatusDramas = MYSTOP;

	m_isHaveCurrentTimeCheking = false;
	m_currentIndexWaitingToShow = -1;
	MyStartTimer();
	MyStartThreadMailSlot();
	MyStartThreadDramas();

	//
//MYDEL	CXWinVersion ver;
//MYDEL	if(ver.IsXP())
//MYDEL		FixShowTrayIcon();
	myShowDlg(false);
	TrayMessage(NIM_ADD);
	//TrayMessage(NIM_DELETE);
	//OnbtnHide();
	//TrayMessage(NIM_ADD);
	

	//set font of CEdit textbox
	//setRtbFont(_T("Tahoma"));


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDailyNoteDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDailyNoteDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDailyNoteDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

BOOL CDailyNoteDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
			pMsg->wParam=NULL ;
	} 
	return CDialog::PreTranslateMessage(pMsg);
}

void CDailyNoteDlg::MyGetArrRecordNote()
{
	HMODULE hModuleDataRecordNote; //hmodule
	hModuleDataRecordNote = GetModuleHandle(_T("DataRecordNote"));
	if(hModuleDataRecordNote == NULL)
	{
		return;
	}

	MYPROC pGetArrRecordNote;
	//MYMOUSEPROC pSetMouseHHook;

	pGetArrRecordNote = (MYPROC) GetProcAddress(hModuleDataRecordNote, "GetListRecordNote");
	//test pSetHHook if nessesary
	CString filePath = AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt"));

	CFileFind finder;
	bool bCanAccess;
	//CString folderPath = finder.GetFilePath(filePath);
	if(!IsFileExist(filePath, bCanAccess))
	{
		filePath=_T("C:\\myRecordNote.txt");
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("FILESOTER"), filePath);
		//reload setting file
		COptionTab *pDlgOption = (COptionTab*)(m_tbCtrl.m_Dialog[1]);
		pDlgOption->setConfigValue(false);
	}
	
	if(!finder.FindFile(filePath))
	{
		CTextFileWrite myfile(filePath, 
		CTextFileWrite::UTF_8);
		myfile.Close();
	}
	//MessageBox(filePath);
	MyClearMem();
	(pGetArrRecordNote)(m_RecordNoteArray, filePath);	
}

void CDailyNoteDlg::MyUseDLL()
{
	hUpdateRecordDll = LoadLibrary(_T("DataRecordNote.dll"));//(LPCTSTR) 
	if(hUpdateRecordDll == NULL)
	{
		MessageBox(_T("Can't load DataRecordNote.dll"));
		return;
	}
}

void CDailyNoteDlg::MyUnUseDLL()
{
	if(hUpdateRecordDll)
	{
		FreeLibrary(hUpdateRecordDll);
		hUpdateRecordDll = 0;
		//FreeLibrary(hMMsgDLL);
	}
}

void CDailyNoteDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	MyClearMem();
	// TODO: Add your message handler code here
	MyUnUseDLL();
	//comment out for version 1.0.2.4
//MYDEL	if(m_visible == false) // this program is in tray area
//MYDEL	{
//MYDEL		TrayMessage(NIM_DELETE);
//MYDEL	}
	TrayMessage(NIM_DELETE);
	setUnHookKeyBoard();
	
//MYDEL	CTabMessage *pDlgNetSend = (CTabMessage *)(m_tbCtrl.m_Dialog[3]);
//MYDEL	pDlgNetSend->SaveMsgDefault();

	//delete font variable
	//When you no longer need the font, call the DeleteObject function to delete the font.
	//especially if you have a sub dialog
	m_font.DeleteObject();
}

void CDailyNoteDlg::MyRefreshListReport(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote)
{
//	CStringArray arrStr;
//
//	CMAINTAB *pDlgMain = (CMAINTAB *)(m_tbCtrl.m_Dialog[0]);
//	//pDlgMain->m_wndList.ins
//	//pDlgMain->m_wndList.SetColumnHeader(_T("STT, 50; Date time, 200; Frequency, 50; Message, 250, 2"));
//	pDlgMain->m_wndList.SetColumnHeader(_T("STT, 30; Date time, 170; Frequency, 80; Message, 240"));
//
//	CRecordNote *pRectNote;
//	TCHAR temp[5];
//	pDlgMain->m_wndList.SetSortable(false);
//	int i;
//	for (i = 0; i < arrayRecordNote.GetSize(); i++)
//	{
//		pRectNote = arrayRecordNote.GetAt(i);
//		// arrStr
//		StringSplit(pRectNote->m_line, arrStr, _T(';'));
//		const int IDX = pDlgMain->m_wndList.InsertItem(i, _T(""));
//		pDlgMain->m_wndList.SetItemText(IDX, 0, _itow(i, temp, 10));
//		pDlgMain->m_wndList.SetItemText(IDX, 1, arrStr.GetAt(0));
//		pDlgMain->m_wndList.SetItemText(IDX, 2, arrStr.GetAt(1));
//		pDlgMain->m_wndList.SetItemText(IDX, 3, arrStr.GetAt(2));
//	}	
		
	//pDlgMain->m_wndList.SortItems(0, TRUE);
	CMAINTAB *pDlgMain = (CMAINTAB *)(m_tbCtrl.m_Dialog[0]);
	pDlgMain->MyRefreshListReport(arrayRecordNote);
}

void CDailyNoteDlg::MyClearMem()
{
	for(int i = 0; i < m_RecordNoteArray.GetSize(); i++)
	{
		//CRecordNote *pRn = (CRecordNote *)m_RecordNoteArray.GetAt(i);
		delete m_RecordNoteArray.GetAt(i);
		//delete pRn;
	}
	m_RecordNoteArray.RemoveAll();

	//remove thread
	//delete m_pTimerThread;
	DWORD exitCode;
	::GetExitCodeThread(m_pTimerThread, &exitCode);
	if(exitCode == STILL_ACTIVE)
	{
		//m_pTimerThread->end
		m_myStatus = MYSTOP;
	}
	
	if(m_pTimerThread)
		delete m_pTimerThread;

	::GetExitCodeThread(m_pTimerThreadML, &exitCode);
	if(exitCode == STILL_ACTIVE)
	{
		//m_pTimerThread->end
		m_myStatusMailSlot = MYSTOP;
	}
	
	if(m_pTimerThreadML)
		delete m_pTimerThreadML;

	::GetExitCodeThread(m_pTimerThreadDramas, &exitCode);
	if(exitCode == STILL_ACTIVE)
	{
		//m_pTimerThread->end
		m_myStatusDramas = MYSTOP;
	}
	
	if(m_pTimerThreadDramas)
		delete m_pTimerThreadDramas;

	if(m_pMessageBoxThread)
		delete m_pMessageBoxThread;
}

void CDailyNoteDlg::MyAddItemDLL(CString strLineAdd)
{
	HMODULE hModuleDataRecordNote; //hmodule
	hModuleDataRecordNote = GetModuleHandle(_T("DataRecordNote"));
	if(hModuleDataRecordNote == NULL)
	{
		return;
	}

	ADDPROC pAddRecordNote;
	//MYMOUSEPROC pSetMouseHHook;

	pAddRecordNote = (ADDPROC) GetProcAddress(hModuleDataRecordNote, "AddRecordNote");
	//test pSetHHook if nessesary
	CString filePath = AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt"));

	CFileFind finder;	
	
	if(!finder.FindFile(filePath))
	{
		CTextFileWrite myfile(filePath, 
		CTextFileWrite::UTF_8);
		myfile.Close();
	}
	//MessageBox(filePath);
	//MyClearMem();
	//(pGetArrRecordNote)(m_RecordNoteArray, filePath);	
	(pAddRecordNote)(m_RecordNoteArray, filePath, strLineAdd);
}

void CDailyNoteDlg::MyDeleteItemDLL(int index)
{
	HMODULE hModuleDataRecordNote; //hmodule
	hModuleDataRecordNote = GetModuleHandle(_T("DataRecordNote"));
	if(hModuleDataRecordNote == NULL)
	{
		return;
	}

	DELPROC pDelRecordNote;
	//MYMOUSEPROC pSetMouseHHook;

	pDelRecordNote = (DELPROC) GetProcAddress(hModuleDataRecordNote, "DelRecordNote");
	//test pSetHHook if nessesary
	CString filePath = AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt"));

	CFileFind finder;	
	
	if(!finder.FindFile(filePath))
	{
		CTextFileWrite myfile(filePath, 
		CTextFileWrite::UTF_8);
		myfile.Close();
	}
	//MessageBox(filePath);
	//MyClearMem();
	//(pGetArrRecordNote)(m_RecordNoteArray, filePath);	
	(pDelRecordNote)(m_RecordNoteArray, filePath, index);

}

void CDailyNoteDlg::StringSplit(const CString &str, CStringArray &arr, TCHAR chDelimitior)
{
	int nStart = 0, nEnd = 0;
	arr.RemoveAll();

	while (nEnd < str.GetLength())
	{
		// determine the paragraph ("xxx,xxx,xxx;")
		nEnd = str.Find(chDelimitior, nStart);
		if( nEnd == -1 )
		{
			// reached the end of string
			nEnd = str.GetLength();
		}

		CString s = str.Mid(nStart, nEnd - nStart);
		if (!s.IsEmpty())
			arr.Add(s);

		nStart = nEnd + 1;
	}
}

void CDailyNoteDlg::myShowDlg(bool ins_isShow)
{
	if(ins_isShow == true)
	{
		m_visible = true;
		//TrayMessage(NIM_DELETE); //comment out for version 1.0.2.4
		ShowWindow(SW_RESTORE);//SW_NORMAL  SW_HIDE  
		SetForegroundWindowInternal(this->m_hWnd);
		//ShowWindow(SW_SHOW);
		//ShowWindow(SW_NORMAL);
		//move from tray area to desktop
	}
	else
	{
		m_visible = false;
		//TrayMessage(NIM_ADD);//comment out for version 1.0.2.4
		//myResetSystrayIconStatus(isRunning);
		ShowWindow(SW_HIDE);//SW_NORMAL
		//move from desktop to tray area
	}
}

int CDailyNoteDlg::TrayMessage(DWORD dwMessage)
{
	CString sTip(_T("Note Message"));
	//sTip += MYVERSION;
	
    NOTIFYICONDATA tnd;
	
    tnd.cbSize = sizeof(NOTIFYICONDATA);	
	
    tnd.hWnd = m_hWnd;
	tnd.uID = ICON_VALUE1;
    //tnd.uID = IDR_MAINFRAME;
	if(m_bIsMute)
	{
		
		m_hIconSystemTray = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDI_DucklingMute));
		
		//tnd.uID = ICON_VALUE1;
	}
	else
	{
		m_hIconSystemTray = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDI_Duckling));
		//tnd.uID = IDI_Duckling;
	}
	
    //tnd.uFlags = NIF_MESSAGE|NIF_ICON;
    tnd.uCallbackMessage = MYWM_NOTIFYICON;
    tnd.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP; 
    //VERIFY( tnd.hIcon = LoadIcon(AfxGetInstanceHandle(), 
	//	MAKEINTRESOURCE (tnd.uID)) );
	tnd.hIcon = m_hIconSystemTray;

    lstrcpyn(tnd.szTip, (LPCTSTR)sTip, sizeof(tnd.szTip));

	Shell_NotifyIcon(dwMessage, &tnd);
	//DestroyIcon(
    return 0;
}

int CDailyNoteDlg::TrayMessage(DWORD dwMessage, CString sTip)//set the tray icon with strMsg input
{	
	sTip.Format(_T("%s\n%s"), sTip, _T(MYVERSION)); 
	//sTip += _T("\n");
	 //sTip += _T(MYVERSION);

    NOTIFYICONDATA tnd;

    tnd.cbSize = sizeof(NOTIFYICONDATA);	

    tnd.hWnd = m_hWnd;
    ////tnd.uID = IDR_MAINFRAME;
	tnd.uID = ICON_VALUE1;
//	if(m_bIsMute)
//		tnd.uID = IDI_DucklingMute;
//	else
//		tnd.uID = IDI_Duckling;
	if(m_bIsMute)
	{
		
		m_hIconSystemTray = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDI_DucklingMute));
		
		//tnd.uID = ICON_VALUE1;
	}
	else
	{
		m_hIconSystemTray = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDI_Duckling));
		//tnd.uID = IDI_Duckling;
	}
	
    ////tnd.uFlags = NIF_MESSAGE|NIF_ICON;
    tnd.uCallbackMessage = MYWM_NOTIFYICON;
    tnd.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP; 
//    VERIFY( tnd.hIcon = LoadIcon(AfxGetInstanceHandle(), 
//		MAKEINTRESOURCE (tnd.uID)) );
	tnd.hIcon = m_hIconSystemTray;
    lstrcpyn(tnd.szTip, (LPCTSTR)sTip, sizeof(tnd.szTip));

    return Shell_NotifyIcon(dwMessage, &tnd);
}

int CDailyNoteDlg::TrayMessageShowBalloon(DWORD dwMessage, CString sTip, CString sTitle, CString sMsg, int timeout)//set the tray icon with strMsg input
{	
	//show balloon
	//sTip.Format(_T("%s\n%s"), sTip, _T(MYVERSION)); 
	//sTip += _T("\n");
	 //sTip += _T(MYVERSION);
	//NIM_MODIFY

    NOTIFYICONDATA tnd;

    tnd.cbSize = sizeof(NOTIFYICONDATA);	

    tnd.hWnd = m_hWnd;
    ////tnd.uID = IDR_MAINFRAME;
	tnd.uID = ICON_VALUE1;
//	if(m_bIsMute)
//		tnd.uID = IDI_DucklingMute;
//	else
//		tnd.uID = IDI_Duckling;
	if(m_bIsMute)
	{
		
		m_hIconSystemTray = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDI_DucklingMute));
		
		//tnd.uID = ICON_VALUE1;
	}
	else
	{
		m_hIconSystemTray = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDI_Duckling));
		//tnd.uID = IDI_Duckling;
	}
	
    ////tnd.uFlags = NIF_MESSAGE|NIF_ICON;
    //tnd.uCallbackMessage = MYWM_NOTIFYICON;
    tnd.uFlags = NIF_INFO; 
//    VERIFY( tnd.hIcon = LoadIcon(AfxGetInstanceHandle(), 
//		MAKEINTRESOURCE (tnd.uID)) );
	tnd.hIcon = m_hIconSystemTray;
    lstrcpyn(tnd.szInfo, (LPCTSTR)sMsg, sizeof(tnd.szInfo));
	lstrcpyn(tnd.szInfoTitle, (LPCTSTR)sTitle, sizeof(tnd.szInfoTitle));
	tnd.uTimeout = timeout;
	tnd.dwInfoFlags = NIF_INFO;

    Shell_NotifyIcon(NIM_MODIFY, &tnd);

	//rollback msg
	sTip.Format(_T("%s\n%s"), sTip, _T(MYVERSION)); 
	//sTip += _T("\n");
	 //sTip += _T(MYVERSION);

    NOTIFYICONDATA tnd2;

    tnd2.cbSize = sizeof(NOTIFYICONDATA);	

    tnd2.hWnd = m_hWnd;
    ////tnd.uID = IDR_MAINFRAME;
	tnd2.uID = ICON_VALUE1;
//	if(m_bIsMute)
//		tnd.uID = IDI_DucklingMute;
//	else
//		tnd.uID = IDI_Duckling;
	if(m_bIsMute)
	{
		
		m_hIconSystemTray = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDI_DucklingMute));
		
		//tnd.uID = ICON_VALUE1;
	}
	else
	{
		m_hIconSystemTray = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDI_Duckling));
		//tnd.uID = IDI_Duckling;
	}
	
    ////tnd.uFlags = NIF_MESSAGE|NIF_ICON;
    tnd2.uCallbackMessage = MYWM_NOTIFYICON;
    tnd2.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP; 
//    VERIFY( tnd.hIcon = LoadIcon(AfxGetInstanceHandle(), 
//		MAKEINTRESOURCE (tnd.uID)) );
	tnd2.hIcon = m_hIconSystemTray;
    lstrcpyn(tnd2.szTip, (LPCTSTR)sTip, sizeof(tnd.szTip));

    return Shell_NotifyIcon(dwMessage, &tnd2);
}


void CDailyNoteDlg::OnbtnHide()
{
	// TODO: Add your control notification handler code here
	myShowDlg(false);
	if(m_isHaveCurrentTimeCheking)
	{	
		if(!m_visible)
			TrayMessage(NIM_MODIFY, m_strMsgNotify);
	}
}

void CDailyNoteDlg::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos) 
{
	//my code begin
	if(m_visible == false)
	{
		lpwndpos->flags &= ~SWP_SHOWWINDOW;
	}
	//my code end
	CDialog::OnWindowPosChanging(lpwndpos);
	
	// TODO: Add your message handler code here
	
}

void CDailyNoteDlg::OnbtnExit() 
{
	// TODO: Add your control notification handler code here
	EndDialog(0);
}

LRESULT CDailyNoteDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	// Open window when double click to the Systray Icon
    if(message == MYWM_NOTIFYICON){ 
        switch (lParam){
			case WM_LBUTTONDOWN: 
            case WM_LBUTTONDBLCLK: 
                switch (wParam) {
                    case ICON_VALUE1:					
						//m_hided = false;
                        //ShowWindow(SW_NORMAL);
						//TrayMessage(NIM_DELETE);
						myShowDlg(true);
                        SetForegroundWindow();
                        SetFocus();
                        return TRUE; 
                        break; 
                } 
                break; //showContextNotMenu

			case WM_RBUTTONUP:
                switch (wParam) {
                    case ICON_VALUE1:					
						myShowContextNotMenu();
                        return TRUE; 
                        break; 
                } 
                break; //showContextNotMenu
        } 
    }//MYWM_KEYPRESS 
	switch(message)
	{
	case MYWM_FORCEGROUND_FF_DRAMAS:
		//MessageBox(_T("F8"));
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("IEDRAMAONTOP"), _T("0")) == _T("1"))
			ResetForcegroundIEDramas();
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FFDRAMAONTOP"), _T("0")) == _T("1"))
			ResetForcegroundFFDramas();
		break;
	case MYWM_FF_DRAMAS:
		//MessageBox(_T("F8"));
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("IEDRAMAONTOP"), _T("0")) == _T("1"))
			TogleIEDramas();
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FFDRAMAONTOP"), _T("0")) == _T("1"))
			TogleFFDramas();
		m_bIsOnTopDramas = !m_bIsOnTopDramas;
		break;
	case MYWM_F2_TUYEN:
		//MessageBox(_T("F8"));
		FixF2TotalCommand();
		break;
	case MYWM_KEYPRESS_F10:
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("HOTKEY"), _T("F10")) == _T("F10"))
		{
			//show
			//MessageBox(_T("F10"));
			if(!m_visible)
			{
				myShowDlg(true);
				SetForegroundWindow();
				SetFocus();
			}
			else
			{
				//myShowDlg(true);
				SetForegroundWindow();
				SetFocus();
			}
		}
		break;
	case MYWM_KEYPRESS_F11:
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("HOTKEY"), _T("F11")) == _T("F11"))
		{
			//show
			//MessageBox(_T("F11"));
			if(!m_visible)
			{
				myShowDlg(true);
				SetForegroundWindow();
				SetFocus();
			}
			else
			{
				//myShowDlg(true);
				SetForegroundWindow();
				SetFocus();
			}
		}
		break;
	case MYWM_KEYPRESS_F2:
		if(bIsWindowsVistaOrLater)
			ToggleMute();
		else
			ToggleMuteXP();
		break;
	case MYWM_MEDIA_TOGGLE_PLAY:
		ToggleMediaPlay();
		break;
	case MYWM_MEDIA_NEXT:
		ToggleMediaNext();
		break;
	case MYWM_SHOW_TOTAL_APP:
		ShowTotalApp();
		break;
		//MYWM_RESIZE_REMOTE_DESKTOP
	case MYWM_RESIZE_REMOTE_DESKTOP:
		//ResizeRemoteDesktopApp(_T("xelamardete_fast_browser_for_xp2_xp - xp-testing2 - Remote Desktop"));
		ResizeRemoteDesktopApp(_T("xelamardete_fast_browser_for_60 - 203.160.10.60 - Remote Desktop"));
		Sleep(1000);
		ResizeRemoteDesktopApp(_T("xelamardete_fast_browser_for_75w7_huy - 203.160.10.75 - Remote Desktop"));
		Sleep(1000);
		ResizeRemoteDesktopApp(_T("203.160.10.72 - Remote Desktop"));
		Sleep(1000);
		ResizeRemoteDesktopApp(_T("203.160.10.77 - Remote Desktop"));
		Sleep(1000);
		//ResizeRemoteDesktopApp(_T("xelamardete_fast_browser_for_xp2_xp - xp-testing2 - Remote Desktop"));
		ResizeRemoteDesktopApp(_T("etmbuild - Remote Desktop"));
		break;
	case MYWM_RUN_ALL_SEN1K:
		//MessageBox(_T("F12"));
		MyGet1KApps(true);
		break;
	case MYWM_RUN_ALL_SEN1K_MULTI:
		//MessageBox(_T("Alt F11"));
		MyGet1KApps(false);
		break;
	case  WM_ENDSESSION://detect windows is shutting down
		CTabMessage *pDlgNetSend = (CTabMessage *)(m_tbCtrl.m_Dialog[3]);
		pDlgNetSend->SaveMsgDefault();
		//return TRUE; 
		break; //showContextNotMenu
//MYDEL	default:
//MYDEL		break;
	}
//	if(message == MYWM_KEYPRESS_F10)
//	{			
//		
////		else
////			ToggleMute();
//	}
//	else
//	{
//		if(message == MYWM_KEYPRESS_F11)
//		{				
//			if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("HOTKEY"), _T("F11")) == _T("F11"))
//			{
//				//show
//				//MessageBox(_T("F11"));
//				if(!m_visible)
//				{
//					myShowDlg(true);
//					SetForegroundWindow();
//					SetFocus();
//				}
//				else
//				{
//					//myShowDlg(true);
//					SetForegroundWindow();
//					SetFocus();
//				}
//			}
////			else
////				ToggleMute();
//		}
//		else
//		{
//			if(message == MYWM_KEYPRESS_F2)
//			{
//				if(bIsWindowsVistaOrLater)
//					ToggleMute();
//				else
//					ToggleMuteXP();
//				//AfxMessageBox(_T("F2"));				
//			}
//			else
//			{
//				if(message == MYWM_MEDIA_TOGGLE_PLAY)//VK_RETURN
//				{					
//					ToggleMediaPlay();
//				}
//				else
//				{
//					if(message == MYWM_MEDIA_NEXT)//VK_RETURN
//					{					
//						ToggleMediaNext();
//					}
//				}
//			}
//		}
//	}
	
	return CDialog::WindowProc(message, wParam, lParam);
}


void CDailyNoteDlg::CheckIsMute()
{
	/*
	if(GetVolume() == 0)
		m_bIsMute = true;
	else
		m_bIsMute = false;
		*/
}

void CDailyNoteDlg::ToggleMute()
{
	

	//CheckIsMute();
	
	if(m_bIsMute)//already mute => unmute
	{
		m_bIsMute = false;

		if(!m_visible)
		{		
			//TrayMessage(NIM_MODIFY, m_strMsgNotify);
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Volume"), _T("On"), 2000);
		}
		//send mgs to Helper App		
	}
	else //not mute => mute it
	{
		m_bIsMute = true;
		//dwVolume = GetVolume();
		//SetVolume(0);
		if(!m_visible)
		{			
			//TrayMessage(NIM_MODIFY, m_strMsgNotify);
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Volume"), _T("Off"), 2000);
		}
	}
	HWND hHelper = MyGetHelperApp();
	if(hHelper)
	{
		::SendMessage(hHelper, MYWM_MTHELPER_TOGGLE_MUTE, 0, 0);
		//AfxMessageBox(_T("sent to helper!"));
	}
	else
	{
		//AfxMessageBox(_T("Not found helper!"));
	}
}

//version 1.0.4.4
//date: 04/08/2019
//run all sen1k apps

HWND CDailyNoteDlg::MyGet1KApps(bool bIsSignle)
{	
	//TCHAR		szBuff[500];

	EnumWindows((WNDENUMPROC )EnumWindowsProc1kApps, static_cast<DWORD_PTR>(bIsSignle));
	
	return m_h1KApp;
}

BOOL CALLBACK EnumWindowsProc1kApps(HWND hWnd, LPARAM lParam)
{
	TCHAR buff[500];
	int buffsize=500;
	//HWND hYahooWnd;
	//hYahooWnd=NULL;
		
	//::GetWindowText(hwnd,buff,buffsize);
	::GetWindowText(hWnd, buff, sizeof (buff) - 1);
	if (_tcslen(buff)<1)
		return TRUE;
	

	CString strTemp(buff);

	//if (_tcslen(buff)>50)
	//	AfxMessageBox(strTemp);

	//CHECK for Yahoo MESSENGER CHAT WINDOW
	string::size_type pos=0;
	//int pos=0;
	//pos=strTemp.rfind("-- Instant Message",strTemp.length());
	//pos=strTemp.Find(_T("anhhuycan"));
	pos=strTemp.Find(_T("HPM -"));
	//pos=strTemp.rfind("nhutnhut",strTemp.length());

	
	//pos=strTemp.rfind(" @",strTemp.length());
	if (pos!=string::npos)
	//if (pos!=-1)
	{
		//char buf[20];
		//AfxMessageBox(_itoa((int) pos, buf, 10));		
		m_h1KApp = hWnd;		
		//m_countWindow = 0;
		//EnumChildWindows(m_hTotalCommand,ChildWndProcTotalCommand,0);

		//return FALSE; //delete this line for fin all windows, does not stop when meet the first window
		//now send ctr + all + r to run this apps
		bool curValue = static_cast<DWORD_PTR>(lParam) == 0 ? false : true;
		//::SendMessage(hWnd, MYWM_RUN_ALL_SEN1K, 0, 0);
		
		if(curValue)
			::SendMessage(hWnd, MYWM_RUN_ALL_SEN1K, 0, 0);
		else
			::SendMessage(hWnd, MYWM_RUN_ALL_SEN1K_MULTI, 0, 0);
		

	}
	return TRUE;
}

void CDailyNoteDlg::ToggleMuteXP()
{
	

	//CheckIsMute();
	
	if(m_bIsMute)//already mute => unmute
	{
		m_bIsMute = false;
		SetVolume(dwVolume);
		
		if(!m_visible)
		{			
			//TrayMessage(NIM_MODIFY, m_strMsgNotify);
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Volume"), _T("On"), 2000);
		}
	}
	else //not mute => mute it
	{
		m_bIsMute = true;

		dwVolume = GetVolume();
		SetVolume(0);

		if(!m_visible)
		{		
			//TrayMessage(NIM_MODIFY, m_strMsgNotify);
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Volume"), _T("Off"), 2000);
		}
	}
}

void CDailyNoteDlg::SetVolume( const DWORD dwVolume )
{
    MMRESULT                        result;
    HMIXER                          hMixer;
    MIXERLINE                       ml   = {0};
    MIXERLINECONTROLS               mlc  = {0};
    MIXERCONTROL                    mc   = {0};
    MIXERCONTROLDETAILS             mcd  = {0};
    MIXERCONTROLDETAILS_UNSIGNED    mcdu = {0};


    // get a handle to the mixer device
	//MIXER_OBJECTF_AUX
    result = ::mixerOpen(&hMixer, MIXER_OBJECTF_MIXER, 0, 0, 0);
	//result = ::mixerOpen(&hMixer, MIXER_OBJECTF_AUX, 0, 0, 0);
    if (MMSYSERR_NOERROR == result)
    {
        ml.cbStruct        = sizeof(MIXERLINE);
        ml.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_SPEAKERS;

        // get the speaker line of the mixer device
        result = ::mixerGetLineInfo((HMIXEROBJ) hMixer, &ml, MIXER_GETLINEINFOF_COMPONENTTYPE);
        if (MMSYSERR_NOERROR == result)
        {
            mlc.cbStruct      = sizeof(MIXERLINECONTROLS);
            mlc.dwLineID      = ml.dwLineID;
            mlc.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
            mlc.cControls     = 1;
            mlc.pamxctrl      = &mc;
            mlc.cbmxctrl      = sizeof(MIXERCONTROL);

            // get the volume controls associated with the speaker line
            result = ::mixerGetLineControls((HMIXEROBJ) hMixer, &mlc, MIXER_GETLINECONTROLSF_ONEBYTYPE);
            if (MMSYSERR_NOERROR == result)
            {
                mcdu.dwValue    = dwVolume;

                mcd.cbStruct    = sizeof(MIXERCONTROLDETAILS);
                mcd.hwndOwner   = 0;
                mcd.dwControlID = mc.dwControlID;
                mcd.paDetails   = &mcdu;
                mcd.cbDetails   = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
                mcd.cChannels   = 1;

                // set the volume
                result = ::mixerSetControlDetails((HMIXEROBJ) hMixer, &mcd, MIXER_SETCONTROLDETAILSF_VALUE);
//                if (MMSYSERR_NOERROR == result)
//                    AfxMessageBox("Volume changed!");
//                else
//                    AfxMessageBox("mixerSetControlDetails() failed");
            }
            else
                AfxMessageBox(_T("mixerGetLineControls() failed"));
        }
        else
            AfxMessageBox(_T("mixerGetLineInfo() failed"));
    
        mixerClose(hMixer);
    }
    else
        AfxMessageBox(_T("mixerOpen() failed"));
}

//====================================================================

DWORD CDailyNoteDlg::GetVolume( void )
{
    DWORD                           dwVolume = -1;
    MMRESULT                        result;
    HMIXER                          hMixer;
    MIXERLINE                       ml   = {0};
    MIXERLINECONTROLS               mlc  = {0};
    MIXERCONTROL                    mc   = {0};
    MIXERCONTROLDETAILS             mcd  = {0};
    MIXERCONTROLDETAILS_UNSIGNED    mcdu = {0};


    // get a handle to the mixer device
    result = ::mixerOpen(&hMixer, 0, 0, 0, MIXER_OBJECTF_HMIXER);
	//result = ::mixerOpen(&hMixer, 0, 0, 0, MIXER_OBJECTF_AUX);
	
    if (MMSYSERR_NOERROR == result)
    {
        ml.cbStruct        = sizeof(MIXERLINE);
        ml.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_SPEAKERS;

        // get the speaker line of the mixer device
        result = ::mixerGetLineInfo((HMIXEROBJ) hMixer, &ml, MIXER_GETLINEINFOF_COMPONENTTYPE);
        if (MMSYSERR_NOERROR == result)
        {
            mlc.cbStruct      = sizeof(MIXERLINECONTROLS);
            mlc.dwLineID      = ml.dwLineID;
            mlc.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
            mlc.cControls     = 1;
            mlc.pamxctrl      = &mc;
            mlc.cbmxctrl      = sizeof(MIXERCONTROL);

            // get the volume controls associated with the speaker line
            result = ::mixerGetLineControls((HMIXEROBJ) hMixer, &mlc, MIXER_GETLINECONTROLSF_ONEBYTYPE);
            if (MMSYSERR_NOERROR == result)
            {
                mcd.cbStruct    = sizeof(MIXERCONTROLDETAILS);
                mcd.hwndOwner   = 0;
                mcd.dwControlID = mc.dwControlID;
                mcd.paDetails   = &mcdu;
                mcd.cbDetails   = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
                mcd.cChannels   = 1;

                // get the volume
                result = ::mixerGetControlDetails((HMIXEROBJ) hMixer, &mcd, MIXER_SETCONTROLDETAILSF_VALUE);
                if (MMSYSERR_NOERROR == result)
                    dwVolume = mcdu.dwValue;
                else
                    AfxMessageBox(_T("mixerGetControlDetails() failed"));
            }
            else
                AfxMessageBox(_T("mixerGetLineControls() failed"));
        }
        else
            AfxMessageBox(_T("mixerGetLineInfo() failed"));
    
        mixerClose(hMixer);
    }
    else
        AfxMessageBox(_T("mixerOpen() failed"));

    return (dwVolume);
}

void CDailyNoteDlg::myShowContextNotMenu()
{
	CMenu menu ;
   // Load and Verify Menu

   VERIFY(menu.LoadMenu(IDR_ContextNoMenu));
   CMenu* pPopup = menu.GetSubMenu (0);
   //my code begin to check if the mit is enable or disable
   /*
   if(m_btRunObject.IsWindowEnabled() == false)
   {	   
	   menu.EnableMenuItem(mitStart, MF_GRAYED);
   }
   if(m_btStopObject.IsWindowEnabled() == false)
   {
	   menu.EnableMenuItem(mitStop, MF_GRAYED);
   }
   */



   ASSERT(pPopup != NULL);

   // Get the cursor position

   POINT pt ;
   GetCursorPos (&pt) ;

   // Fix Microsofts' BUG!!!!

   SetForegroundWindow();

   ///////////////////////////////////

   // Display The Menu

   pPopup->TrackPopupMenu(TPM_LEFTALIGN |
   TPM_RIGHTBUTTON,pt.x, pt.y, AfxGetMainWnd());
   SetForegroundWindow();
}

void CDailyNoteDlg::OnpmMitExit() 
{
	// TODO: Add your command handler code here
	EndDialog(0);
}

void CDailyNoteDlg::OnpmMitShow() 
{
	// TODO: Add your command handler code here
	myShowDlg(true);
}

bool CDailyNoteDlg::setHookKeyBoard()
{
	hmeDll = LoadLibrary((LPCTSTR) _T("HME.dll"));
	//hmeDll = LoadLibrary((LPCTSTR) "HME.dll");
	if(hmeDll == NULL)
	{
		MessageBox(_T("Can't load HME.dll"));
		return false;
	}
	
	hModuleKeyboard = GetModuleHandle(_T("HME"));
	
	if(hModuleKeyboard == NULL)
	{
		return false;
	}
	//GetModuleHandle("TestDLL")
	/*
	msg.Format("exe file: last error code: %d", GetLastError());
	MessageBox(msg);
	*/

	oldKeyBoardHook = SetWindowsHookEx(WH_KEYBOARD, (HOOKPROC)GetProcAddress(hModuleKeyboard,"myKeyBoardHook"), hModuleKeyboard, 0);
	/*
	msg.Format("exe file: last error code: %d", GetLastError());
	MessageBox(msg);
	*/
	//set hhook to dll
	
	pSetHHook = (MYKBPROC) GetProcAddress(hModuleKeyboard, "setHKeyBoardHook");
	//test pSetHHook if nessesary
	(pSetHHook)(oldKeyBoardHook);

	//set hWnd to get message
	pSetHWNDRectMsg = (MYKBPROC3) GetProcAddress(hModuleKeyboard, "setHWNDRecMsg");
	(pSetHWNDRectMsg)(AfxGetMainWnd()->m_hWnd);
	return true;
}

void CDailyNoteDlg::setUnHookKeyBoard()
{
	//free dll	
	if(hmeDll)
	{
		FreeLibrary(hmeDll);
		hmeDll = 0;	
	}	
	//UnhookWindowsHookEx return != if success else return  0
	if ((oldKeyBoardHook != 0) && (UnhookWindowsHookEx(oldKeyBoardHook) != 0))
	{
		//unhook keyboard
		oldKeyBoardHook = NULL;
		/*
		if ((oldMouseHook != 0) && (UnhookWindowsHookEx(oldMouseHook) != 0))
		{		
			//unhook mouse
			oldMouseHook = NULL;
			return true;
		}
		*/
	}	
}

void CDailyNoteDlg::MyStartTimer()
{
	switch(m_myStatus)
	{
		case MYSTOP:
			if(m_RecordNoteArray.GetSize() > 0)
			{
				MySetCurrentCheckingTime();
				if(m_isHaveCurrentTimeCheking)
				{
					m_pTimerThread = AfxBeginThread(MyTimer, this, THREAD_PRIORITY_NORMAL, 0,
						CREATE_SUSPENDED);
					m_pTimerThread->m_bAutoDelete = false;				
					m_myStatus = MYRUN;
									
					m_pTimerThread->ResumeThread();	
				}
			}

			break;
		case MYPAUSE:	
			m_myStatus = MYRUN;
			break;
		case MYRUN:
			m_myStatus = MYPAUSE;
			break;
		default:
			break;
	}
}
LRESULT CDailyNoteDlg::OnCheckTime(WPARAM, LPARAM)
{
	COleDateTime timeNow = COleDateTime::GetCurrentTime();

	//minute
	if(m_timeChecking.GetMinute() == timeNow.GetMinute())
	{
		//hour
		if(m_timeChecking.GetHour() == timeNow.GetHour())
		{
			//second
			if(m_timeChecking.GetSecond() == timeNow.GetSecond())
			{
				//day
				if(m_timeChecking.GetDay() == timeNow.GetDay())
				{
					//month
					if(m_timeChecking.GetMonth() == timeNow.GetMonth())
					{
						//year
						if(m_timeChecking.GetYear() == timeNow.GetYear())
						{
							//play a sound file
							//MessageBox(_T("helo"));
							CMAINTAB *pDlgMain = (CMAINTAB *)(m_tbCtrl.m_Dialog[0]);
							m_myStatus = MYPAUSE;
							//AfxMessageBox(pDlgMain->m_wndList.GetItemText(m_currentIndexWaitingToShow, 2));
							//on top message
							m_sMsgContain = timeNow.Format(_T("%H:%M:%S - ")) + pDlgMain->m_wndList.GetItemText(m_currentIndexWaitingToShow, 3);

							if (!MyProccessContainMessage(m_sMsgContain))
							{
								/*::MessageBox(NULL,
									sMsgContain,
									_T("Alert"),
									MB_ICONINFORMATION | MB_OK | MB_SETFOREGROUND | MB_TOPMOST);*/
								//MyStartThreadMessageBox(m_sMsgContain);
							}
							MyStartThreadMessageBox(m_sMsgContain);

							MyReRunTimer();
						}
					}
				}
			}
		}
	}	
	
	return 1;
}

LRESULT CDailyNoteDlg::OnReadMailSlot(WPARAM, LPARAM)
{
	DWORD rs;
	//CTabMessage *pTabMsg = (CTabMessage *)(m_tbCtrl.m_Dialog[3]);
	rs = m_pTabMessage->GetMessage();
	if(rs)
	{
		if(m_pTabMessage->m_bIsFlashWindow)
		{
			//m_pTimerThread->SuspendThread();
			//m_myStatus = MYPAUSE;
			myShowDlg(true, SW_SHOWNOACTIVATE);
			//SetForegroundWindow();
			//SetFocus();
			FlashWindow(TRUE);
			//MYDEL		//AfxMessageBox(m_strArrMsg.GetAt(2));
			//MYDEL		::MessageBox(NULL,
			//MYDEL								m_strArrMsg.GetAt(2),
			//MYDEL								_T("Alert"),
			//MYDEL								MB_ICONINFORMATION | MB_OK | MB_SETFOREGROUND | MB_TOPMOST);
			//m_pTimerThread->ResumeThread();
			//insert to list
			//pTabMsg->AddNewMsg(&m_strArrMsg);
			//active message tab
			m_tbCtrl.SetCurSel(3);
			m_tbCtrl.ActivateTabDialogs();
			//MyReRunTimer();		
		}
		//comment out for version 1.0.2.4
//MYDEL		//show traymessage
//MYDEL		if(!m_visible)
//MYDEL		{		
//MYDEL			//TrayMessage(NIM_MODIFY, m_strMsgNotify);
//MYDEL			CString msg;
//MYDEL			msg.Format(_T("from: %s"), m_strArrMsg.GetAt(0));
//MYDEL			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("New message!"), msg, 2000);
//MYDEL		}
		CString msg;
		msg.Format(_T("from: %s"), m_strArrMsg.GetAt(0));
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("New message!"), msg, 2000);
	}
	
	return rs;
}

LRESULT CDailyNoteDlg::OnDisableBroadCast(WPARAM, LPARAM)
{
	CNetSendTab *pDlgNetSend = (CNetSendTab *)(m_tbCtrl.m_Dialog[2]);
	pDlgNetSend->CheckDisableBroadCast();	
	return 1;
}

UINT CDailyNoteDlg::MyTimer(LPVOID pParam)
{
/*
	enum TimerStatus {
       MYSTOP = 0,
       MYRUN = 1,
       MYPAUSE = 2,
    };

*/
	//CHighTimeSpan tsTemp;
	//CSubtitleDlg *pSubDlg = (CSubtitleDlg *)(pParam);

	//tsTemp = CHighTimeSpan(0,0,0,0,100,0,0);
	//CSubtitleDlg *pSubDlg = (CSubtitleDlg *) CWnd::FromHandle (*(HWND *)pParam);

	CDailyNoteDlg *pSubDlg = (CDailyNoteDlg*)pParam;
	//pSubDlg->m_tsTimer = CHighTimeSpan(0,0,0,0,0,0,0);	
	while(1)
	{
		switch(pSubDlg->m_myStatus)
		{
			case MYSTOP:
				return 0;
				break;
			case MYPAUSE:				
				break;
			case MYRUN:
				//pSubDlg->m_tsTimer += tsTemp;
				//_ultow(pSubDlg->m_tsTimer.GetTotalMilliSeconds(), m_txtSecondValue1, 10);
				//pSubDlg->MySetTxtSecond1();
				pSubDlg->PostMessage(MYWM_CHECKTIME, 0, 0);
				//check disable broadcast button on netsend GUI
				//comment out for version 1.0.4.2
				pSubDlg->PostMessage(MYWM_DISABLEBROADCAST, 0, 0);
				
				//pSubDlg->UpdateData(false);
				break;
			default:
				break;
		}
		::Sleep(1000);
	}

	return 0;
}


void CDailyNoteDlg::MySetCurrentCheckingTime()
{
	//int iCurrentRecAlartBackup = m_currentIndexWaitingToShow;
	COleDateTime oleDT, oleTempDT;
	COleDateTime oleMinDT;
	COleDateTime oleDTNow = COleDateTime::GetCurrentTime();
	//COleDateTime oleMinDT;
	CRecordNote *pRectNote;
	CStringArray arrStr;
	bool isFirstItem = true;
	m_isHaveCurrentTimeCheking = false;
	eFrequency eFreItem;
	int totalItem = m_RecordNoteArray.GetSize();
	CString strMsg, strSequence;
	//CString strMsgNotify;
	for(int i = 0; i < totalItem; i++)
	{
		
		pRectNote = m_RecordNoteArray.GetAt(i);
		//pRectNote->m_bIsChecked = false;
		// arrStr
		StringSplit(pRectNote->m_line, arrStr, _T(';'));
		// Configure per-thread locale to cause all subsequently created 
		// threads to have their own locale.
		//_configthreadlocale(_ENABLE_PER_THREAD_LOCALE);
		//1066 is vietnam datetime
		oleDT.ParseDateTime(arrStr.GetAt(0));
		//if(!oleDT.ParseDateTime(arrStr.GetAt(0), LOCALE_NOUSEROVERRIDE, 1066))
		//	oleDT.ParseDateTime(arrStr.GetAt(0));
		
		eFreItem = GetFrequencyEnum(arrStr.GetAt(1));
		oleDT = GetPresentDT(oleDT, eFreItem);
		//eFreItem = GetFrequencyEnum(strFre);		
		/*
		if(i==8)
		{	
			CString strFre;
			strFre = arrStr.GetAt(0);
		}
		oleTempDT = GetPresentDT(oleDT, eFreItem);
		int n,t,d,h,p,s;
		n=oleTempDT.GetYear();t= oleTempDT.GetMonth();d= oleTempDT.GetDay();
			h= oleTempDT.GetHour();p=oleTempDT.GetMinute();s=oleTempDT.GetSecond();

		oleDT.SetDateTime(oleTempDT.GetYear(), oleTempDT.GetMonth(), oleTempDT.GetDay()
			, oleTempDT.GetHour(), oleTempDT.GetMinute(), oleTempDT.GetSecond());
		CString strFre = arrStr.GetAt(0);
		*/
		//if(i==8)
//		{
//			CString strMsg;		
//			//strMsg.Format(_T("has check: %d timeItem: %s timeMin: %s index: %d"),m_isHaveCurrentTimeCheking, oleDT.Format(_T("%#c")), oleMinDT.Format(_T("%#c")), m_currentIndexWaitingToShow);
//			//strMsg += _T(" ") + strFre;
//			//AfxMessageBox(strMsg);
//
//			strMsg.Format(_T("timeTemp: %s"), oleDT.Format(_T("%#c")));
//			//strMsg += _T(" ") + strFre;
//			AfxMessageBox(strMsg);
//		}
		/* //4/15/2013 by huy, tc, resean: do require for sitch
		switch(eFreItem)
		{
		case ONCE:
			if(oleDT > oleDTNow)
			{
				if(isFirstItem)
				{
					oleMinDT = oleDT;
					m_currentIndexWaitingToShow = i;
					m_isHaveCurrentTimeCheking = true;
					isFirstItem = false;
					//m_strMsgNotify = arrStr.GetAt(0) + _T(" - ") + arrStr.GetAt(1) +
					//	_T(" - ") + arrStr.GetAt(2);
					//m_strMsgNotify = oleMinDT.Format(_T("Ng\x61y: %d th\xE1ng%m  n\x103m:%Y"));
					//m_strMsgNotify += _T("N\x1ED9i \x64ung: ") + arrStr.GetAt(1);
					//m_strMsgNotify += _T("L\x1EB7p l\x1EA1i: ") + arrStr.GetAt(2);
					strSequence = arrStr.GetAt(1);
					strMsg = arrStr.GetAt(2);
					//SetStringNotify(&arrStr, oleMinDT);

				}
				else
				{					
					if(oleDT < oleMinDT)
					{
						oleMinDT = oleDT;
						m_currentIndexWaitingToShow = i;
						//m_strMsgNotify = arrStr.GetAt(0) + _T(" - ") + arrStr.GetAt(1) +
						//	_T(" - ") + arrStr.GetAt(2);
						//SetStringNotify(&arrStr, oleMinDT);
						strSequence = arrStr.GetAt(1);
						strMsg = arrStr.GetAt(2);
					}
				}
			}
			break;
		case DAILY:			
		case WEEKLY:			
		case MONTHLY:
			if(isFirstItem)
			{
				oleMinDT = oleDT;
				m_currentIndexWaitingToShow = i;
				m_isHaveCurrentTimeCheking = true;
				isFirstItem = false;
				//m_strMsgNotify = arrStr.GetAt(0) + _T(" - ") + arrStr.GetAt(1) +
				//	_T(" - ") + arrStr.GetAt(2);
				//SetStringNotify(&arrStr, oleMinDT);
				strSequence = arrStr.GetAt(1);
				strMsg = arrStr.GetAt(2);
			}
			else
			{
				if(oleDT < oleMinDT)
				{
					oleMinDT = oleDT;
					m_currentIndexWaitingToShow = i;
					//m_strMsgNotify = arrStr.GetAt(0) + _T(" - ") + arrStr.GetAt(1) +
					//	_T(" - ") + arrStr.GetAt(2);
					//SetStringNotify(&arrStr, oleMinDT);
					strSequence = arrStr.GetAt(1);
					strMsg = arrStr.GetAt(2);
				}
			}
			break;
		default:			
			break;
		}//end switch
		*/
		if(oleDT > oleDTNow)
		{
			if(isFirstItem)
			{
				oleMinDT = oleDT;
				m_currentIndexWaitingToShow = i;
				m_isHaveCurrentTimeCheking = true;
				isFirstItem = false;
				//m_strMsgNotify = arrStr.GetAt(0) + _T(" - ") + arrStr.GetAt(1) +
				//	_T(" - ") + arrStr.GetAt(2);
				//m_strMsgNotify = oleMinDT.Format(_T("Ng\x61y: %d th\xE1ng%m  n\x103m:%Y"));
				//m_strMsgNotify += _T("N\x1ED9i \x64ung: ") + arrStr.GetAt(1);
				//m_strMsgNotify += _T("L\x1EB7p l\x1EA1i: ") + arrStr.GetAt(2);
				strSequence = arrStr.GetAt(1);
				strMsg = arrStr.GetAt(2);
				//SetStringNotify(&arrStr, oleMinDT);
				
			}
			else
			{					
				if(oleDT < oleMinDT)
				{
					oleMinDT = oleDT;
					m_currentIndexWaitingToShow = i;
					//m_strMsgNotify = arrStr.GetAt(0) + _T(" - ") + arrStr.GetAt(1) +
					//	_T(" - ") + arrStr.GetAt(2);
					//SetStringNotify(&arrStr, oleMinDT);
					strSequence = arrStr.GetAt(1);
					strMsg = arrStr.GetAt(2);
				}
			}
		}
	}//end for	
	
	if(m_isHaveCurrentTimeCheking)
	{		
		//EF,EB,DE
		//SetItemBkColor(m_currentIndexWaitingToShow,RGB(0xEF, 0xEB, 0xDE)); //gray F7,EF,F7
		SetItemBkColor(m_currentIndexWaitingToShow,RGB(0xF7, 0xEF, 0xF7)); //gray F7,EF,F7
		m_timeChecking = oleMinDT;
		if(!m_visible)
		{
			SetStringNotify(strMsg, strSequence);
			TrayMessage(NIM_MODIFY, m_strMsgNotify);
		}
		/*
		CString strMsg;		
		strMsg.Format(_T("has check: %d time: %s index: %d"),m_isHaveCurrentTimeCheking, m_timeChecking.Format(_T("%#c")), m_currentIndexWaitingToShow);
		AfxMessageBox(strMsg);
		*/
	}	
}


void CDailyNoteDlg::SetStringNotify(CString strMsg, CString strSequence)
{
	//m_strMsgNotify = m_timeChecking.Format(_T("Ng\x61y: %d th\xE1ng%m  n\x103m:%Y"));
	//m_strMsgNotify.Format(_T("Ng\xE0y: %d, T\x61h\x31ng: %d, N\x103m: %d\nN\x1ED9i \x64ung: %s L\x1EB7p l\x1EA1i: %s"), 
	//	m_timeChecking.GetDay(), m_timeChecking.GetMonth(), m_timeChecking.GetYear(), strMsg, strSequence);
	int maxChar = 16;
	if(strMsg.GetLength() > maxChar)
		strMsg = strMsg.Left(maxChar);
	m_strMsgNotify.Format(_T("Ng\xE0y: %d, Th\xE1ng: %d\n%d Gi\x1EDD %d Ph\xFAt\nMsg: %s"), 
		m_timeChecking.GetDay(), m_timeChecking.GetMonth(), m_timeChecking.GetHour(), m_timeChecking.GetMinute(), strMsg);
	//m_strMsgNotify += strMsg;
	//m_strMsgNotify = _T("Ng\xE0y: ") + m_timeChecking.GetDay();
	//m_strMsgNotify += _T(" T\x61h\x31ng: ") + m_timeChecking.GetMonth();
	//m_strMsgNotify += _T(" N\x103m: ") + m_timeChecking.GetYear() + _T("\n");
//	m_strMsgNotify += _T("N\x1ED9i \x64ung: ") + strMsg;
//	m_strMsgNotify += _T("L\x1EB7p l\x1EA1i: ") + strSequence;
}

void CDailyNoteDlg::MyReRunTimer()
{
	m_myStatus = MYSTOP;
	m_isHaveCurrentTimeCheking = false;
	//clear backgroud of current selected record
	if(m_currentIndexWaitingToShow != -1)	
	{
		SetItemBkColor(m_currentIndexWaitingToShow,RGB(255,255,255)); //white
		//pDlgMain->m_wndList.SetItemTextColor(m_currentIndexWaitingToShow, -1, RGB(0,0,0), true);
	}
	
	m_currentIndexWaitingToShow = -1;
	MyStartTimer();
}

void CDailyNoteDlg::SetItemBkColor(int indexRow, COLORREF colorToSet)
{
	CMAINTAB *pDlgMain = (CMAINTAB *)(m_tbCtrl.m_Dialog[0]);
	//COLORREF colorCurrentActiveRecord;
	//colorCurrentActiveRecord = RGB(255,0,0); // red
	//color = RGB(0,255,0); // green
	//colorCurrentActiveRecord = RGB(0,0,255); // blue
	//EF,EB,DE
	COLORREF colorText = RGB(255,255,255);
	if(colorText == colorToSet)
		colorText = RGB(0,0,0);
	else
		colorText = RGB(255,0,0);
	pDlgMain->m_wndList.SetItemBkColor(m_currentIndexWaitingToShow, -1, colorToSet, true);
	pDlgMain->m_wndList.SetItemTextColor(m_currentIndexWaitingToShow, -1, colorText, true);
}

bool CDailyNoteDlg::MyCompareYMD(COleDateTime startDT, COleDateTime endDT)
{
	bool bRs = false;
	if(startDT.GetYear() < endDT.GetYear())
	{
		bRs = true;
	}
	else
	{
		if(startDT.GetYear() == endDT.GetYear())
		{
			if(startDT.GetMonth() < endDT.GetMonth())
			{
				bRs = true;
			}						
			else
			{
				if(startDT.GetMonth() == endDT.GetMonth())
				{
					if(startDT.GetDay() < endDT.GetDay())
					{
						bRs = true;
					}					
				}
			}
		}
	}
	return bRs;
}

bool CDailyNoteDlg::MyCompareHMS(COleDateTime startDT, COleDateTime endDT)
{
	bool bRs = false;
	if(startDT.GetHour() < endDT.GetHour())
	{
		bRs = true;
	}
	else
	{
		if(startDT.GetHour() == endDT.GetHour())
		{
			if(startDT.GetMinute() < endDT.GetMinute())
			{
				bRs = true;
			}						
			else
			{
				if(startDT.GetMinute() == endDT.GetMinute())
				{
					if(startDT.GetSecond() < endDT.GetSecond())
					{
						bRs = true;
					}
				}
			}
		}
	}
	return bRs;
}

COleDateTime CDailyNoteDlg::GetPresentDT(COleDateTime dtInput, eFrequency eFre)
{
	COleDateTimeSpan ts;
	COleDateTime dtRs = dtInput;
	COleDateTime dtNow = COleDateTime::GetCurrentTime();
	int dayToAdd, dowInput, dowNow, dayInput, dayNow;		
	switch(eFre)
	{
	case ONCE:
		//dtRs = dtInput;
		break;
	case DAILY:
		//dtRs = dtInput;
		
		dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
			dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
		if(dtRs < dtNow)
		{
			//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 1, 
			//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
			dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
				dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

			ts.SetDateTimeSpan(1, 0, 0, 0);
			dtRs = dtRs + ts;
			//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
			//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
			//Rs = dtRs + ts.SetDateTimeSpan(1, 0, 0, 0);
		}
		break;
	case WEEKLY:		
		dowInput = dtInput.GetDayOfWeek();
		dowNow = dtNow.GetDayOfWeek();
		if(dowInput == dowNow)
		{			
			//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay());
			dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
				dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
			if(dtRs < dtNow)
			{
				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtInput.GetDay(), 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());		

				ts.SetDateTimeSpan(7, 0, 0, 0);
				dtRs = dtRs + ts;
			}
		}
		else //day of week of input != day of week of now
		{
			if(dowInput < dowNow)
			{
				dayToAdd = dowNow - dowInput;				
				//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7 - dayToAdd);

				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7 - dayToAdd, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7 - dayToAdd, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
					dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				//dtRs = dtRs + ts.SetDateTimeSpan(7 - dayToAdd, 0, 0, 0);

				ts.SetDateTimeSpan(7 - dayToAdd, 0, 0, 0);
				dtRs = dtRs + ts;
			}
			else
			{
				dayToAdd = dowInput - dowNow;
				//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + dayToAdd);

				dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
					dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + dayToAdd, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				ts.SetDateTimeSpan(dayToAdd, 0, 0, 0);
				dtRs = dtRs + ts;				
			}
		}
		break;
	case MONTHLY:		
		dayInput = dtInput.GetDay(); // day of month
		dayNow = dtNow.GetDay();// day of month
		if(dayInput == dayNow)
		{			
			//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dayNow);
			dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dayNow, 
				dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
//			CString st;
//			CDateTimeFormat dtf;
//	dtf.SetDateTime(dtRs);
//	dtf.SetFormat(_T("dd MM yyyy hh:mm:ss tt"));
//	st.Format(_T("%s %d"),dtf.GetString(), dtInput.GetSecond());
//	//st = dtf.GetString();
//	TRACE(st);
//	MessageBox(st);
			/*
			if(dayInput < dtNow)
			{
				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dtNow.GetDay(), 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dtNow.GetDay(), 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				//ts.SetDateTimeSpan(dayToAdd, 0, 0, 0);
				if(dtNow.GetMonth() < 12)
				{
					int daysInMonth = MyGetDaysInMonth(dtNow.GetMonth() + 1, dtNow.GetYear());
					if(daysInMonth < dtInput.GetDay())
					{
						dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, daysInMonth, 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					else
					{
						dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dtInput.GetDay(), 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
				}
				else
				{
					int daysInMonth = MyGetDaysInMonth(1, dtNow.GetYear() + 1);
					if(daysInMonth < dtInput.GetDay())
					{
						dtRs.SetDateTime(dtNow.GetYear() + 1, 1, daysInMonth, 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					else
					{
						dtRs.SetDateTime(dtNow.GetYear() + 1, 1, dtInput.GetDay(), 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
				}				
			}
			*/
		}
		else //day of input != day of now
		{
			if(dayInput < dayNow)
			{				
				//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth() + 1, dayInput);

				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dayInput, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				if(dtNow.GetMonth() < 12)
				{
					int daysInMonth = MyGetDaysInMonth(dtNow.GetMonth() + 1, dtNow.GetYear());
					if(daysInMonth < dtInput.GetDay())
					{
						dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, daysInMonth, 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					else
					{
						dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dtInput.GetDay(), 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					
				}
				else
				{
					int daysInMonth = MyGetDaysInMonth(1, dtNow.GetYear() + 1);
					if(daysInMonth < dtInput.GetDay())
					{
						dtRs.SetDateTime(dtNow.GetYear() + 1, 1, daysInMonth, 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					else
					{
						dtRs.SetDateTime(dtNow.GetYear() + 1, 1, dtInput.GetDay(), 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}					
				}
			}
			else//input > now
			{
				//dayToAdd = dayInput - dayNow;
				//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dayNow + dayToAdd);
				    // Adjust time and frac time
				
				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dayNow + dayToAdd, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				int daysInMonth = MyGetDaysInMonth(dtNow.GetMonth(), dtNow.GetYear());
				if(dtInput.GetDay() >= daysInMonth)
				{
					dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), daysInMonth, 
						dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				}
				else
				{
					dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtInput.GetDay(), 
						dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				}				
			}
		}
		break;
	default:		
		break;
	}
	
	return dtRs;
}



bool CDailyNoteDlg::MyCompareYMDEqual(COleDateTime startDT, COleDateTime endDT)
{
	bool bRs = false;
	if(startDT.GetYear() == endDT.GetYear() && startDT.GetYear() == endDT.GetYear() 
		&& startDT.GetMonth() == endDT.GetMonth())	
		bRs = true;
	return bRs;
}

eFrequency CDailyNoteDlg::GetFrequencyEnum(CString strFre)
{
	eFrequency eRs = ONCE;
	//CString sCase("AAAA");
	STR_SWITCH(strFre)      //Start of switch
	{                        //Opening and closing braces
		//NOT MANDATORY for switch.
		STR_CASE(_T("ONCE"))
		{                      //MANDATORY for case.
			eRs = ONCE;
			break;               //break has to in braces of case
		}  
		STR_CASE(_T("DAILY"))
		{                      //MANDATORY for case.
			eRs = DAILY;
			break;               //break has to in braces of case
		}                      //Opening and closing braces
		//MANDATORY for case.
		
		STR_CASE(_T("WEEKLY"))
		{
			eRs = WEEKLY;
			break;
		}
		STR_CASE(_T("MONTHLY"))
		{
			eRs = MONTHLY;
			break;
		}
		DEFAULT_CASE()
		{
			eRs = ONCE;//Default handling if any
			break;
		}
	}                        //Opening and closing braces
	//NOT MANDATORY for switch
	STR_SWITCH_END()         //MANDATORY statement
		

	return eRs;
}

COleDateTime CDailyNoteDlg::MySetDateTime(int year, int month, int day, int hour, int minute, int second)
{
	COleDateTime dtRs;
	minute = minute + second / 60;	
	second = second % 60;
	hour = hour + minute / 60;
	minute = minute % 60;
	day = day + hour / 24;
	hour = hour % 24;
	month = month + day / 31;
	day = day % 31;
	year = year + month / 12;
	month = month % 12;
	dtRs.SetDateTime(year, month, day, hour, minute, second);
	return dtRs;

}

int CDailyNoteDlg::MyGetDaysInMonth(int month, int year)
{
	int dayNo = 28;	
	
	switch(month)
	{
	case 1:		
	case 3:		
	case 5:		
	case 7:		
	case 8:		
	case 10:		
	case 12:
		dayNo = 31;
		break;		
	case 2:
		if((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
			dayNo = 29;
		else
			dayNo = 28;
		break;		
	case 4:		
	case 6:		
	case 9:
	case 11:
		dayNo = 30;
		break;
	default:
		break;
	}
	return dayNo;
}




//DEL void CDailyNoteDlg::setRtbFont(CString ins_strFont)
//DEL {
//DEL 	CDC *pDC = GetDC();
//DEL 
//DEL 	// create UNICODE font
//DEL 	LOGFONT lf;
//DEL 	
//DEL 	memset(&lf, 0, sizeof(lf));
//DEL 	lf.lfHeight =
//DEL 	MulDiv(25, ::GetDeviceCaps(pDC->m_hDC,
//DEL 		 LOGPIXELSY), 172);
//DEL 	lf.lfWeight = FW_NORMAL;
//DEL 	lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
//DEL 	wcscpy(lf.lfFaceName, ins_strFont);
//DEL 	m_font.CreateFontIndirect(&lf);
//DEL 
//DEL 	// apply the font to the controls
//DEL 	//m_list.SetFont(&m_font);
//DEL 	//m_edit.SetFont(&m_font);
//DEL 	CMAINTAB *pDlgMain = (CMAINTAB *)(m_tbCtrl.m_Dialog[0]);
//DEL 	pDlgMain->m_wndList.SetFont(&m_font);
//DEL 	
//DEL 	//CRichEditCtrl* pTxtResult;
//DEL 	//pTxtResult = (CRichEditCtrl*) GetDlgItem(txtResult);
//DEL 	//pTxtResult->SetFont(&m_font);	
//DEL 
//DEL 	// release the device context.
//DEL 	ReleaseDC(pDC);
//DEL }

COleDateTime CDailyNoteDlg::MyAddMonth(COleDateTime dtInput, int monthToAdd)
{
	COleDateTime dtRs;
	int dayInput = dtInput.GetDay();
	int month = dtRs.GetMonth() + monthToAdd;
	int year = dtRs.GetYear() + month/12;
	month = month%12;
	int dimNew = MyGetDaysInMonth(month, year);

	
	if(dayInput <= dimNew)
	{
		dtRs.SetDateTime(year, month, dayInput, 
			dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
	}
	else
	{
		dtRs.SetDateTime(year, month, dimNew, 
			dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
	}
	return dtRs;	
}

int CDailyNoteDlg::DoModal() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	//return CDialog::DoModal();
	CDialogTemplate dlt;
	int nResult;
	// load dialog template
	if (!dlt.Load(MAKEINTRESOURCE(CDailyNoteDlg::IDD))) return -1;
	// set your own font, for example "Arial", 10 pts.
	dlt.SetFont(_T("Arial"), 10);
	// get pointer to the modified dialog template
	LPSTR pdata = (LPSTR)GlobalLock(dlt.m_hTemplate);
	// let MFC know that you are using your own template
	m_lpszTemplateName = NULL;
	InitModalIndirect(pdata);
	// display dialog box
	nResult = CDialog::DoModal();
	// unlock memory object
	GlobalUnlock(dlt.m_hTemplate);
	return nResult;
}



HWND CDailyNoteDlg::MyGetHelperApp()
{
	//HWND hHelper = null;
	
	TCHAR		szBuff[100];

	if(hHelper == NULL)
	{
		hHelper = ::FindWindow(NULL, _T("MTHelper v1.0.0.5"));
		//comment out for version 1.0.4.2
	}
	else
	{
		::GetWindowText(hHelper, szBuff, sizeof (szBuff) - 1);
		if(szBuff != _T("MTHelper v1.0.0.5"))
		{
			hHelper = ::FindWindow(NULL, _T("MTHelper v1.0.0.5"));
		}
	}

	return hHelper;

}

HWND CDailyNoteDlg::MyGetMediaApp()
{
	//HWND hHelper = null;
	
	TCHAR		szBuff[100];

	if(m_hMedia == NULL)
	{
		if(bIsWindowsVistaOrLater)
		{
			m_hMedia = ::FindWindowEx(NULL, NULL, _T("WMPlayerApp"), _T("Windows Media Player"));
		}
		else
		{
			m_hMedia = ::FindWindow(NULL, _T("Windows Media Player"));
		}
	}
	else
	{
		::GetWindowText(m_hMedia, szBuff, sizeof (szBuff) - 1);		
		if(szBuff != _T("Windows Media Player"))
		{			
			if(bIsWindowsVistaOrLater)
			{
				m_hMedia = ::FindWindowEx(NULL, NULL, _T("WMPlayerApp"), _T("Windows Media Player"));
			}
			else
			{
				m_hMedia = ::FindWindow(NULL, _T("Windows Media Player"));
			}
		}		
	}

	return m_hMedia;
}

void CDailyNoteDlg::ToggleMediaPlay()
{
	MyGetMediaApp();
	if(m_hMedia)
	{
		//now find child window
		//CDailyNoteApp *pApp = (CDailyNoteApp*) AfxGetApp();
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Media"), _T("Play/Pause!"), 2000);
		//CTextFileWrite myfile(_T("C:\\dailynote.txt"), CTextFileWrite::UTF_8);
		//myfile.Write(_T("found parent!"));
		//	myfile.WriteEndl();
		//	myfile.Close();
		//TRACE(_T("parent: 0x%x"), m_hMedia);
		::EnumChildWindows(m_hMedia, ChildWndProcMediaPlay,0);
		//EnumChildWindows(NULL, ChildWndProcMediaPlay,0);
		
		TRACE(_T("end!"));
	}
	else
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Media"), _T("Not found media player!"), 2000);
}

BOOL CALLBACK ChildWndProcMediaPlay(HWND hwnd, LPARAM lParam)
{
	//TRACE(_T("ch: %d"), hwnd);
	

	TCHAR wndowclass[CLASS_SIZE];
	GetWindowText(hwnd,wndowclass,CLASS_SIZE);

	//if (GetWindowText(hwnd,wndowclass,CLASS_SIZE)==0)
	//	return TRUE;
	//m_strMessage = wndowclass;

	CString strTemp(wndowclass);
	//const TCHAR* myCharString = LPCTSTR(strTemp);
	//CString sSwitch("bbbb");
	CString sCase("WMPAppHost");
	
	if(strTemp == sCase)//WMPAppHost
	{
		//found it, send left click button
		//caculate size
		RECT		rect;              // Rectangle area of the found window.
		 // Get the screen coordinates of the rectangle of the found window.
		long x,y;
		GetWindowRect (hwnd, &rect);
		x = (rect.right - rect.left) / 2;
		y = (rect.bottom - rect.top) - 15;
		//CString msg;
		//msg.Format(_T("x: %d, y: %d"),x, y);
		
		
		SendMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x,y));
		SendMessage(hwnd, WM_LBUTTONUP, 0, MAKELPARAM(x,y));
		
		//TRACE(msg);
		//AfxMessageBox(msg);
		//TRACE(_T("xy!"));
		//myfile2.Write(strTemp);
			//myfile2.WriteEndl();
		//myfile2.Close();
		
		//TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Media"), msg, 2000);
		return FALSE;
	}
	//myfile2.Write(strTemp);
			//myfile2.WriteEndl();
			
	return TRUE;
}

void CDailyNoteDlg::ToggleMediaNext()
{
	MyGetMediaApp();
	if(m_hMedia)
	{
		//now find child window
		//CDailyNoteApp *pApp = (CDailyNoteApp*) AfxGetApp();
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Media"), _T("Next!"), 2000);
		EnumChildWindows(m_hMedia, ChildWndProcMediaNext,0);
	}
	else
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Media"), _T("Not found media player!"), 2000);
}

BOOL CALLBACK ChildWndProcMediaNext(HWND hwnd, LPARAM lParam)
{
	//TRACE(_T("ch: %d"), hwnd);
	TCHAR wndowclass[CLASS_SIZE];

	if (GetWindowText(hwnd,wndowclass,CLASS_SIZE)==0)
		return TRUE;
	//m_strMessage = wndowclass;

	CString strTemp(wndowclass);
	//const TCHAR* myCharString = LPCTSTR(strTemp);
	//CString sSwitch("bbbb");
	//CString sCase("WMPAppHost");
	
	if(strTemp == _T("WMPAppHost"))
	{
		//found it, send left click button
		//caculate size
		RECT		rect;              // Rectangle area of the found window.
		 // Get the screen coordinates of the rectangle of the found window.
		long x,y;
		GetWindowRect (hwnd, &rect);
		x = ((rect.right - rect.left) / 2) + 40;
		y = (rect.bottom - rect.top) - 30;
		//CString msg;
		//msg.Format(_T("x: %d, y: %d"),x, y);
		
		
		SendMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x,y));
		SendMessage(hwnd, WM_LBUTTONUP, 0, MAKELPARAM(x,y));

		//SendMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(345,555));
		//SendMessage(hwnd, WM_LBUTTONUP, 0, MAKELPARAM(345,555));
		//TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Media"), _T("Receive msg Play"), 2000);
		return FALSE;
	}
	return TRUE;
}

void CDailyNoteDlg::ShowTotalApp()
{
	MyGetTotalApp();
	if(m_hTotalCommand)
	{
		//now find child window
		//CDailyNoteApp *pApp = (CDailyNoteApp*) AfxGetApp();
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Total App"), _T("Show!"), 2000);
		//SendMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(345,555));
		SetForegroundWindowInternal(m_hTotalCommand);		
		//EnumChildWindows(m_hMedia, ChildWndProcMediaNext,0);
	}
	else
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Total App"), _T("Not found!"), 2000);
}


HWND CDailyNoteDlg::MyGetTotalApp()
{	
	TCHAR		szBuff[100];

	if(m_hTotalCommand == NULL)
	{
		
		m_hTotalCommand = NULL;
		m_hFunctions = NULL;
		if (!EnumWindows((WNDENUMPROC )EnumWindowsProcTotalCommand,0))
		{
			if(m_hTotalCommand != NULL)
			{
			}
		}
	}
	else
	{		
		::GetClassName(m_hTotalCommand, szBuff, sizeof (szBuff) - 1);
		CString currentWnd(szBuff);
		if(currentWnd.Find(_T("TTOTAL_CMD")) == -1)
		{
			m_hTotalCommand = NULL;
			m_hFunctions = NULL;
			if (!EnumWindows((WNDENUMPROC )EnumWindowsProcTotalCommand,0))
			{
				if(m_hTotalCommand != NULL)
				{
				}
			}
		}		
	}

	
	return m_hTotalCommand;
}

void CDailyNoteDlg::ResetForcegroundIEDramas()
{	
	if(!m_hIEDramas)
	{
		MyGetIEDramas();
	}
	if(m_hIEDramas)
	{
		//comment out for version 1.0.3.6
		HWND  hCurrWnd = ::GetForegroundWindow();
		DWORD dwThisTID = ::GetCurrentThreadId(),
			dwCurrTID = ::GetWindowThreadProcessId(hCurrWnd,0);
		
		//we need to bypass some limitations from Microsoft :)
		if(dwThisTID == dwCurrTID)
		{	
			/*
			::SetWindowPos(m_hIEDramas,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
			::AttachThreadInput(dwThisTID, dwCurrTID, TRUE);
			::BringWindowToTop(this->m_hWnd);
			::SetForegroundWindow(this->m_hWnd);
			::AttachThreadInput(dwThisTID, dwCurrTID, FALSE);
			//::AllowSetForegroundWindow(ASFW_ANY);
			*/
		}
		else
			::SetWindowPos(m_hIEDramas,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
		
		//::SetWindowPos(m_hIEDramas,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
		//TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("xonefm"), _T("Refresh"), 1000);
	}
	
}
void CDailyNoteDlg::TogleIEDramas()
{
	MyGetIEDramas();
	if(m_hIEDramas)
	{
		if(m_bIsOnTopDramas)
		{
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("xonefm"), _T("OFF"), 2000);
			//m_bIsOnTopDramas = FALSE;
			
			LONG lStyle = GetWindowLong(m_hIEDramas, GWL_STYLE);
			lStyle |= (WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
			SetWindowLong(m_hIEDramas,GWL_STYLE,lStyle);
			
			LONG lExStyle = GetWindowLong(m_hIEDramas, GWL_EXSTYLE);
			lExStyle |= (WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
			SetWindowLong(m_hIEDramas, GWL_EXSTYLE, lExStyle);

			::SetWindowPos(m_hIEDramas, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
						
			::SetWindowPos(m_hIEDramas,HWND_NOTOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
		}
		else
		{
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("xonefm"), _T("ON"), 2000);
			//m_bIsOnTopDramas = TRUE;
			
			SetForegroundWindowInternal(m_hIEDramas);
			LONG lStyle = GetWindowLong(m_hIEDramas, GWL_STYLE);
			lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
			SetWindowLong(m_hIEDramas,GWL_STYLE,lStyle);
			
			LONG lExStyle = GetWindowLong(m_hIEDramas, GWL_EXSTYLE);
			lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
			SetWindowLong(m_hIEDramas, GWL_EXSTYLE, lExStyle);

			::SetWindowPos(m_hIEDramas, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
			
			::SetWindowPos(m_hIEDramas,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
		}
		
		//m_bIsOnTopDramas = !m_bIsOnTopDramas;
		HWND hWndInsertAfter = ( m_bIsOnTopDramas ? HWND_TOPMOST : HWND_NOTOPMOST );
		::SetWindowPos( m_hIEDramas, hWndInsertAfter, 0, 0 , 0 , 0, SWP_NOMOVE | SWP_NOSIZE );
		
	}
	else
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("xonefm"), _T("Not found!"), 2000);
}


HWND CDailyNoteDlg::MyGetIEDramas()
{	
	TCHAR		szBuff[500];

	if(m_hIEDramas == NULL)
	{
		
		m_hIEDramas = NULL;
		//m_hFunctions = NULL;
		if (!EnumWindows((WNDENUMPROC )EnumWindowsProcIEDramas,0))
		{
			if(m_hIEDramas != NULL)
			{
			}
		}
	}
	else
	{		
		::GetWindowText(m_hIEDramas, szBuff, sizeof (szBuff) - 1);
		CString currentWnd(szBuff);
		if(currentWnd.Find(_T("xonefm.com")) == -1)
		{
			m_hIEDramas = NULL;
			//m_hFunctions = NULL;
			if (!EnumWindows((WNDENUMPROC )EnumWindowsProcIEDramas,0))
			{
				if(m_hIEDramas != NULL)
				{
				}
			}
		}		
	}

	
	return m_hIEDramas;
}

BOOL CALLBACK EnumWindowsProcIEDramas(HWND hWnd, LPARAM lParam)
{
	TCHAR buff[500];
	int buffsize=500;
	//HWND hYahooWnd;
	//hYahooWnd=NULL;
		
	//::GetWindowText(hwnd,buff,buffsize);
	::GetWindowText(hWnd, buff, sizeof (buff) - 1);
	if (_tcslen(buff)<1)
		return TRUE;
	

	CString strTemp(buff);

	//if (_tcslen(buff)>50)
	//	AfxMessageBox(strTemp);

	//CHECK for Yahoo MESSENGER CHAT WINDOW
	string::size_type pos=0;
	//int pos=0;
	//pos=strTemp.rfind("-- Instant Message",strTemp.length());
	//pos=strTemp.Find(_T("anhhuycan"));
	pos=strTemp.Find(_T("xonefm.com"));
	//pos=strTemp.rfind("nhutnhut",strTemp.length());

	
	//pos=strTemp.rfind(" @",strTemp.length());
	if (pos!=string::npos)
	//if (pos!=-1)
	{
		//char buf[20];
		//AfxMessageBox(_itoa((int) pos, buf, 10));		
		m_hIEDramas = hWnd;		
		//m_countWindow = 0;
		//EnumChildWindows(m_hTotalCommand,ChildWndProcTotalCommand,0);
		return FALSE;

	}
	return TRUE;
}

// for firefox

void CDailyNoteDlg::ResetForcegroundFFDramas()
{	
	if(!m_hFFDramas)
	{
		MyGetFFDramas();
	}
	if(m_hFFDramas)
	{
		//comment out for version 1.0.3.6
		HWND  hCurrWnd = ::GetForegroundWindow();
		DWORD dwThisTID = ::GetCurrentThreadId(),
			dwCurrTID = ::GetWindowThreadProcessId(hCurrWnd,0);
		
		//we need to bypass some limitations from Microsoft :)
		if(dwThisTID == dwCurrTID)
		{	
			/*
			::SetWindowPos(m_hFFDramas,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
			::AttachThreadInput(dwThisTID, dwCurrTID, TRUE);
			::BringWindowToTop(this->m_hWnd);
			::SetForegroundWindow(this->m_hWnd);
			::AttachThreadInput(dwThisTID, dwCurrTID, FALSE);
			//::AllowSetForegroundWindow(ASFW_ANY);
			*/
		}
		else
			::SetWindowPos(m_hFFDramas,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
		
		//::SetWindowPos(m_hFFDramas,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
		//TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("xonefm"), _T("Refresh"), 1000);
	}
	
}

void CDailyNoteDlg::TogleFFDramas()
{
	MyGetFFDramas();
	if(m_hFFDramas)
	{
		if(m_bIsOnTopDramas)
		{
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("dra ff"), _T("OFF"), 2000);
			//m_bIsOnTopDramas = FALSE;
			
			LONG lStyle = GetWindowLong(m_hFFDramas, GWL_STYLE);
			lStyle |= (WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
			SetWindowLong(m_hFFDramas,GWL_STYLE,lStyle);
			
			LONG lExStyle = GetWindowLong(m_hFFDramas, GWL_EXSTYLE);
			lExStyle |= (WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
			SetWindowLong(m_hFFDramas, GWL_EXSTYLE, lExStyle);

			::SetWindowPos(m_hFFDramas, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
						
			::SetWindowPos(m_hFFDramas,HWND_NOTOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
		}
		else
		{
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("dra ff"), _T("ON"), 2000);
			//m_bIsOnTopDramas = TRUE;
			
			SetForegroundWindowInternal(m_hFFDramas);
			LONG lStyle = GetWindowLong(m_hFFDramas, GWL_STYLE);
			lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
			SetWindowLong(m_hFFDramas,GWL_STYLE,lStyle);
			
			LONG lExStyle = GetWindowLong(m_hFFDramas, GWL_EXSTYLE);
			lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
			SetWindowLong(m_hFFDramas, GWL_EXSTYLE, lExStyle);

			::SetWindowPos(m_hFFDramas, NULL, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
			
			::SetWindowPos(m_hFFDramas,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
		}
		
		//m_bIsOnTopDramas = !m_bIsOnTopDramas;
		HWND hWndInsertAfter = ( m_bIsOnTopDramas ? HWND_TOPMOST : HWND_NOTOPMOST );
		::SetWindowPos( m_hFFDramas, hWndInsertAfter, 0, 0 , 0 , 0, SWP_NOMOVE | SWP_NOSIZE );
		
	}
	else
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("dra ff"), _T("Not found!"), 2000);
}


HWND CDailyNoteDlg::MyGetFFDramas()//IE
{	
	TCHAR		szBuff[500];

	if(m_hFFDramas == NULL)
	{
		
		m_hFFDramas = NULL;
		//m_hFunctions = NULL;
		if (!EnumWindows((WNDENUMPROC )EnumWindowsProcFFDramas,0))
		{
			if(m_hFFDramas != NULL)
			{
			}
		}
	}
	else
	{		
		::GetWindowText(m_hFFDramas, szBuff, sizeof (szBuff) - 1);
		CString currentWnd(szBuff);
		if(currentWnd.Find(_T("Mozilla Firefox")) == -1)
		{
			m_hTotalCommand = NULL;
			//m_hFunctions = NULL;
			if (!EnumWindows((WNDENUMPROC )EnumWindowsProcFFDramas,0))
			{
				if(m_hFFDramas != NULL)
				{
				}
			}
		}		
	}

	
	return m_hFFDramas;
}

BOOL CALLBACK EnumWindowsProcFFDramas(HWND hWnd, LPARAM lParam)
{
	TCHAR buff[500];
	int buffsize=500;
	//HWND hYahooWnd;
	//hYahooWnd=NULL;
		
	//::GetWindowText(hwnd,buff,buffsize);
	::GetWindowText(hWnd, buff, sizeof (buff) - 1);
	if (_tcslen(buff)<1)
		return TRUE;
	

	CString strTemp(buff);

	//if (_tcslen(buff)>50)
	//	AfxMessageBox(strTemp);

	//CHECK for Yahoo MESSENGER CHAT WINDOW
	string::size_type pos=0;
	//int pos=0;
	//pos=strTemp.rfind("-- Instant Message",strTemp.length());
	//pos=strTemp.Find(_T("anhhuycan"));
	pos=strTemp.Find(_T("Mozilla Firefox"));
	//pos=strTemp.rfind("nhutnhut",strTemp.length());

	
	//pos=strTemp.rfind(" @",strTemp.length());
	if (pos!=string::npos)
	//if (pos!=-1)
	{
		//char buf[20];
		//AfxMessageBox(_itoa((int) pos, buf, 10));		
		m_hFFDramas = hWnd;		
		//m_countWindow = 0;
		//EnumChildWindows(m_hTotalCommand,ChildWndProcTotalCommand,0);
		return FALSE;

	}
	return TRUE;
}
/////////////

void CDailyNoteDlg::ResizeRemoteDesktopApp(CString strRemoteDesktopCaption)
{
	MyGetRemoteDesktopApp(strRemoteDesktopCaption);
	if(m_hRemoteDesktop)
	{
		//now find child window
		//CDailyNoteApp *pApp = (CDailyNoteApp*) AfxGetApp();
		
		//SendMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(345,555));
		int x,y,cx,cy;
		x = -4;
		y = -25;
		//x = 4;
		//y = 30;
		if(::IsIconic(m_hRemoteDesktop))
			::ShowWindow(m_hRemoteDesktop, SW_RESTORE);
		else
			::ShowWindow(m_hRemoteDesktop, SW_SHOW);

		cx = 1288;
		cy = 1058;
		RECT		rect;              // Rectangle area of the found window.
		 // Get the screen coordinates of the rectangle of the found window.		
		::GetWindowRect(m_hRemoteDesktop, &rect);

		if(rect.left != x)
		{
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Resized"), strRemoteDesktopCaption, 2000);
			::SetWindowPos(m_hRemoteDesktop, HWND_NOTOPMOST, x, y, cx, cy, SWP_SHOWWINDOW);
			::UpdateWindow(m_hRemoteDesktop);
		}
		else
			TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Already resized!"), strRemoteDesktopCaption, 2000);
	}
	else
		TrayMessageShowBalloon(NIM_MODIFY, m_strMsgNotify, _T("Not found!"), strRemoteDesktopCaption, 2000);
}


HWND CDailyNoteDlg::MyGetRemoteDesktopApp(CString strRemoteDesktopCaption)
{
	//HWND hHelper = null;
	
	TCHAR		szBuff[100];

	if(m_hRemoteDesktop == NULL)
	{
		m_hRemoteDesktop = ::FindWindow(NULL, strRemoteDesktopCaption);
	}
	else
	{
		::GetWindowText(m_hRemoteDesktop, szBuff, sizeof (szBuff) - 1);		
		if(szBuff != strRemoteDesktopCaption)
		{			
			m_hRemoteDesktop = ::FindWindow(NULL, strRemoteDesktopCaption);
		}		
	}

	return m_hRemoteDesktop;
}


void SetForegroundWindowInternal(HWND hWnd)
{	
	if(!::IsWindow(hWnd)) return;
	
	//relation time of SetForegroundWindow lock
	DWORD lockTimeOut = 0;
	HWND  hCurrWnd = ::GetForegroundWindow();
	DWORD dwThisTID = ::GetCurrentThreadId(),
		dwCurrTID = ::GetWindowThreadProcessId(hCurrWnd,0);
	
	//we need to bypass some limitations from Microsoft :)
	if(dwThisTID != dwCurrTID)
	{
		::AttachThreadInput(dwThisTID, dwCurrTID, TRUE);
		::BringWindowToTop(hWnd);
		::SetForegroundWindow(hWnd);
		::AttachThreadInput(dwThisTID, dwCurrTID, FALSE);
		
		//::AllowSetForegroundWindow(ASFW_ANY);
	}

	if (::GetForegroundWindow() != hWnd)
	{
		// Code by Daniel P. Stasinski
		// Converted to C# by Kevin Gale                
		::SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT,0,&lockTimeOut,0);
		::SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT,0,0,SPIF_SENDWININICHANGE | SPIF_UPDATEINIFILE);
		::BringWindowToTop(hWnd); // IE 5.5 related hack
		::SetForegroundWindow(hWnd);
		::SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT,0,(PVOID)lockTimeOut,SPIF_SENDWININICHANGE | SPIF_UPDATEINIFILE);
	}
}

bool CDailyNoteDlg::IsWindowsVistaOrLater()
{
	OSVERSIONINFO osvi;
    //bool bIsWindowsVistaOrLater;
	
    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	
    GetVersionEx(&osvi);
	
    //bIsWindowsVistaOrLater = (osvi.dwMajorVersion == 6) && (osvi.dwMinorVersion >= 0));
	bIsWindowsVistaOrLater = (osvi.dwMajorVersion > 5);
	
    return bIsWindowsVistaOrLater;
	
}

/*
Version Number    Description
6.1               Windows 7     / Windows 2008 R2
6.0               Windows Vista / Windows 2008
5.2               Windows 2003 
5.1               Windows XP
5.0               Windows 2000
*/

//this function is to control the while(1) loop of thread ML
//so it reverse the status of loop.
//ex: current stop -> create thread
// current pause => run again
void CDailyNoteDlg::MyStartThreadMailSlot()
{
	switch(m_myStatusMailSlot)
	{
		case MYSTOP:
			m_pTimerThreadML = AfxBeginThread(MyThreadMailSlot, this, THREAD_PRIORITY_NORMAL, 0,
				CREATE_SUSPENDED);
			m_pTimerThreadML->m_bAutoDelete = false;				
			m_myStatusMailSlot = MYRUN;			
			m_pTimerThreadML->ResumeThread();	
			break;
		case MYPAUSE:	
			m_myStatusMailSlot = MYRUN;
			break;
		case MYRUN:
			m_myStatusMailSlot = MYPAUSE;
			break;
		default:
			break;
	}
}

//this loop only depends on the status of status enum variable
UINT CDailyNoteDlg::MyThreadMailSlot(LPVOID pParam)
{
	CDailyNoteDlg *pSubDlg = (CDailyNoteDlg*)pParam;
	//pSubDlg->m_tsTimer = CHighTimeSpan(0,0,0,0,0,0,0);	
	while(1)
	{
		switch(pSubDlg->m_myStatusMailSlot)
		{
			case MYSTOP:
				return 0;
				break;
			case MYPAUSE:				
				break;
			case MYRUN:
				//pSubDlg->m_tsTimer += tsTemp;
				//_ultow(pSubDlg->m_tsTimer.GetTotalMilliSeconds(), m_txtSecondValue1, 10);
				//pSubDlg->MySetTxtSecond1();
				pSubDlg->PostMessage(MYWM_READMAILSLOT, 0, 0);
				
				//pSubDlg->UpdateData(false);
				break;
			default:
				break;
		}
		//comment out for version 1.0.2.9
		::Sleep(600);
	}

	return 0;
}
void CDailyNoteDlg::MyStartThreadMessageBox(CString sMsg)
{
	//LPCSTR szTemp = (LPCSTR)(LPCTSTR)sMsg;
	LPCTSTR szTemp = (LPCTSTR)sMsg;
	m_pMessageBoxThread = AfxBeginThread(MyThreadMessageBox, (LPVOID) szTemp, THREAD_PRIORITY_NORMAL, 0,
				CREATE_SUSPENDED);
			m_pMessageBoxThread->m_bAutoDelete = false;
			m_pMessageBoxThread->ResumeThread();
}

//this loop only depends on the status of status enum variable
UINT CDailyNoteDlg::MyThreadMessageBox(LPVOID pParam)
{
	CString t = (LPCTSTR)pParam;
	//CString* t = static_cast<CString*>(pParam);
	//CString* t = (CString*)(pParam);
	//CString msg(t);
	::MessageBox(NULL,
									t,
									_T("Alert"),
									MB_ICONINFORMATION | MB_OK | MB_SETFOREGROUND | MB_TOPMOST);
	//delete t;
	return 0;
}
void CDailyNoteDlg::MyStartThreadDramas()
{
	switch(m_myStatusDramas)
	{
		case MYSTOP:
			m_pTimerThreadDramas = AfxBeginThread(MyThreadDramas, this, THREAD_PRIORITY_NORMAL, 0,
				CREATE_SUSPENDED);
			m_pTimerThreadDramas->m_bAutoDelete = false;				
			m_myStatusDramas = MYRUN;			
			m_pTimerThreadDramas->ResumeThread();	
			break;
		case MYPAUSE:	
			m_myStatusDramas = MYRUN;
			break;
		case MYRUN:
			m_myStatusDramas = MYPAUSE;
			break;
		default:
			break;
	}
}

//this loop only depends on the status of status enum variable
UINT CDailyNoteDlg::MyThreadDramas(LPVOID pParam)
{
	CDailyNoteDlg *pSubDlg = (CDailyNoteDlg*)pParam;
	//pSubDlg->m_tsTimer = CHighTimeSpan(0,0,0,0,0,0,0);	
	while(1)
	{
		switch(pSubDlg->m_myStatusDramas)
		{
			case MYSTOP:
				return 0;
				break;
			case MYPAUSE:				
				break;
			case MYRUN:
				//pSubDlg->m_tsTimer += tsTemp;
				//_ultow(pSubDlg->m_tsTimer.GetTotalMilliSeconds(), m_txtSecondValue1, 10);
				//pSubDlg->MySetTxtSecond1();
				if(pSubDlg->m_bIsOnTopDramas)		
					pSubDlg->PostMessage(MYWM_FORCEGROUND_FF_DRAMAS, 0, 0);
				
				//pSubDlg->UpdateData(false);
				break;
			default:
				break;
		}
		//comment out for version 1.0.2.9
		::Sleep(10000);
	}

	return 0;
}
void CDailyNoteDlg::myShowDlg(bool ins_isShow, int flag)
{
	if(ins_isShow == true)
	{
		m_visible = true;
		//TrayMessage(NIM_DELETE); //comment out for version 1.0.2.4
		ShowWindow(flag);//SW_NORMAL  SW_HIDE  
		//move from tray area to desktop
	}
	else
	{
		m_visible = false;
		//TrayMessage(NIM_ADD);//comment out for version 1.0.2.4
		//myResetSystrayIconStatus(isRunning);
		ShowWindow(SW_HIDE);//SW_NORMAL
		//move from desktop to tray area
	}
}

bool CDailyNoteDlg::IsFileExist(CString fullFileName, bool &vCanAccess)
{
	CFileStatus rStatus;
	bool bIsFileExist;
	if(CFile::GetStatus(fullFileName, rStatus) == 0)
		bIsFileExist = false;
	else
		bIsFileExist = true;
	return bIsFileExist;
}

void CDailyNoteDlg::FixShowTrayIcon()
{
	bool bVisibleTemp;
	bVisibleTemp = m_visible;
	m_visible = true;
//MYDEL	if(::IsIconic(m_hRemoteDesktop))
//MYDEL		::ShowWindow(m_hRemoteDesktop, SW_RESTORE);
//MYDEL	else
//MYDEL		::ShowWindow(m_hRemoteDesktop, SW_SHOW);
	
	RECT		rect;              // Rectangle area of the found window.
	// Get the screen coordinates of the rectangle of the found window.		
	//rectTemp.top = 0;
	::GetWindowRect(this->m_hWnd, &rect);
	TRACE(_T("%d, %d, %d, %d"), rect.top, rect.left, rect.right, rect.bottom);
	SetWindowPos(NULL, 0, 0, 0, 0, ~SWP_SHOWWINDOW);
	//SetWindowPos(NULL, 0, 0, 1, 1, SWP_SHOWWINDOW);
	m_visible = false;
	::SetWindowPos(this->m_hWnd, HWND_NOTOPMOST, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);
	//SetWindowPos(this->m_hWnd, 0, 0, 0, 0, HWND_BOTTOM);
	//m_visible = false;
	m_visible = bVisibleTemp;
}

bool CDailyNoteDlg::FixF2TotalCommand()
{
	bool rs = false;
	CTabMessage *pDlgNetSend = (CTabMessage *)(m_tbCtrl.m_Dialog[3]);
	
	if(pDlgNetSend->m_localName.Find(_T("TUYEN")) == -1 )
	{
//MYDEL		CString t;
//MYDEL		t.Format(_T("%s"), pDlgNetSend->m_localName);
//MYDEL		MessageBox(t);
		return false;
	}
	
	HWND hWnd = ::GetForegroundWindow();
	RECT		rect;              // Rectangle area of the found window.
	TCHAR		szBuff[100];
	long lRet = 0;
	::GetWindowRect (hWnd, &rect);
	lRet = ::GetClassName(hWnd, szBuff, sizeof (szBuff) - 1);
	CString clsName(szBuff);
	if(clsName.Find(_T("TTOTAL_CMD")) >= 0)
	{
		//
		m_hFunctions = NULL;
		m_countWindow = 0;
		EnumChildWindows(hWnd,ChildWndProcTotalCommand,0);
		if(m_hFunctions != NULL)
		{
			LPARAM lParam;
			int nMessageDown, nMessageUp;
			int wparam = MK_LBUTTON;
			wparam = MK_LBUTTON;
			nMessageDown = WM_LBUTTONDOWN;
			nMessageUp = WM_LBUTTONUP;
			lParam = MAKELPARAM(820, 40);
			::PostMessage(m_hFunctions, nMessageDown, wparam, lParam);
			::PostMessage(m_hFunctions, nMessageUp, wparam, lParam);
			rs = true;
		}
		//clsName.Format(_T("%d"), m_countWindow);
		//MessageBox(clsName);
		return true;
	}
	//MessageBox(_T("notfound"));
	return rs;
	//GetWindowText(hWnd, szBuff, sizeof (szBuff) - 1);
}


BOOL CALLBACK EnumWindowsProcTotalCommand(HWND hWnd, LPARAM lParam)
{
	TCHAR buff[500];
	int buffsize=500;
	//HWND hYahooWnd;
	//hYahooWnd=NULL;
		
	//::GetWindowText(hwnd,buff,buffsize);
	::GetClassName(hWnd, buff, sizeof (buff) - 1);
	if (_tcslen(buff)<1)
		return TRUE;

	CString strTemp(buff);

	//CHECK for Yahoo MESSENGER CHAT WINDOW
	string::size_type pos=0;
	//int pos=0;
	//pos=strTemp.rfind("-- Instant Message",strTemp.length());
	//pos=strTemp.Find(_T("anhhuycan"));
	pos=strTemp.Find(_T("TTOTAL_CMD"));
	//pos=strTemp.rfind("nhutnhut",strTemp.length());

	
	//pos=strTemp.rfind(" @",strTemp.length());
	if (pos!=string::npos)
	//if (pos!=-1)
	{
		//char buf[20];
		//AfxMessageBox(_itoa((int) pos, buf, 10));		
		m_hTotalCommand = hWnd;		
		//m_countWindow = 0;
		//EnumChildWindows(m_hTotalCommand,ChildWndProcTotalCommand,0);
		return FALSE;

	}
	return TRUE;
}

BOOL CALLBACK ChildWndProcTotalCommand(HWND hWnd, LPARAM lParam)
{
	/*
	TCHAR wndowclass[CLASS_SIZE];

	if (GetClassName(hwnd,wndowclass,CLASS_SIZE)==0)
		return TRUE;
		*/
	TCHAR buff[500];
	string::size_type pos=0;
	//::GetWindowText(hwnd,buff,buffsize);
	::GetClassName(hWnd, buff, sizeof (buff) - 1);	

//MYDEL	if (_tcslen(buff)<5)
//MYDEL		return TRUE;
	//m_strMessage = wndowclass;

	CString strTemp(buff);
	pos=strTemp.Find(_T("Window"));//TMyPanel
	//const TCHAR* myCharString = LPCTSTR(strTemp);
	//CString sSwitch("bbbb");
	if (pos!=string::npos)	
	{
		m_countWindow++;
		if(m_countWindow == 4)
		{
			m_hFunctions = hWnd;
			return FALSE;
		}
	}
	
	return TRUE;
}

HWND CDailyNoteDlg::MyGetVideoMonitorClient()
{
	EnumWindows((WNDENUMPROC )EnumWindowsProcVideoMonitorClient,0);

	/*
	TCHAR		szBuff[100];

	if(m_hBtnVideoMonitorClient == NULL)
	{
		m_hBtnVideoMonitorClientRecordMam1 = NULL;
		if (!EnumWindows((WNDENUMPROC )EnumWindowsProcVideoMonitorClient,0))
		{
			
		}
	}
	else
	{		
		::GetWindowText(m_hBtnVideoMonitorClient,szBuff,sizeof (szBuff) - 1);//caption

		//::GetClassName(m_hSkypeGroupTCGroup, szBuff, sizeof (szBuff) - 1);
		CString currentWnd(szBuff);
		if(currentWnd.Find(_T("iVMS-4200 Lite")) == -1)
		{
			m_hBtnVideoMonitorClient = NULL;
			MyGetVideoMonitorClient();
			/*
			m_hBtnVideoMonitorClientRecordMam1 = NULL;
			if (!EnumWindows((WNDENUMPROC )ChildWndProcVideoMonitorClient,0))
			{
				if(m_hSkypeGroupRichTextBoxTCGroup != NULL)
				{
				}
			}
			
		}		
	}
*/
	
	return m_hBtnVideoMonitorClient;
}

BOOL CALLBACK EnumWindowsProcVideoMonitorClient(HWND hWnd, LPARAM lParam)
{
	TCHAR buff[500];
	int buffsize=500;

	::GetWindowText(hWnd,buff,sizeof (buff) - 1);

	int sLen = _tcslen(buff);
	if (sLen!=14 && sLen!=9)
		return TRUE;

	CString strTemp(buff);

	//CHECK for Yahoo MESSENGER CHAT WINDOW
	string::size_type pos=0;
	//pos=strTemp.Find(_T("Video Monitor Client"));
	pos=strTemp.Find(_T("iVMS-4200 Lite"));
	//pos=strTemp.rfind("nhutnhut",strTemp.length());
	//pos=strTemp.rfind(" @",strTemp.length());
	if (pos!=string::npos)
	{
		::GetClassName(hWnd, buff, sizeof (buff) - 1);
		CString strTempClassName(buff);
		pos=strTempClassName.Find(_T("Qt5QWindowIcon"));
		if (pos!=string::npos)	
		{
			m_hBtnVideoMonitorClient = hWnd;		
			EnumChildWindows(m_hBtnVideoMonitorClient,ChildWndProcVideoMonitorClient,0);
			return FALSE;
		}	
	}
	else
	{
		//CHECK for Yahoo MESSENGER CHAT WINDOW
		string::size_type pos=0;
		//pos=strTemp.Find(_T("Video Monitor Client"));
		pos=strTemp.Find(_T("iVMS-4200"));//iVMS-4200
		//pos=strTemp.rfind("nhutnhut",strTemp.length());
		//pos=strTemp.rfind(" @",strTemp.length());
		if (pos!=string::npos)
		{
			::GetClassName(hWnd, buff, sizeof (buff) - 1);
			CString strTempClassName(buff);
			pos=strTempClassName.Find(_T("Qt5QWindowIcon"));
			if (pos!=string::npos)	
			{
				m_hBtnVideoMonitorClient = hWnd;		
				EnumChildWindows(m_hBtnVideoMonitorClient,ChildWndProcVideoMonitorClient,0);
				return FALSE;
			}	
		}
	}
	return TRUE;
}

BOOL CALLBACK ChildWndProcVideoMonitorClient(HWND hWnd, LPARAM lParam)
{
	TCHAR buff[500];
	string::size_type pos=0;
	::GetWindowText(hWnd,buff,sizeof (buff) - 1);//caption
	//::GetClassName(hWnd, buff, sizeof (buff) - 1);	
	int sLen = _tcslen(buff);
	if (sLen!=63)
		return TRUE;

	
	CString strTempCaption(buff);
	pos=strTempCaption.Find(_T("iVMSPlayWindow::CStreamPlayStateBarSupportTimeSliderClassWindow"));
	if (pos!=string::npos)	
	{
		::GetClassName(hWnd, buff, sizeof (buff) - 1);
		CString strTempClassName(buff);
		pos=strTempClassName.Find(_T("Qt5QWindowIcon"));
		if (pos!=string::npos)	
		{
			//check rect size
			RECT		rect;              // Rectangle area of the found window.
			// Get the screen coordinates of the rectangle of the found window.
			long x,y;
			::GetWindowRect(hWnd, &rect);
			x = (rect.right - rect.left);//width
			y = (rect.bottom - rect.top);//heigh
			if(x==199 && y == 26)
			{	
				m_hBtnVideoMonitorClientRecordMam1 = hWnd;
				m_windowSize = 0;
				MyVideoMonitorClientClick(hWnd, false);				
			}
			else
			{
				if(x==400 && y == 26)
				{	
					m_hBtnVideoMonitorClientRecordMam1 = hWnd;
					m_windowSize = 1;
					MyVideoMonitorClientClick(hWnd, false);
				}
			}
			//return FALSE;
		}
	}
	return TRUE;
}

void MyVideoMonitorClientClick(HWND hwnd, bool bIsForgeGround)
{
	//HWND hwnd = m_hBtnVideoMonitorClientRecordMam1;
	if(hwnd != NULL)
	{
		//set forceground
		if(bIsForgeGround)
			SetForegroundWindowInternal(hwnd);

		RECT		rect;              // Rectangle area of the found window.
		 // Get the screen coordinates of the rectangle of the found window.
		long x,y;
		int addLeft = 50;
		::GetWindowRect(hwnd, &rect);
		if(m_windowSize == 1)
			addLeft = 150;

		x = ((rect.right - rect.left) / 2) + addLeft;
		y = (rect.bottom - rect.top) / 2;
		//CString msg;
		//msg.Format(_T("x: %d, y: %d"),x, y);
		
		
		::SendMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x,y));
		::SendMessage(hwnd, WM_LBUTTONUP, 0, MAKELPARAM(x,y));
	}
}

bool CDailyNoteDlg::MyProccessContainMessage(CString sMsgContain)
{
	bool bIsProccessed = false;
	string::size_type pos=0;
	
	CStringArray arrStr;
	CString commandName = _T("");
	m_bIsForgeGround = false;
	
	//msg array
	StringSplit(sMsgContain, arrStr, _T(';'));
	
	switch(arrStr.GetSize())
	{
	case 2:
		commandName = arrStr.GetAt(0);
		m_bIsForgeGround = (arrStr.GetAt(0) == _T("true") ? true:false);
		break;		
	case 1:
		commandName = arrStr.GetAt(0);
		break;
	default:
		break;
	}
	
	if(commandName.GetLength()>0)
	{
		pos=sMsgContain.Find(MY_VIDEO_MONITOR_CLIENT_CLICK);
		if (pos!=string::npos)							
		{
			MyGetVideoMonitorClient();			
			//MyVideoMonitorClientClick(bIsForgeGround, m_hBtnVideoMonitorClientRecordMam1);
			bIsProccessed = true;
		}
	}
	return bIsProccessed;
}