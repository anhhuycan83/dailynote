#if !defined(AFX_MAINTAB_H__D5D268C2_BE8D_4887_A99E_35E099D8F173__INCLUDED_)
#define AFX_MAINTAB_H__D5D268C2_BE8D_4887_A99E_35E099D8F173__INCLUDED_

#include "RecordNoteDlg.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MAINTAB.h : header file
//

/////////////////////////////////////////////////////////////////////////////
#include "ReportCtrl.h"
#include "RecordNoteDlg.h"
#include "DailyNoteDlg.h"

// CMAINTAB dialog

class CMAINTAB : public CDialog
{
// Construction
public:
	void MySetStatusBtn();
	void MyRefreshListReport(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote);
	void MyRefreshSTT();
	CRecordNoteDlg m_dlgRecordNote;
//	CRecordNoteDlg *m_pRecordNoteDlg;
	CMAINTAB(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMAINTAB)
	enum { IDD = IDD_DIALOG1 };
	CButton	m_btnEdit;
	CButton	m_btnDelete;
	CReportCtrl	m_wndList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMAINTAB)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL
private:
	//CFont m_font;
// Implementation
protected:
	void _StringSplit(const CString &str, CStringArray &arr, TCHAR chDelimitior);

	// Generated message map functions
	//{{AFX_MSG(CMAINTAB)
	virtual BOOL OnInitDialog();
	afx_msg void OnbtnAdd();
	afx_msg void OnbtnEdit();
	afx_msg void OnbtnDelete();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINTAB_H__D5D268C2_BE8D_4887_A99E_35E099D8F173__INCLUDED_)
