// OptionTab.cpp : implementation file
//

#include "stdafx.h"
#include "DailyNote.h"
#include "OptionTab.h"
#include "WinStartup.h"
#include "textfile.h"
#include "DailyNoteDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionTab dialog


COptionTab::COptionTab(CWnd* pParent /*=NULL*/)
	: CDialog(COptionTab::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptionTab)
	m_txtFileStore = _T("");
	//}}AFX_DATA_INIT
}


void COptionTab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionTab)
	DDX_Control(pDX, chbFFDramaOnTop, m_chbFFDramaOnTop);
	DDX_Control(pDX, chbIEDramaOnTop, m_chbIEDramaOnTop);
	DDX_Control(pDX, chbNSFullMode, m_chbNSFullMode);
	DDX_Control(pDX, chbNSFlashWindow, m_chbNSFlashWindow);
	DDX_Control(pDX, rbF11, m_rbF11Ctrl);
	DDX_Control(pDX, rbF10, m_rbF10Ctrl);
	DDX_Text(pDX, txtFileStore, m_txtFileStore);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionTab, CDialog)
	//{{AFX_MSG_MAP(COptionTab)
	ON_BN_CLICKED(chbStartup, OnchbStartup)
	ON_BN_CLICKED(btnSave, OnbtnSave)
	ON_BN_CLICKED(btnDefault, OnbtnDefault)
	ON_BN_CLICKED(rbF10, OnrbF10)
	ON_BN_CLICKED(rbF11, OnrbF11)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionTab message handlers

void COptionTab::SetFileStorePath(CString ins_fullPath)
{
	ins_fullPath = m_txtFileStore;
	UpdateData(false);	
}

CString COptionTab::GetFileStorePath()
{
	UpdateData(true);
	return m_txtFileStore;
}

BOOL COptionTab::OnInitDialog() 
{
	CDialog::OnInitDialog();	

	//set font to main dialog font
	CDailyNoteDlg* parentWnd = ((CDailyNoteDlg*)GetParent());
	SendMessageToDescendants(WM_SETFONT,
		(WPARAM)parentWnd->GetFont()->m_hObject,
		MAKELONG(FALSE, 0), 
		FALSE);

	//init check box start when system startup
	StartupUser user;
	user = CurrentUser;
	//AfxGetApp()->m_hInstance, _T("Startup ArcKey"), user);
	if(CWinStartup::isAppAdded(AfxGetApp()->m_hInstance, _T(ONCE_INSTANCE), user) == true)
	{
		//m_chbStartupCtrl.SetCheck(true);
		//UpdateData(false);
		CheckDlgButton(chbStartup, TRUE);
	}
	else
	{
		CheckDlgButton(chbStartup, FALSE);
	}
	
	//load config file
	loadConfigValue();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COptionTab::OnchbStartup() 
{
	// TODO: Add your control notification handler code here
	StartupUser user;
	user = CurrentUser;

	if(IsDlgButtonChecked(chbStartup))
	{
		CWinStartup::AddApp(AfxGetApp()->m_hInstance, _T(ONCE_INSTANCE), user);
	}
	else
	{
		CWinStartup::RemoveApp(_T(ONCE_INSTANCE), user);
	}
}

void COptionTab::loadConfigValue()
{
	CFileFind finder;	
	CString FullPath;
	FullPath = GetProgramDir();
	FullPath += _T("\\config.ini");

	free((void*)AfxGetApp()->m_pszProfileName);
	AfxGetApp()->m_pszProfileName = _tcsdup(FullPath);

	if(!finder.FindFile(FullPath))
	{
		createConfigFile(FullPath);		
	}
	setConfigValue(false);
}

void COptionTab::createConfigFile(CString ins_FullPath)
{
	//create file
	CTextFileWrite fwrite(ins_FullPath, CTextFileWrite::UTF_8);
	ASSERT(fwrite.IsOpen());
	fwrite.Close();

	//write default value:
	//FILESTORE
	AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt"));
	//FILESOUND
	AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("FILESOUND"), _T("C:\\soud.wav"));
	//HOTKEY
	AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("HOTKEY"), _T("F10"));
}

CString COptionTab::GetProgramDir()
{
	CString RtnVal;
    wchar_t    FileName[MAX_PATH];
	//CString FileName;
    GetModuleFileName(AfxGetInstanceHandle(), FileName, MAX_PATH);
    RtnVal = FileName;
    RtnVal = RtnVal.Left(RtnVal.ReverseFind('\\'));
    return RtnVal;
}

void COptionTab::setConfigValue(bool ins_getSet)
{
	if(ins_getSet) //set to config file
	{
		CString strTemp;
		//FILESTORE
		GetDlgItemText(txtFileStore, strTemp);
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("FILESOTER"), strTemp);

		//FILESOUND
		GetDlgItemText(txtFileStore, strTemp);
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("FILESOUND"), strTemp);

		//HOTKEY
		if(IsDlgButtonChecked(rbF10))	
		{
			strTemp = _T("F10");
		}
		else
		{
			strTemp = _T("F11");
		}

		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("HOTKEY"), strTemp);

		//NETSENDFLASH
		if(m_chbNSFlashWindow.GetCheck())	
		{
			strTemp = _T("1");
		}
		else
		{
			strTemp = _T("0");
		}
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("NETSENDFLASH"), strTemp);
		//NETSENDFULLMODE
		if(m_chbNSFullMode.GetCheck())	
		{
			strTemp = _T("1");
		}
		else
		{
			strTemp = _T("0");
		}
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("NETSENDFULLMODE"), strTemp);
		//IE Drama Ontop
		if(m_chbIEDramaOnTop.GetCheck())	
		{
			strTemp = _T("1");
		}
		else
		{
			strTemp = _T("0");
		}
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("IEDRAMAONTOP"), strTemp);
		//FF Drama Ontop
		if(m_chbFFDramaOnTop.GetCheck())	
		{
			strTemp = _T("1");
		}
		else
		{
			strTemp = _T("0");
		}
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("FFDRAMAONTOP"), strTemp);
	}
	else
	{
		//RECORD
		SetDlgItemText(txtFileStore, AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt")));
		//SOUND
		SetDlgItemText(txtFileSound, AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FILESOUND"), _T("C:\\sound.wav")));
		//HOTKEY
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("HOTKEY"), _T("F10")) == _T("F10"))
		{
			m_rbF10Ctrl.SetCheck(true);
		}
		else
		{
			m_rbF11Ctrl.SetCheck(true);
		}
		//NETSENDFLASH
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("NETSENDFLASH"), _T("1")) == _T("0"))
			m_chbNSFlashWindow.SetCheck(false);
		else
			m_chbNSFlashWindow.SetCheck(true);
		//NETSENDFULLMODE
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("NETSENDFULLMODE"), _T("0")) == _T("0"))
			m_chbNSFullMode.SetCheck(false);
		else
			m_chbNSFullMode.SetCheck(true);
		//IEDRAMAONTOP
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("IEDRAMAONTOP"), _T("0")) == _T("0"))
			m_chbIEDramaOnTop.SetCheck(false);
		else
			m_chbIEDramaOnTop.SetCheck(true);
		//FFDRAMAONTOP
		if(AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FFDRAMAONTOP"), _T("0")) == _T("0"))
			m_chbFFDramaOnTop.SetCheck(false);
		else
			m_chbFFDramaOnTop.SetCheck(true);
	}
}

void COptionTab::OnbtnSave() 
{
	// TODO: Add your control notification handler code here
	setConfigValue(true);
	//reload flash windows config member variable of dialog netsend
	CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
	pMainDlg->m_pTabMessage->LoadSettingFlashWindow();
}

void COptionTab::OnbtnDefault() 
{
	// TODO: Add your control notification handler code here
	//setConfigValue(false);
	createConfigFile(GetProgramDir() + _T("\\config.ini"));
	setConfigValue(false);
}

void COptionTab::OnrbF10() 
{
	// TODO: Add your control notification handler code here
	if(IsDlgButtonChecked(rbF10))	
	{
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("HOTKEY"), _T("F10"));
	}	
}

void COptionTab::OnrbF11() 
{
	// TODO: Add your control notification handler code here
	if(IsDlgButtonChecked(rbF11))	
	{
		AfxGetApp()->WriteProfileString(_T("INITIAL"), _T("HOTKEY"), _T("F11"));
	}
}
