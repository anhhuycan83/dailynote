#if !defined(AFX_HISTORY_H__42665BAF_4AE4_42F7_991F_5294F527083C__INCLUDED_)
#define AFX_HISTORY_H__42665BAF_4AE4_42F7_991F_5294F527083C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// History.h : header file
//

#include "TabMessage.h"
/////////////////////////////////////////////////////////////////////////////
// CHistory dialog

class CHistory : public CDialog
{
// Construction
public:
	CString m_strMsgSelected;
	void AddNewMsg(CHistoryMsg *arrMsg);
	void LoadHistory();
	CTypedPtrArray<CObArray, CHistoryMsg*> *m_pListHistory;
	CHistory(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CHistory)
	enum { IDD = IDD_DGLHISTORY };
	CEdit	m_txtHistoryMsgCtrl;
	CReportCtrl	m_wndListHistoryCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHistory)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHistory)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnItemchangedwndListHistoryView(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HISTORY_H__42665BAF_4AE4_42F7_991F_5294F527083C__INCLUDED_)
