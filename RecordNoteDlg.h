#if !defined(AFX_RECORDNOTEDLG_H__B2CD5D2E_D66C_45EB_B77E_11CC162358CD__INCLUDED_)
#define AFX_RECORDNOTEDLG_H__B2CD5D2E_D66C_45EB_B77E_11CC162358CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RecordNoteDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRecordNoteDlg dialog



class CRecordNoteDlg : public CDialog
{
// Construction
public:
	void setRtbFont(CString ins_strFont);

	eFrequency GetSelectedFrequencyRb();
	eFrequency GetFrequencyEnum(CString strFre);
	void SetFreRb(eFrequency eFre);
	CString m_strFrequency;

	CString GetFrequencyString(eFrequency eFre);
	

	SYSTEMTIME m_editSysTime;
	int m_indexItemUpdating;
	bool m_isNew;
	void getValues();
	CString m_msgText;
	CRecordNoteDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRecordNoteDlg)
	enum { IDD = IDD_POPUP_RECORD_NOTE };
	CDateTimeCtrl	m_dtpDateTime;
	CString	m_txtMsgValue;
	CButton	m_rbOnceCtrl;
	CButton	m_rbDailyCtrl;
	CButton	m_rbWeeklyCtrl;
	CButton	m_rbMonthlyCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRecordNoteDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CFont m_font;

	// Generated message map functions
	//{{AFX_MSG(CRecordNoteDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RECORDNOTEDLG_H__B2CD5D2E_D66C_45EB_B77E_11CC162358CD__INCLUDED_)
